<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
    'title' => 'PMAP Services',
    'footer' => "<a href='https://www.pmap.org.ph/'>People Management Association of the Philippines</a>.",
    'year' => date('Y', time())
);
