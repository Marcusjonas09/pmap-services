<?php
defined('BASEPATH') or exit('No direct script access allowed');


$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'session', 'form_validation');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'form', 'array', 'text', 'date', 'debug_helper', 'cookie');

$autoload['config'] = array('pmap_config');

$autoload['language'] = array();

$autoload['model'] = array();
