<?php

defined('BASEPATH') or exit('No direct script access allowed');
class CronModel extends CI_Model
{
    public function reset_daily_post()
    {
        $this->db->update('subscribers_tbl', array('daily_post' => 0));
    }

    public function reset_montlhy_post()
    {
        $this->db->update('subscribers_tbl', array('monthly_post' => 0));
    }
}
