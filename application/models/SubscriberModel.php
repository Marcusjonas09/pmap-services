<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SubscriberModel extends CI_Model
{
    public function subscribers()
    {
        $this->db->order_by('date_submitted', 'ASC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array('subscribers_tbl.sub_status !=' => 2))->result();
    }

    public function job_subscribers()
    {
        $this->db->order_by('date_submitted', 'ASC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array('packages_tbl.package_type' => 'job'))->result();
    }

    public function vendor_subscribers()
    {
        $this->db->order_by('date_submitted', 'ASC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array('packages_tbl.package_type' => 'vendor'))->result();
    }

    public function vendor_subscribers_valid()
    {
        $this->db->order_by('date_submitted', 'ASC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'packages_tbl.package_type' => 'vendor',
            'subscribers_tbl.sub_status' => 1
        ))->result();
    }

    public function job_subscribers_valid()
    {
        $this->db->order_by('date_submitted', 'DESC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'packages_tbl.package_type' => 'job',
            'subscribers_tbl.sub_status' => 1
        ))->result();
    }

    public function view_subscriber($sub_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array('sub_id' => $sub_id))->row();
    }

    public function check_if_applied_to_jobs($company_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        $subs = $this->db->get_where('subscribers_tbl', array('company_id' => $company_id))->result();
        foreach ($subs as $sub) {
            if ($sub->package_type == 'job') {
                return true;
            }
        }
        return false;
    }

    public function check_if_subbed_to_jobs($company_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        $subs = $this->db->get_where('subscribers_tbl', array('company_id' => $company_id))->result();
        $message = '';
        foreach ($subs as $sub) {
            if ($sub->package_type == 'job' && $sub->sub_status == 1) {
                $message = 'Company is currently subscribed to a Job Service Plan!';
            }

            if ($sub->package_type == 'job' && $sub->sub_status == 2) {
                $message = 'Company has a pending Job Service Application!';
            }
        }

        if ($message != '') {
            return '<div class="alert alert-warning" role="alert">' . $message . "</div>";
        } else {
            return false;
        }
    }

    public function check_if_subbed_to_vendors($company_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        $subs = $this->db->get_where('subscribers_tbl', array('company_id' => $company_id))->result();
        $message = '';
        foreach ($subs as $sub) {
            if ($sub->package_type == 'vendor' && $sub->sub_status == 1) {
                $message = 'Company is currently subscribed to a Vendor Service Plan!';
            }

            if ($sub->package_type == 'vendor' && $sub->sub_status == 2) {
                $message = 'Company has a pending Vendor Service Application!';
            }
        }

        if ($message != '') {
            return '<div class="alert alert-warning" role="alert">' . $message . "</div>";
        } else {
            return false;
        }
    }

    public function view_job_subscriber_valid($sub_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'sub_id' => $sub_id,
            'subscribers_tbl.sub_status' => 1,
            'packages_tbl.package_type' => 'job'
        ))->row();
    }

    public function view_vendor_subscriber_valid($sub_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'sub_id' => $sub_id,
            'subscribers_tbl.sub_status' => 1,
            'packages_tbl.package_type' => 'vendor'
        ))->row();
    }

    public function add_subscriber($SubDetails)
    {
        $this->db->insert('subscribers_tbl', $SubDetails);
    }

    public function edit_subscriber($sub_id, $SubDetails)
    {
        $this->db->update('subscribers_tbl', $SubDetails, array('sub_id' => $sub_id));
    }

    public function approve_subscriber($sub_id)
    {
        $subscriber = $this->view_subscriber($sub_id);

        if ($subscriber->sub_status == 2) {
            $this->db->set('sub_status', 1);
            $this->db->set('sub_start', date('Y-m-d H:i:s', time()));
            $this->db->set('sub_end', date('Y-m-d H:i:s', time() + (60 * 60 * 24 * $subscriber->package_duration)));
            $this->db->where('sub_id', $sub_id);
            $this->db->update('subscribers_tbl');
            return true;
        } else {
            return false;
        }
    }

    public function delete_subscriber($sub_id)
    {
        $this->db->delete('subscribers_tbl', array('sub_id' => $sub_id));
    }

    ///////////////////////////////////////////////////////////////////////////////

    public function JobsRegisteredCompanies()
    {
        $this->db->order_by('date_submitted', 'ASC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'packages_tbl.package_type' => 'vendor',
            'subscribers_tbl.sub_status' => 1
        ))->num_rows();
    }

    public function VendorRegisteredCompanies()
    {
        $this->db->order_by('date_submitted', 'DESC');
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'packages_tbl.package_type' => 'job',
            'subscribers_tbl.sub_status' => 1
        ))->num_rows();
    }

    public function PendingJobsRequest()
    {
        return $this->db->get_where('subscribers_tbl', array(
            'package_type' => 'job',
            'sub_status' => 2
        ))->num_rows();
    }

    public function PendingVendorsRequest()
    {
        return $this->db->get_where('subscribers_tbl', array(
            'package_type' => 'vendor',
            'sub_status' => 2
        ))->num_rows();
    }
}
