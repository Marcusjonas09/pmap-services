<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VendorsModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
    }

    public function getCompanies()
    {
        $q = $this->db->get('company_tbl');
        return $q->result();
    }
    public function count_all()
    {
        $query = $this->db->get("company_tbl");
        return $query->num_rows();
    }


    public function make_query($CompanyName, $ServiceOffered)
    {
        $current_date = date('Y-m-d h:i:s', time());
        $query = "
            SELECT * FROM vendorposting_tbl
            WHERE IsPublished = '1' AND ExpireAt >= '" . $current_date . "'
            ";

        if (isset($CompanyName) &&  !empty($CompanyName) && $CompanyName != '_') {
            $query .= "
                AND CompanyName LIKE '%" . $CompanyName . "%'
            ";
        }

        if (isset($ServiceOffered) &&  !empty($ServiceOffered) && $ServiceOffered != '_') {
            $query .= "
                    AND ServiceTitle LIKE '%" . $ServiceOffered . "%'
                ";
        }

        return $query;
    }

    public function count_results($CompanyName, $ServiceOffered)
    {
        $query = $this->make_query($CompanyName, $ServiceOffered);
        $data = $this->db->query($query);
        return $data->num_rows();
    }
    function fetch_details($limit, $start, $CompanyName, $ServiceOffered)
    {
        $subscriberDetails = '';

        $q = $this->make_query($CompanyName, $ServiceOffered);

        $q .= ' ORDER BY CreatedAt DESC LIMIT ' . $start . ', ' . $limit;

        $query = $this->db->query($q);

        $subscriberDetails .= '
        <ul class="list-group px-0 bg-white overflow-auto" style="height:600px; overflow-y:hidden;">
        ';
        if (!empty($query->result())) {
            foreach ($query->result() as $service) {
                $subscriberDetails .= '<button onclick="view_vendor_single(' . $service->ServiceId . ')" class="list-group-item list-group-item-action p-0">
                <div class="row py-3 ml-2 mr-0" style="background-color:white;">
                    <div class="col-xs-3 col-3 border rounded border-dark d-flex align-items-center">
                        <img class="img-fluid " src="' . base_url() . 'assets/img/jobs/pmap-logo.PNG" alt="">
                    </div>
                    <div class="col-xs-8 col-8">
                        <p class="font-weight-bold m-0">' . $service->ServiceTitle . '</p>
                        <p class="font-weight-bold m-0"><span class="fa fa-building"></span> ' . $service->CompanyName . '</p>
                        <p class="m-0 text-muted"><span class="fa fa-map-marker-alt"></span> ' . character_limiter($service->CompanyAddress, 40) . '</p>
                    </div>
                </div>
            </button>
            ';
            }
            $subscriberDetails .= '</ul>';
            return $subscriberDetails;
        } else {
            $subscriberDetails .= '
        <ul class="list-group px-0 bg-white" style="height:600px;">
        ';
            $subscriberDetails .= '<button class="list-group-item list-group-item-action p-0">
                <div class="row py-3 ml-2 mr-0" style="background-color:white;">
                    <div class="col-md-12 col-sm-12">
                        <h3 class="m-0 text-muted text-center">No Vendor Services Found</h3>
                    </div>
                </div>
            </button>
            ';
            $subscriberDetails .= '</ul>';
            return $subscriberDetails;
        }
    }

    public function post_service($post_details)
    {
        $q = $this->db->insert('vendorposting_tbl', $post_details);
        $message = '<div class="alert alert-success" role="alert">Service posted successfully!</div>';
        return $message;
    }

    public function getVendorHTML($ServiceId)
    {
        $servicesHTML = "";

        if ($ServiceId) {
            $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId');
            $service = $this->db->get_where('vendorposting_tbl', array(
                'ServiceId' => $ServiceId,
            ))->row();

            $difference = abs(strtotime($service->CreatedAt) - time());

            $SecsInMin = 60;
            $SecsInHour = 60 * 60;
            $SecsInDay = 60 * 60 * 24;

            if ($difference < $SecsInHour) {
                //diplay minutes ago
                $timeDiffSingle =   floor($difference / 60) . ' minute/s ago';
                // echo floor($difference / 60) . ' minutes ago';
            } else if ($difference > $SecsInHour && $difference < $SecsInDay) {
                //diplay hours ago
                $timeDiffSingle =   floor($difference / 60 / 60) . ' hour/s ago';
                // echo floor($difference / 60 / 60) . ' hours ago';
            } else {
                //diplay days ago
                $timeDiffSingle =   floor($difference / 60 / 60 / 24) . ' day/s ago';
                // echo floor($difference / 60 / 60 / 24) . ' days ago';
            }

            $servicesHTML .= '
        <div class="modal-dialog modal-xl m-0 modal-full" role="document">
            <div class="modal-content" style="height:600px;">
                <div class="modal-header" id="web_modal_header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Vendor Services</strong></h5>
                    <button id="close_view" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-x:hidden;">
                        <ul class="list-group overflow-y bg-white">                        
                            <li href="#" class="list-group-item">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4 class="font-weight-bold">' . $service->ServiceTitle . '</h4>
                                        <p class="my-0">' . '<strong><span class="fa fa-building"></span> ' . $service->CompanyName . '</strong></p>
                                        <p class="my-0"><span class="fa fa-map-marker-alt"></span> ' . $service->CompanyAddress . '</p>
                                        <p class="my-0"><span class="fa fa-clock"></span> ' . $timeDiffSingle . '</p>
                                    </div>
                                </div>
                            </li>
                            <li href="#" class="list-group-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="font-weight-bold" id="jobdesc">Contact Us</p>
                                        <small class="text-justify"><strong>Email Address: </strong>' . $service->CompanyEmail . '</small></br>
                                        <small class="text-justify"><strong>Landline: </strong>' . $service->CompanyTelNo . '</small></br>
                                        <small class="text-justify"><strong>Mobile Number:  </strong>' . $service->CompanyCelNo . '</small>
                                        </br>
                                        </br>
                                        <hr>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <p class="font-weight-bold" id="jobdesc">Description</p>
                                        <small class="text-justify">' . $service->ServiceDescription . '</small>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            ';
        } else {
            $servicesHTML .= '
        <div class="modal-dialog modal-xl m-0 modal-full" role="document">
            <div class="modal-content" style="height:600px;">
                <div class="modal-header" id="web_modal_header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Vendor Services</strong></h5>
                    <button id="close_view" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-x:hidden;">
                    <h3 class="m-0 text-muted text-center">No Vendor Services Found</h3>
                </div>
            </div>
        </div>
            ';
        }
        return $servicesHTML;
    }

    public function repost_service($id)
    {
        $job = $this->getVendor($id);
        $this->db->set(array(
            'IsPublished' => 0,
            'IsExpired' => 0,
            'WasPublished' => 0,
            'CreatedAt' => 0,
            'ExpireAt' => 0,
        ));
        $this->db->where('ServiceId', $id);
        $this->db->update('vendorposting_tbl');
    }

    public function getVendorRecent()
    {
        $this->db->select('*');
        $this->db->where(array(
            'IsPublished' => 1,
            'ExpireAt >=' => date('Y-m-d H:i:s', time()),
            'ExpireAt !=' => 0,
        ));

        $this->db->order_by('CreatedAt', 'DESC');
        $q = $this->db->get('vendorposting_tbl');

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function getVendor($id)
    {
        $q = $this->db->get_where('vendorposting_tbl', array(
            'ServiceId' => $id,
        ));
        return $q->row();
    }

    public function getSubscriber($company_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'company_id' => $company_id,
            'subscribers_tbl.package_type' => 'vendor',
        ))->row();
    }

    public function getPublishedPosts()
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        if ($this->session->role == 'admin') {
            $condition = array(
                'isPublished' => 1,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
            );
        } else {
            $condition = array(
                'isPublished' => 1,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
                'company_tbl.CompanyId' => $this->session->user_id
            );
        }

        $q = $this->db->get_where(
            'vendorposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getUnpublishedPosts()
    {
        // $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        // $q = $this->db->get_where('vendorposting_tbl', array(
        //     'isPublished' => 0,
        //     'isDeleted' => 0,
        // ));
        // return $q->result();

        $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        if ($this->session->role == 'admin') {
            $condition = array(
                'isPublished' => 0,
                'isDeleted' => 0,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
            );
        } else {
            $condition = array(
                'isPublished' => 0,
                'isDeleted' => 0,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
                'company_tbl.CompanyId' => $this->session->user_id
            );
        }

        $q = $this->db->get_where(
            'vendorposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getExpiredPosts()
    {
        // $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        // $q = $this->db->get_where('vendorposting_tbl', array(
        //     'ExpireAt <=' => date('Y-m-d H:i:s', time()),
        //     'ExpireAt !=' => 0,
        // ));
        // return $q->result();


        $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        if ($this->session->role == 'admin') {
            $condition = array(
                'ExpireAt <=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => 0,
            );
        } else {
            $condition = array(
                'ExpireAt <=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => 0,
                'company_tbl.CompanyId' => $this->session->user_id
            );
        }

        $q = $this->db->get_where(
            'vendorposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getDeletedPosts()
    {
        // $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        // $q = $this->db->get_where('vendorposting_tbl', array(
        //     'IsDeleted' => 1
        // ));
        // return $q->result();

        $this->db->join('company_tbl', 'company_tbl.CompanyId = vendorposting_tbl.CompanyId', 'LEFT');
        if ($this->session->role == 'admin') {
            $condition = array(
                'IsDeleted' => 1
            );
        } else {
            $condition = array(
                'IsDeleted' => 1,
                'company_tbl.CompanyId' => $this->session->user_id
            );
        }

        $q = $this->db->get_where(
            'vendorposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function delete_unpublished_service($id)
    {
        $vendor = $this->getvendor($id);
        $vendor->ServiceId;

        if ($vendor->IsPublished == 0) {
            $this->db->set('IsDeleted', 1);
            $this->db->where('ServiceId', $id);
            $this->db->update('vendorposting_tbl');
        }
    }

    public function publish_service($Service, $subscriber)
    {
        $this->db->set('daily_post', $subscriber->daily_post + 1);
        $this->db->set('monthly_post', $subscriber->monthly_post + 1);
        $this->db->where('sub_id', $subscriber->sub_id);
        $this->db->update('subscribers_tbl');

        if ($Service->IsPublished == 0 && $Service->WasPublished == 0) {
            $this->db->set('IsPublished', 1);
            $this->db->set('CreatedAt', date('Y-m-d H:i:s', time()));
            $this->db->set('ExpireAt', date('Y-m-d H:i:s', time() + (60 * 60 * 24 * $Service->PostDuration)));
            $this->db->where('ServiceId', $Service->ServiceId);
            $this->db->update('vendorposting_tbl');
        } else {
            $this->db->set('IsPublished', 1);
            $this->db->where('ServiceId', $Service->ServiceId);
            $this->db->update('vendorposting_tbl');
        }
    }

    public function publish_job($Job, $subscriber)
    {
        $this->db->set('daily_post', $subscriber->daily_post + 1);
        $this->db->where('sub_id', $subscriber->sub_id);
        $this->db->update('subscribers_tbl');

        if ($Job->IsPublished == 0 && $Job->WasPublished == 0) {
            $this->db->set('IsPublished', 1);
            $this->db->set('CreatedAt', date('Y-m-d H:i:s', time()));
            $this->db->set('ExpireAt', date('Y-m-d H:i:s', time() + (60 * 60 * 24 * $Job->PostDuration)));
            $this->db->where('JobId', $Job->JobId);
            $this->db->update('jobposting_tbl');
        } else {
            $this->db->set('IsPublished', 1);
            $this->db->where('JobId', $Job->JobId);
            $this->db->update('jobposting_tbl');
        }
    }

    public function unpublish_vendor($id)
    {
        $vendor = $this->getvendor($id);
        $vendor->ServiceId;

        if ($vendor->IsPublished == 1) {
            $this->db->set('IsPublished', 0);
            $this->db->set('WasPublished', 1);
            $this->db->where('ServiceId', $id);
            $this->db->update('vendorposting_tbl');
        }
    }

    public function update_service($id, $data)
    {
        $vendor = $this->getvendor($id);

        if ($vendor->IsPublished == 0) {
            $this->db->set($data);
            $this->db->where('ServiceId', $id);
            $this->db->update('vendorposting_tbl');
        }
    }

    public function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }
}
