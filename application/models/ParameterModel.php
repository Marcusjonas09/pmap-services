<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ParameterModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // PARAMETERS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function get_all()
    {
        $q = $this->db->get('parameter_tbl');
        return $q->result();
    }

    public function add($data)
    {
        $this->db->insert('parameter_tbl', $data);
    }

    public function add_value($data)
    {
        $q = $this->db->get_where('param_val_tbl', array(
            'pv_param' => $data['pv_param'],
            'pv_value' => $data['pv_value']
        ));
        $numrows =  $q->num_rows();

        if ($numrows == 0) {
            $this->db->insert('param_val_tbl', $data);
            $message = '<div class="alert alert-success" role="alert">Record added successfully!</div>';
        } else {
            $message = '<div class="alert alert-warning" role="alert">Parameter value already exists!</div>';
        }
        return $message;
    }

    public function fetchValues($param_id)
    {
        $q = $this->db->get_where('param_val_tbl', array(
            'pv_param' => $param_id
        ));
        return $q->result();
    }

    public function edit($id, $param_name, $param_type)
    {
        $q = $this->db->get_where('parameter_tbl', array(
            'param_name' => $param_name,
            'param_type' => $param_type
        ));

        $result = $q->row();

        if ($param_name != $result->param_name || $param_type != $result->param_type) {
            $message = '<div class="alert alert-success" role="alert">Record edited successfully!</div>';
            $this->db->set('param_name', $param_name);
            $this->db->set('param_type', $param_type);
            $this->db->where('param_id', $id);
            $this->db->update('parameter_tbl');
        } else {
            $message = '<div class="alert alert-warning" role="alert">No changes were done!</div>';
        }
        return $message;
    }

    public function delete($id)
    {
        $this->db->delete('parameter_tbl', array('param_id' => $id));
    }

    public function delete_job_select($id)
    {
        $this->db->delete('parameter_tbl', array('param_id' => $id));
        $this->db->delete('param_val_tbl', array('pv_param' => $id));
    }

    public function delete_job_param_val($id)
    {
        $this->db->delete('param_val_tbl', array('pv_id' => $id));
    }

    public function view($id)
    {
        $q = $this->db->get_where('parameter_tbl', array('param_id' => $id));
        return $q->row();
    }

    public function fetchParameters()
    {
        $q = $this->db->get('parameter_tbl');
        return $q->result();
    }

    public function fetchParameterValues()
    {
        $q = $this->db->get('param_val_tbl');
        return $q->result();
    }

    public function getIndustries()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Industry'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getJobFunctions()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Job Function'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getJobTypes()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Job Type'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getJobDurations()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Job Duration'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getMinEducations()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Minimum Education'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getMinExperiences()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Minimum Experience'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    public function getRequiredTravels()
    {
        $this->db->select('pv_value');
        $this->db->from('param_val_tbl');
        $this->db->where(array(
            'param_name' => 'Required Travel'
        ));
        $this->db->join('parameter_tbl', 'parameter_tbl.param_id = param_val_tbl.pv_param');
        $q = $this->db->get();
        return $q->result();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PARAMETERS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function getAllJobs()
    {
        $q = $this->db->get('jobposting_tbl');
        return $q->result();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // VENDORS
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function getAllServices()
    {
        $q = $this->db->get('vendorposting_tbl');
        return $q->result();
    }

    public function getAllVendors()
    {
        $q = $this->db->get('vendors_tbl');
        return $q->result();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF VENDORS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // PACKAGES
    //////////////////////////////////////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PACKAGES 
    //////////////////////////////////////////////////////////////////////////////////////////////
}
