<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SettingModel extends CI_Model {
	public function get_all(){
		$q = $this->db->get('settings');
		return $q->result();
	}
	public function get($id){
		$this->db->where('settings_id',$id);
		$q = $this->db->get('settings');
		return $q->row();	
	}
	public function update($id, $data){
		$this->db->where('settings_id',$id);
		$this->db->update('settings',$data);
	}
}