<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    public function login($cond){
        $q = $this->db->get_where('residents',$cond);
        $r = $q->row();
        if(isset($r)){
            return TRUE;
        }
        return FALSE;
    }
    public function login_status($cond){
        $q = $this->db->get_where('residents',$cond);
        $r = $q->row();
         if(isset($r)){
            //audit trail
            $arr = array(
                'trail_user_id' => $r->id,
                'trail_user_type' => $r->type,
                'trail_action' => 'logged in',
                'trail_description' => $r->fname.' '.$r->lname.' was logged in',
                'created_at' => date("Y-m-d H:i:s"),
                'trail_user_ip' => $this->input->ip_address(),
            );
            $this->db->insert('trails',$arr);
            $this->db->reset_query();
            //end audit trail
            $data = array(
                'user_id' => $r->id,
                'name' => $r->fname.' '.$r->lname,
                'role' => $r->type,
                'logged_in' => TRUE,
                'type' => $r->type,
                'email' => $r->email,
                'contact_no' => $r->cno,
                'image' => $r->profilepict,
            );
            $this->session->set_userdata($data);
            return TRUE;
        }
        return FALSE;
    }
}