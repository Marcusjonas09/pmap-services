<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CompanyModel extends CI_Model
{

    public function companies()
    {
        $this->db->order_by('CompanyName', 'ASC');
        return $this->db->get('company_tbl')->result();
    }

    public function view_company($CompanyId)
    {
        return $this->db->get_where('company_tbl', array('CompanyId' => $CompanyId))->row();
    }

    public function add_company($CompanyDetails)
    {
        $this->db->insert('company_tbl', $CompanyDetails);
    }

    public function edit_company($CompanyId, $CompanyDetails)
    {
        $this->db->update('company_tbl', $CompanyDetails, array('CompanyId' => $CompanyId));
    }

    public function delete_company($CompanyId)
    {
        $this->db->delete('company_tbl', array('CompanyId' => $CompanyId));
    }
}
