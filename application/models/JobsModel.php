<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JobsModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
    }

    public function make_query($jobtitle, $joblocation, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry)
    {
        $filter_by_function = "('" . implode("','", explode(',', $jobFunction)) . "')";
        $filter_by_industry = "('" . implode("','", explode(',', $Industry)) . "')";
        $current_date = date('Y-m-d h:i:s', time());
        $query = "
            SELECT * FROM jobposting_tbl
            WHERE IsPublished = '1' AND ExpireAt >= '" . $current_date . "'
            ";

        if ($filter_by_function != "('')") {
            $query .= "
                AND JobFunction IN " . $filter_by_function . "
            ";
        }

        if ($filter_by_industry != "('')") {
            $query .= "
                AND Industry IN " . $filter_by_industry . "
            ";
        }

        if (isset($jobtitle) &&  !empty($jobtitle) && $jobtitle != '_') {
            $query .= "
                AND JobPositionTitle LIKE '%" . $jobtitle . "%'
            ";
        }

        if (isset($joblocation) &&  !empty($joblocation) && $joblocation != '_') {
            $query .= "
                    AND JobLocation LIKE '%" . $joblocation . "%'
                ";
        }

        if ($isPreferred) {
            $query .= "
                    AND IsPreferred = " . $isPreferred . "
                ";
        }

        if ($isSpotlight) {
            $query .= "
                    AND IsSpotlight = " . $isSpotlight . "
                ";
        }

        if ($isMemberCompany) {
            $query .= "
                    AND IsMemberCompany = " . $isMemberCompany . "
                ";
        }

        return $query;
    }

    public function count_results($jobtitle, $joblocation, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry)
    {
        $query = $this->make_query($jobtitle, $joblocation, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry);
        $data = $this->db->query($query);
        return $data->num_rows();
    }

    function fetch_details($limit, $start, $jobtitle, $joblocation, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry)
    {
        $jobpost = '';

        $q = $this->make_query($jobtitle, $joblocation, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry);

        $q .= ' ORDER BY CreatedAt DESC LIMIT ' . $start . ', ' . $limit;

        $query = $this->db->query($q);

        $jobpost .= '
        <ul class="list-group px-0 bg-white overflow-auto" style="height:600px; overflow-y:hidden;">
        ';
        if (!empty($query->result())) {
            foreach ($query->result() as $job) {
                $difference = abs(strtotime($job->CreatedAt) - time());

                $SecsInMin = 60;
                $SecsInHour = 60 * 60;
                $SecsInDay = 60 * 60 * 24;

                if ($difference > 0 && $difference < $SecsInHour) {
                    //diplay minutes ago
                    $timeDiff =   floor($difference / 60) . ' minute/s ago';
                    // echo floor($difference / 60) . ' minutes ago';
                } else if ($difference > $SecsInHour && $difference < $SecsInDay) {
                    //diplay hours ago
                    $timeDiff =   floor($difference / 60 / 60) . ' hour/s ago';
                    // echo floor($difference / 60 / 60) . ' hours ago';
                } else {
                    //diplay days ago
                    $timeDiff =   floor($difference / 60 / 60 / 24) . ' day/s ago';
                    // echo floor($difference / 60 / 60 / 24) . ' days ago';
                }
                $jobpost .= '<button id="' . $job->JobId . '" onclick="view_job_single(' . $job->JobId . ')" class="list-group-item list-group-item-action p-0 ' . ($job->IsSpotlight ? 'bg-warning' : '') . '">
                    <div class="row py-3 ml-2 mr-0 " style="background-color:white;">
                        <div class="col-xs-3 col-3 border rounded border-dark d-flex align-items-center">
                            <img class="img-fluid " src="' . base_url() . 'assets/img/jobs/pmap-logo.PNG" alt="">
                        </div>
                        <div class="col-xs-8 col-8">
                            <p class="font-weight-bold m-0">' . character_limiter($job->JobPositionTitle, 30) . '</p>
                            ' . ($job->IsPreferred ? '<span class="badge badge-primary">Preferred</span>' : '') . '
                            <p class="font-weight-bold m-0"><span class="fa fa-building"></span> ' . $job->CompanyName . '</p>
                            <p class="m-0 small text-muted"><span class="fa fa-map-marker-alt"></span> ' . character_limiter($job->JobLocation, 40) . '</p>
                            <p class="m-0 text-muted small"><span class="fa fa-clock"></span> ' . $timeDiff . '</p>
                        </div>
                    </div>
                </button>
                ';
            }
            $jobpost .= '</ul>';
            return $jobpost;
        } else {
            $jobpost .= '
            <ul class="list-group px-0 bg-white" style="height:600px;">
            ';
            $jobpost .= '<button class="list-group-item list-group-item-action p-0">
                    <div class="row py-3 ml-2 mr-0" style="background-color:white;">
                        <div class="col-md-12 col-sm-12">
                            <h3 class="m-0 text-muted text-center">No Jobs Found</h3>
                        </div>
                    </div>
                </button>
                ';
            $jobpost .= '</ul>';
            return $jobpost;
        }
    }

    public function getjobHTML($id)
    {

        $jobpost = "";

        if ($id) {
            $q = $this->db->get_where('jobposting_tbl', array(
                'JobId' => $id,
            ));

            $jobSingle = $q->row();

            $company = $this->db->get_where('company_tbl', array('CompanyId' => $jobSingle->CompanyId))->row();

            $recipient = $company->HRemail;
            $subject = 'Job Application for ' . $jobSingle->JobPositionTitle . ' ads posted from PMAP Services';
            $body = 'I found out about your job offer at http://services.pmap.org.ph/jobs';

            $email = 'mailto:' . $recipient . '?subject=' . $subject . '&amp;body=' . $body;

            $difference = abs(strtotime($jobSingle->CreatedAt) - time());

            $SecsInMin = 60;
            $SecsInHour = 60 * 60;
            $SecsInDay = 60 * 60 * 24;

            if ($difference < $SecsInHour) {
                //diplay minutes ago
                $timeDiffSingle =   floor($difference / 60) . ' minute/s ago';
                // echo floor($difference / 60) . ' minutes ago';
            } else if ($difference > $SecsInHour && $difference < $SecsInDay) {
                //diplay hours ago
                $timeDiffSingle =   floor($difference / 60 / 60) . ' hour/s ago';
                // echo floor($difference / 60 / 60) . ' hours ago';
            } else {
                //diplay days ago
                $timeDiffSingle =   floor($difference / 60 / 60 / 24) . ' day/s ago';
                // echo floor($difference / 60 / 60 / 24) . ' days ago';
            }
            $salary = "";
            if ($jobSingle->MinSalary != 0 && $jobSingle->MaxSalary != 0) {
                $salary = 'P' . number_format($jobSingle->MinSalary, 2, '.', ',') . '- P' . number_format($jobSingle->MaxSalary, 2, '.', ',');
            }

            $jobpost .= '
            <div class="modal-dialog modal-xl m-0 modal-full" role="document">
            <div class="modal-content" style="height:600px;">
                <div class="modal-header" id="web_modal_header">
                    <h5 class="modal-title" id="exampleModalLabel"><strong>Job Information </strong></h5>
                    <button id="close_view" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-x:hidden;">
        <ul class="list-group overflow-y bg-white">
        <li href="#" class="list-group-item">
            <div class="row">
                <div class="col-md-8">

                    <h4 class="font-weight-bold">' . character_limiter($jobSingle->JobPositionTitle, 30) . '</h4>
                    ' . (($jobSingle->IsSpotlight) ? '<span class="badge badge-warning text-light p-2 mr-2">Spotlight</span>' : '') . '

                    ' . (($jobSingle->IsPreferred) ? '<span class="badge badge-primary text-light p-2 mr-2">Preferred</span>' : '') . '

                    ' . (($jobSingle->IsMemberCompany) ? '<span class="badge badge-success text-light p-2 mr-2">MemberCompany</span>' : '') . '

                    <p class="my-0">' . '<strong>' . $jobSingle->JobType . ' | </strong><strong class="text-success">' . $salary . '</strong></p>
                    <p class="my-0">' . '<strong><span class="fa fa-building"></span> ' . $jobSingle->CompanyName . '</strong></p>
                    <p class="my-0"><span class="fa fa-map-marker-alt"></span> ' . $jobSingle->JobLocation . '</p>
                    <p class="my-0"><span class="fa fa-clock"></span> ' . $timeDiffSingle . '</p>
                </div>

                <div class="col-md-4 mt-auto">
                    <a class="col-md-12 btn btn-primary float-right mt-2" href="' . $email . '">Apply Now</a>
                </div>
            </div>
        </li>

        <li href="#" class="list-group-item">
            <div class="row">
                <div class="col-md-4" id="jobInfoLeft">
                    <p class="font-weight-bold">Job Information</p><hr>
                    <small><p class="m-0"><strong>Job ID:</strong> ' . $jobSingle->JobId . '</p></small>
                    <small><p class="m-0"><strong>Position Title:</strong> ' . $jobSingle->JobPositionTitle . '</p></small>
                    <small><p class="m-0"><strong>Company Name:</strong> ' . $jobSingle->CompanyName . '</p></small>
                    <small><p class="m-0"><strong>Industry:</strong> ' . $jobSingle->Industry . '</p></small>
                    <small><p class="m-0"><strong>Job Function:</strong> ' . $jobSingle->JobFunction . '</p></small>
                    <small><p class="m-0"><strong>Job Type:</strong> ' . $jobSingle->JobType . '</p></small>
                    <small><p class="m-0"><strong>Job Duration:</strong> ' . $jobSingle->JobDuration . '</p></small>
                    <small><p class="m-0"><strong>Min Education:</strong> ' . $jobSingle->MinExperience . '</p></small>
                    <small><p class="m-0"><strong>Min Experience:</strong> ' . $jobSingle->MinExperience . '</p></small>
                    <small><p class="m-0"><strong>Required Travel:</strong> ' . $jobSingle->RequiredTravel . '</p></small>
                    <small><p class="m-0"><strong>Salary:</strong> ' . $salary . '</p></small>
                </div>

                <div class="col-md-8">                    
                    <p class="font-weight-bold" id="jobdesc">Job Description</p><hr>
                    <small class="text-justify">' . $jobSingle->JobDescription . '</small>

                    <p class="font-weight-bold mt-5">Job Requirements</p><hr>
                    <small class="text-justify">' . $jobSingle->JobRequirements . '</small>
                </div>

                <div class="col-md-4" id="JobInfoRight">
                    <p class="font-weight-bold">Job Information</p><hr>
                    <small><p class="m-0"><strong>Job ID:</strong> ' . $jobSingle->JobId . '</p></small>
                    <small><p class="m-0"><strong>Position Title:</strong> ' . $jobSingle->JobPositionTitle . '</p></small>
                    <small><p class="m-0"><strong>Company Name:</strong> ' . $jobSingle->CompanyName . '</p></small>
                    <small><p class="m-0"><strong>Industry:</strong> ' . $jobSingle->Industry . '</p></small>
                    <small><p class="m-0"><strong>Job Function:</strong> ' . $jobSingle->JobFunction . '</p></small>
                    <small><p class="m-0"><strong>Job Type:</strong> ' . $jobSingle->JobType . '</p></small>
                    <small><p class="m-0"><strong>Job Duration:</strong> ' . $jobSingle->JobDuration . '</p></small>
                    <small><p class="m-0"><strong>Min Education:</strong> ' . $jobSingle->MinExperience . '</p></small>
                    <small><p class="m-0"><strong>Min Experience:</strong> ' . $jobSingle->MinExperience . '</p></small>
                    <small><p class="m-0"><strong>Required Travel:</strong> ' . $jobSingle->RequiredTravel . '</p></small>
                    <small><p class="m-0"><strong>Salary:</strong> ' . $salary . '</p></small>
                </div>
            </div>
        </li>
        
    </ul>
    </div>
    </div>
    </div>';
        } else {
            $jobpost .= '
            <div class="modal-dialog modal-xl m-0 modal-full" role="document">
                <div class="modal-content" style="height:600px;">
                    <div class="modal-header" id="web_modal_header">
                        <h5 class="modal-title" id="exampleModalLabel"><strong>Vendor Services</strong></h5>
                        <button id="close_view" type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="overflow-x:hidden;">
                        <h3 class="m-0 text-muted text-center">No Postings Found</h3>
                    </div>    
                </div>
            </div>
                ';
        }
        return $jobpost;
    }

    public function getJobRecent()
    {
        $this->db->select('JobId');
        $this->db->where(array(
            'IsPublished' => TRUE,
            'ExpireAt >=' => date('Y-m-d H:i:s', time()),
            'ExpireAt !=' => 0,
        ));

        $this->db->order_by('CreatedAt', 'DESC');
        $q = $this->db->get('jobposting_tbl');
        return $q->row();
    }

    public function getjob($id)
    {
        $q = $this->db->get_where('jobposting_tbl', array(
            'JobId' => $id,
        ));
        return $q->row();
    }

    public function getPublishedJobs()
    {
        if ($this->session->role == 'admin') {
            $condition = array(
                'isPublished' => 1,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
            );
        } else {
            $condition = array(
                'isPublished' => 1,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
                'CompanyId' => $this->session->user_id
            );
        }
        $q = $this->db->get_where(
            'jobposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getUnpublishedJobs()
    {
        // $q = $this->db->get_where('jobposting_tbl', array(
        //     'isPublished' => 0,
        //     'isDeleted' => 0,
        // ));
        // return $q->result();

        if ($this->session->role == 'admin') {
            $condition = array(
                'isPublished' => 0,
                'isDeleted' => 0,
            );
        } else {
            $condition = array(
                'isPublished' => 0,
                'isDeleted' => 0,
                'ExpireAt >=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
                'CompanyId' => $this->session->user_id
            );
        }
        $q = $this->db->get_where(
            'jobposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getExpiredJobs()
    {
        // $q = $this->db->get_where('jobposting_tbl', array(
        //     'ExpireAt <=' => date('Y-m-d H:i:s', time()),
        //     'ExpireAt !=' => '0',
        // ));
        // return $q->result();

        if ($this->session->role == 'admin') {
            $condition = array(
                'ExpireAt <=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
            );
        } else {
            $condition = array(
                'ExpireAt <=' => date('Y-m-d H:i:s', time()),
                'ExpireAt !=' => '0',
                'CompanyId' => $this->session->user_id
            );
        }
        $q = $this->db->get_where(
            'jobposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function getDeletedJobs()
    {
        // $q = $this->db->get_where('jobposting_tbl', array(
        //     'IsDeleted' => 1
        // ));
        // return $q->result();

        if ($this->session->role == 'admin') {
            $condition = array(
                'IsDeleted' => 1
            );
        } else {
            $condition = array(
                'IsDeleted' => 1,
                'CompanyId' => $this->session->user_id
            );
        }
        $q = $this->db->get_where(
            'jobposting_tbl',
            $condition
        );
        return $q->result();
    }

    public function delete_unpublished($id)
    {
        $job = $this->getjob($id);
        $job->JobId;

        if ($job->IsPublished == 0) {
            $this->db->set('IsDeleted', 1);
            $this->db->where('JobId', $id);
            $this->db->update('jobposting_tbl');
        }
    }

    public function getSubscriber($company_id)
    {
        $this->db->join('company_tbl', 'company_tbl.CompanyId = subscribers_tbl.company_id');
        $this->db->join('packages_tbl', 'packages_tbl.package_id = subscribers_tbl.package_id');
        return $this->db->get_where('subscribers_tbl', array(
            'company_id' => $company_id,
            'subscribers_tbl.package_type' => 'job',
        ))->row();
    }

    public function publish_job($Job, $subscriber)
    {
        $this->db->set('daily_post', $subscriber->daily_post + 1);
        $this->db->set('monthly_post', $subscriber->monthly_post + 1);
        $this->db->where('sub_id', $subscriber->sub_id);
        $this->db->update('subscribers_tbl');

        if ($Job->IsPublished == 0 && $Job->WasPublished == 0) {
            $this->db->set('IsPublished', 1);
            $this->db->set('CreatedAt', date('Y-m-d H:i:s', time()));
            $this->db->set('ExpireAt', date('Y-m-d H:i:s', time() + (60 * 60 * 24 * $Job->PostDuration)));
            $this->db->where('JobId', $Job->JobId);
            $this->db->update('jobposting_tbl');
        } else {
            $this->db->set('IsPublished', 1);
            $this->db->where('JobId', $Job->JobId);
            $this->db->update('jobposting_tbl');
        }
    }

    public function post_job($job_details)
    {
        $q = $this->db->insert('jobposting_tbl', $job_details);
        $message = '<div class="alert alert-success" role="alert">Job posted successfully!</div>';
        return $message;
    }

    public function repost_job($id)
    {
        $job = $this->getjob($id);
        $this->db->set(array(
            'IsPublished' => 0,
            'IsExpired' => 0,
            'WasPublished' => 0,
            'CreatedAt' => 0,
            'ExpireAt' => 0,
        ));
        $this->db->where('JobId', $id);
        $this->db->update('jobposting_tbl');
    }

    public function unpublish_job($id)
    {
        $job = $this->getjob($id);
        $job->JobId;

        if ($job->IsPublished == 1) {
            $this->db->set('IsPublished', 0);
            $this->db->set('WasPublished', 1);
            $this->db->where('JobId', $id);
            $this->db->update('jobposting_tbl');
        }
    }

    public function update_job($id, $data)
    {
        $job = $this->getjob($id);
        $job->JobId;

        if ($job->IsPublished == 0) {
            $this->db->set($data);
            $this->db->set(array('UpdatedAt' => date('Y-m-d H:i:s', time())));
            $this->db->where('JobId', $id);
            $this->db->update('jobposting_tbl');
        }
    }

    public function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }
}
