<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FAQModel extends CI_Model {
    
    public function get_all(){
        $q = $this->db->get('faqs'); 
        return $q->result();    
    }
    
    public function get($id){
        $q = $this->db->where('faq_id',$id)
            ->get('faqs');
        
        return $q->row();
    }

    public function delete($id){
        $this->db->where('faq_id',$id)
            ->delete('faqs');
    } 

    public function insert($data){
        $this->db->insert('faqs',$data);

    }

    public function update($id,$data){
        $this->db->where('faq_id',$id)
            ->update('faqs',$data);

    }
}
