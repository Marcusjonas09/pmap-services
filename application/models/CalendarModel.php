<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CalendarModel extends CI_Model {
    public function getDates($year, $month){
        $cond = $year.'-'.$month;
        $this->db->like('date',$cond,'after');
        $q = $this->db->get('tblcalendar');
        //$q = $this->db->get_compiled_select('tblcalendar');
        //die($q);
        return  $q->result();
    }
    public function insert($data){
        $this->db->insert('tblcalendar',$data);
    }
}