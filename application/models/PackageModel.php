<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PackageModel extends CI_Model
{
    public function create_package($package_details)
    {
        $this->db->insert('packages_tbl', $package_details);
        $message = '<div class="alert alert-success" role="alert">Package created successfully!</div>';
        return $message;
    }

    public function edit_package($package_id, $package_details)
    {
        $this->db->set($package_details);
        $this->db->where(array(
            'package_id' => $package_id
        ));
        $this->db->update('packages_tbl');
        $message = '<div class="alert alert-success" role="alert">Package details saved successfully!</div>';
        return $message;
    }

    public function delete_package($id)
    {
        $this->db->delete('packages_tbl', array('package_id' => $id));
    }

    public function get_all_packages()
    {
        return $this->db->get('packages_tbl')->result();
    }

    public function get_all_vendor_packages()
    {
        return $this->db->get_where('packages_tbl', array('package_type' => 'vendor'))->result();
    }

    public function get_all_job_packages()
    {
        return $this->db->get_where('packages_tbl', array('package_type' => 'job'))->result();
    }

    public function get_package($id)
    {
        return $this->db->get_where('packages_tbl', array('package_id' => $id))->row();
    }
}
