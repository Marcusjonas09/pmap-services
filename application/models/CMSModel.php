<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CMSModel extends CI_Model
{

    public function contents()
    {
        $this->db->order_by('created_at', 'DESC');
        return $this->db->get('content_tbl')->result();
    }

    public function view_content($content_id)
    {
        return $this->db->get_where('content_tbl', array('content_id' => $content_id))->row();
    }

    public function published_contents()
    {
        return $this->db->get_where('content_tbl', array('is_published' => 1))->result();
    }

    public function add_content($content_details)
    {
        $this->db->insert('content_tbl', $content_details);
    }

    public function edit_content($content_id, $content_details)
    {
        $this->db->update('content_tbl', $content_details, array('content_id' => $content_id));
    }

    public function delete_content($content_id)
    {
        $this->db->delete('content_tbl', array('content_id' => $content_id));
    }

    public function publish_content($content_id)
    {
        $this->db->update('content_tbl', array('is_published' => 1), array('content_id' => $content_id));
    }

    public function unpublish_content($content_id)
    {
        $this->db->update('content_tbl', array('is_published' => 0), array('content_id' => $content_id));
    }






















































    public function get_all_info()
    {
        $this->db->order_by('info_id', 'DESC');
        $q = $this->db->get('informations');
        return $q->result();
    }


    public function insert($data)
    {

        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'insert',
            'trail_description' => $this->session->name . ' inserted events ' . implode(' ', $data),
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail

        $this->db->insert('informations', $data);
    }

    public function get_info($info_id)
    {
        $this->db->where('info_id', $info_id);
        $this->db->order_by('info_id', 'DESC');
        $q = $this->db->get('informations');
        return $q->row();
    }

    public function update($info_id, $data)
    {

        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'updated',
            'trail_description' => $this->session->name . ' updated events ' . implode(' ', $data) . ' with id ' . $info_id,
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail

        $this->db->where('info_id', $info_id);
        $this->db->update('informations', $data);
    }

    public function delete($info_id)
    {
        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'deleted',
            'trail_description' => $this->session->name . ' deleted events with id ' . $info_id,
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail


        $this->db->where('info_id', $info_id);
        $this->db->delete('informations');
    }
}
