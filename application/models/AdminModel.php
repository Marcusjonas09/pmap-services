<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminModel extends CI_Model
{

    public function login($cond)
    {
        $q = $this->db->get_where('tbladmin', $cond);
        $r = $q->row();

        if (isset($r)) {
            //audit trail
            $arr = array(
                'trail_user_id' => $r->id,
                'trail_user_type' => $r->admin_role,
                'trail_action' => 'logged in',
                'trail_description' => $r->admin_name . ' was logged in',
                'created_at' => date("Y-m-d H:i:s"),
                'trail_user_ip' => $this->input->ip_address(),
            );
            $this->db->insert('trails', $arr);
            $this->db->reset_query();
            //end audit trail

            //settings
            $q = $this->db->get_where('settings');
            $rs = $q->result();
            $this->db->reset_query();

            $data = array(
                'user_id' => $r->id,
                'role' => $r->admin_role,
                'name' => $r->admin_name,
                'logged_in' => TRUE,
                'type' => 'admin',
                'image' => 'default_avatar.png',
                'msg' => $rs[0]->settings_enable,
                'sms' => $rs[1]->settings_enable,
                'captcha' => $rs[2]->settings_enable,
            );

            //dd($data);

            $this->session->set_userdata($data);
            return TRUE;
        }
        return FALSE;
    }


    public function get_all()
    {
        $q = $this->db->get('tbladmin');
        return $q->result();
    }
    public function get($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('tbladmin');
        return $q->row();
    }
    public function insert($data)
    {
        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'insert',
            'trail_description' => $this->session->name . ' inserted admin ' . implode(' ', $data),
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail
        $this->db->insert('tbladmin', $data);
    }

    public function password_check($cond)
    {
        $this->db->where($cond);
        $q = $this->db->get('tbladmin');
        $r = $q->row();
        if (isset($r)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function update($id, $data)
    {
        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'updated',
            'trail_description' => $this->session->name . ' updated admin record ' . implode(' ', $data) . ' with id ' . $id,
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail
        $this->db->where('id', $id);
        $this->db->update('tbladmin', $data);
    }

    public function destroy($id)
    {
        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'deleted',
            'trail_description' => $this->session->name . ' deleted admin with id ' . $id,
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail

        $this->db->where('id', $id);
        $this->db->delete('tbladmin');
    }

    public function update_status($id)
    {

        //audit trail
        $arr = array(
            'trail_user_id' => $this->session->user_id,
            'trail_user_type' => $this->session->role,
            'trail_action' => 'updated',
            'trail_description' => $this->session->name . ' updated admin record ' . implode(' ', $data) . ' with id ' . $id,
            'created_at' => date("Y-m-d H:i:s"),
            'trail_user_ip' => $this->input->ip_address(),
        );
        $this->db->insert('trails', $arr);
        $this->db->reset_query();
        //end audit trail

        $this->db->where('id', $id);
        $q = $this->db->get('tbladmin');
        $r = $q->row();
        if (isset($r)) {
            $status = $r->status;
            $status = $status == 1 ? 0 : 1;
            $data = array('status' => $status);
            $this->db->reset_query();
            $this->db->where('id', $id);
            $this->db->update('tbladmin', $data);
        }
    }














    //     public function checkLogin($data){
    //         $this->db->where($data);
    //         $q = $this->db->get('tblAdmin');
    //         $row = $q->row();
    //         if(isset($row)){
    //             $newdata = array(
    //                 'name'  => $row->name,
    //                 //'role' => $row->role; //1 2 4 8 16 
    //                 'logged_in' => TRUE,
    //             );

    //             $this->session->set_userdata($newdata);
    //             //$this->session->mark_as_flash('name');
    //             $this->session->mark_as_temp('name',10);
    //         }

    //     }

    //     public function test(){
    //         //$this->db->where('id',1);
    //         $q = $this->db->where('id',2)->get('tbladmin');
    //         var_dump($q->result());

    //         //$sql = $this->db->get_compiled_select('tbladmin');
    //         //echo $sql;
    //         //$this->db->where('id',1);
    //         //$q = $this->db->get('tblAdmin');
    //         //$row = $q->row(0,'User');
    //         //$row->setName($row->name);
    //         //echo $row->sample();
    //         //$row[0]->setName($row[0]->name);
    //         //var_dump($row);die();
    //         // foreach($q->result('User') as $rec){
    //         //     //echo $rec->name;
    //         //     $rec->setName($rec->name);
    //         //     echo $rec->sample();
    //         // }
    //         //$row[0]->sample();
    //     }

    // }

    // class User{
    //     public $id;
    //     public $name;
    //     public $username;
    //     public $password;
    //     public function setName($name){
    //         $this->name = $name;
    //     }
    //     public function sample(){
    //        return strrev($this->name);
    //     }
}
