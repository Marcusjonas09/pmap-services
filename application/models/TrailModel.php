<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TrailModel extends CI_Model {
    public function get_all(){
    	$q = $this->db->get('trails');
        return $q->result();
    }
}
