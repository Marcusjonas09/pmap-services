

<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Metroville Home Owners Associations
            <small class="pull-right"><?=date('m/d/Y')?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Metroville Subdivision</strong>
            <br>
            Alapan 1-C, MIA Road<br>
            City of Imus, Cavite<br>
            Phone: (+63) 123-5432<br>
            Email: admin@ehoa.com.ph
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?=$resident->fname?> <?=$resident->mname?> <?=$resident->lname?></strong><br>
            <?=$resident->lotno?><br>
            Phone: <?=$resident->cno?><br>
            Email: <?=$resident->email?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Request #<?=$fee->due_id?></b><br>
          <br>
          <b>Reference Code:</b> #<?=$fee->reference_code?><br>
          <b>Payment Due:</b> <?=$fee->due_date==NULL?'N/A':$fee->due_date;?><br>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
            Request: <?=$fee->due_type?>

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

            <?=$fee->due_description?>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due <?=$fee->due_date==NULL?'N/A':$fee->due_date;?></p>

          <div class="table-responsive">
            <table class="table">
              
              <tr>
                <th>Total:</th>
                <td><?=$fee->due_amount?></td>
              </tr>
              <?php if($fee->parking_date!=NULL): ?>
                  <tr><td>Parking License Until</td><td> <?=$fee->parking_date?></td></tr>
                <?php endif; ?>
              <tr>
                <td>
                  Receit
                </td>
                <td>
                  <img src="<?=base_url()?>uploads_payment/<?=$fee->image?>" width="200px" > <br/>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

