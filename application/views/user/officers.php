
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Page Content -->
        <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Officers
        <small>Metroville HomeOwner's Association</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?=base_url()?>user/index">Officers</a>
        </li>
        <li class="breadcrumb-item active">Lis</li>
      </ol>

      

      <!-- Team Members -->
      <h2>Our Team</h2>

      <div class="row">
      <?php foreach($members as $member): ?>



        <div class="col-lg-3 mb-3">
          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" style="height: 100px" src="<?=base_url()?>uploads_event/<?=$member->member_image?>" alt="User profile picture">
              <h3 class="profile-username text-center"><?=$member->member_name?></h3>
              <div class="well well-sm pre-scrollable" style="height: 150px;">
              <p class="text-muted text-center"><?=$member->member_description?></p>
              </div>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Position</b> <a class="pull-right"><?=$member->member_position?></a>
                </li>
              </ul>
              <a href="#" class="btn btn-primary btn-block"><b><?=$member->member_email?></b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


      <?php endforeach; ?>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
      </div>

    </div>
    <!-- .row -->



  </div>
</div>


<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
