
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Policy
        <small>Metroville HomeOwner's Association</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?=base_url()?>user/index">Home</a>
        </li>
        <li class="breadcrumb-item active">Policy</li>
      </ol>

      <!-- Intro Content -->
      <div class="row">
        <div class="col-lg-6">
          <img class="img-fluid rounded mb-4" width="100%" src="<?=base_url()?>uploads_event/<?=$cms->cms_image?>" alt="">
        </div>
        <div class="col-lg-6">
          <h1>
            <?=$cms->cms_title?>
            <p><small><?=$cms->cms_description?></small></p>
          </h1>

        </div>
      </div>
      <!-- /.row -->

      
    </div>
    <!-- /.container -->
      </div>

    </div>
    <!-- .row -->



  </div>
</div>


<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>