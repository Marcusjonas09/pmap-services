<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Resident
        <small>Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Residents</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Resident Record
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    USER PROFILE
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                            <?php if($this->session->flashdata('success')): ?>
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-thumbs-o-up"></i> You have updated the profile successfully</h4>
                                    
                                </div>
                            <?php endif; ?>

                               <?php if(validation_errors()): ?>
                                  <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                    <?=validation_errors()?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-blue"><h2>Edit Profile</h2></div>
                        
                        <form role="form" action="<?=base_url()?>user/update_resident" method="post">
                            <div class="col-lg-6">
                                <input type="hidden" name="r_id" value="<?=$resident->id?>">
                                <div class="form-group">
                                    <label>Residential Lot Number</label>
                                    <input value="<?=set_value('lotno',$resident->lotno)?>" name="lotno" class="form-control" placeholder="Enter Lot Number">
                                    <p class="help-block">Example: block 42-C.</p>
                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input value="<?=set_value('fname',$resident->fname)?>" name="fname" class="form-control" placeholder="Enter First Name">
                                </div>
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input  value="<?=set_value('mname',$resident->mname)?>" name="mname" class="form-control" placeholder="Enter Middle Name">
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input value="<?=set_value('lname',$resident->lname)?>" name="lname" class="form-control" placeholder="Enter Last Name">
                                </div>
                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            <div class="col-lg-6">
                               <div class="form-group">
                                <label>Date of Birth (yyyy-mm-dd)</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input readonly value="<?=set_value('dob',$resident->dob)?>" name="dob" class="form-control pull-right" id="datepicker" placeholder="Enter Date of Birth (yyyy-mm-dd)" >
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <div class="form-group">
                            <label>Mobile Number (+63xxxxxxxx)</label>
                            <input value="<?=set_value('cno',$resident->cno)?>" name="cno" class="form-control" placeholder="Enter Mobile Number (+939876543210)">
                            <p class="help-block">Example: +639876543210</p>
                        </div>
                        <button name="updateprofile" type="submit" class="btn btn-primary">Update Record</button>
                    </div>
                </form>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->

            <div class="row">
                <hr/>

                <div class="col-lg-6">
                    <h2 class="text-blue">Change Username and Password</h2>
                    <form role="form" action="<?=base_url()?>user/update_resident" method="post">
                        <input type="hidden" name="r_id" value="<?=$resident->id?>">
                        <div class="form-group">
                        <label>Username</label>
                            <input value="<?=set_value('uname',$resident->uname)?>" type="text" name="uname" class="form-control"  >
                        </div>
                        <!-- <div class="form-group">
                            <label>Old Password</label>
                            <input type="password" name="upass" class="form-control" placeholder="Old Password" >
                        </div>
 -->
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="upass" class="form-control" placeholder="Password">

                        </div>

                        <!-- <div class="form-group">
                            <label>Password Confirm</label>
                            <input type="password" name="upassnewconf" class="form-control" placeholder="Confirm Password">
                        </div> -->
                        <button name="updatepassword"  type="submit" class="btn btn-warning">Update Password</button>
                    </form>


                </div>

                <!-- /.col-lg-6 (nested) -->
                <div class="col-lg-3">
                    <h2 class="text-blue">Upload Picture</h2>
                    <form role="form" action="<?=base_url()?>user/update_resident" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="r_id" value="<?=$resident->id?>">
                        <div class="form-group">
                          <label for="exampleInputFile">Profile Picture</label>
                          <input name="profilepict" type="file" id="exampleInputFile">
                          <p class="help-block">Choose your profile picture to upload</p>
                        </div>
                      <button name="updatepicture" value="updatepicture" type="submit" class="btn btn-success">Upload Picture</button>
                  </form>
              </div>


              <div class="col-lg-3">
                    <img src="<?=base_url()?>uploads/<?=$resident->profilepict?>" class="img-responsive center-block img-rounded" width="200" alt="Responsive image">
                    
              </div>    
          </div>
          <!-- /.row (nested) -->



      </div>
      <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->




</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>