<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users fa-fw"></i> Complaints
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">complaints</li>
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    Complaints Records 
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <a href="<?=base_url()?>user/add_complaint" 
                            class="btn btn-success btn-md active" role="button"><i class="fa fa-plus"></i> File a Complaint</a>
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Subject</th>
                                        <th>Assisted by</th>
                                        <th>Status</th>
                                        <th>Date Filed</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($complaints as $a): ?>
                                    <tr>
                                        <td><?=$a->complaint_id?></td>
                                        <td><?=$a->reference_code?></td>
                                        
                                        <td><?=$a->complaint_subject?></td>
                                        
                                        <th><?=$a->assisted_by?></th>
                                        <td><?=$a->complaint_status?></td>
                                        <td>
                                            <?php 
                                              $date = date_create($a->created_at);
                                              echo date_format($date,"Y/m/d H:i:s");
                                            ?>
                                        </td>
                                        
                                        <td align="center">
                                            <!-- <a href="<?=base_url()?>admin/view_amenity/<?=$a->complaint_id?>"  class="btn btn-primary btn-circle btn-md"><i class="fa fa-search"></i>
                                            </a> -->
                                           
                                            <button type="button" value="<?=$a->complaint_id?>" data-requestid="<?=$a->complaint_id;?>" class="btn btn-info btn-modal-complaint" data-toggle="modal" data-target="#modal-info">
                                              <i class="fa fa-search"></i>
                                            </button>

                                            <?php if($a->complaint_status=="on process"): ?>
                                              <a onclick="deleteComplaint('<?=$a->complaint_id?>')"  class="btn btn-danger btn-circle btn-md"><i class="fa fa-trash-o tooltip_delete"></i>
                                              </a>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Subject</th>
                                        <th>Assisted by</th>
                                        <th>Status</th>
                                        <th>Date Filed</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </section>
    <!-- /.content -->

     <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>