
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <div class="row">
      <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
          </ol>

          <!-- Wrapper for slides -->
          <?php $isFirst = true ?>
          <div class="carousel-inner" role="listbox">
          <?php foreach($cms as $c): ?>
            <div class="item <?=$isFirst?'active':'';?>">
            <?php $isFirst = false; ?>
              <img style="height: 435px" src="<?=base_url()?>uploads_event/<?=$c->cms_image?>" alt="<?=$c->cms_title?>">
              <div class="carousel-caption">
                <?=$c->cms_description?>
              </div>
            </div>

            <?php endforeach;?>
            
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12">

        <div class="row">
        
        

          </div>

        </div>
        <!-- .row -->



      </div>
    </div>

  </div>


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
