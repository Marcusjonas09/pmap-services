
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Services
        <small></small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?=base_url()?>user/index">Home</a>
        </li>
        <li class="breadcrumb-item active">Services</li>
      </ol>

      <!-- Image Header -->
      <?php if(isset($cms->cms_image)): ?>
        <img class="img-fluid rounded mb-4" src="<?=base_url()?>uploads_event/<?=$cms->cms_image?>" 
        alt="<?=$cms->cms_title?>" style="width: 100%; padding-bottom: 10px;">
     <?php endif; ?>
      <!-- Marketing Icons Section -->
      <div class="row">
      <?php foreach($amenities as $a): ?>
        <div class="col-lg-4 mb-4">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <span class="username"><a href="#"><?=$a->name?></a></span>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="<?=base_url()?>uploads_event/<?=$a->image?>" alt="Photo">
              <p><?=$a->description?></p>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
      <?php endforeach; ?>  
      </div>
      <!-- /.row -->

      </div>
    </div>
    <!-- .row -->
  </div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
