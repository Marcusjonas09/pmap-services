<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Village Complaints
      <small>File Complaints</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>user/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>complaints</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="col-lg-12">

        <!-- Main content -->
        <section class="content">
          <div id="page-wrapper">
            <div class="row">
              <div class="col-lg-12">
                <h1 class="page-header">
                  Resident File Complaint
                </h1>
              </div>
              <!-- /.col-lg-12 -->
            </div>

            <div class="row">
              <div class="col-lg-12">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <a href="<?=base_url()?>user/complaint" type="submit" class="btn btn-success">Back</a>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-lg-6">

                        <form role="form" action="<?=base_url()?>user/do_add_complaint?>"  enctype="multipart/form-data" method="post">
                          <div class="form-group">
                            <label></label>
                          </div>

                          <?php if(validation_errors()): ?>
                            <div class="alert alert-danger alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                              <?=validation_errors()?>
                            </div>
                          <?php endif; ?>

                          <?php if($this->session->success): ?>
                            <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4>Success!</h4>
                              You have sucessfully filed a complaint. Your complaints will be forwarded to appropriate officer for investigation
                            </div>
                          <?php endif; ?>


                          
                          <div class="form-group">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control" value="<?=set_value('subject')?>" ></textarea>
                          </div>  

                          <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="textarea" 
                            placeholder="Enter complaint description here (include date, time and events)" 
                            style="width: 100%; height: 100px; font-size: 14px; line-h"><?=set_value('description')?></textarea>
                          </div>

                          <br/>

                          <div class="form-group">
                            <label for="exampleInputFile">Picture Evidence</label>
                            <input name="profilepict" type="file" id="exampleInputFile">
                            <p class="help-block">Choose image file to upload</p>
                          </div>
                          
                          <div class="form-group">
                            <button type="submit" name="add_complaint" value="true" class="btn btn-primary">SUBMIT COMPLAINT</button>
                          </div>
                        
                        </form>



                      </div>
                      
                      </div>
                      <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->



                  </div>
                  <!-- /.row -->






                </div>
                <!-- /.row (nested) -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


      </section>
      <!-- /.content -->







    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>