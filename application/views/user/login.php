
<div class="wrapper">

  <div class="container" style="padding-top: 50px; padding-bottom:250px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Please sign in</h3>
          </div>
            <div class="panel-body">
              <?php if(validation_errors()): ?>
                  <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                    <?=validation_errors()?>
                  </div>
              <?php endif; ?>


              <form action="<?=base_url()?>user/do_login" method="post" accept-charset="UTF-8" role="form">
                      <fieldset>
                  <div class="form-group">
                    <input class="form-control" placeholder="Username" name="uname" type="text">
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Password" name="upass" type="password">
                </div>
                <div class="checkbox">
                    <label>
                      <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                    </label>
                  </div>
                <input class="btn btn-md btn-primary btn-block" type="submit" value="Login">
              </fieldset>
                </form>
            </div>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>
  </div>

</div>



<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
