<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Certifications
      <small>Request Payment</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>user/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Certifications</li>
      <li class="active">Request Payment</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="col-lg-12">

        <!-- Main content -->
        <section class="content">
          <div id="page-wrapper">
          
            <div class="row">
              <div class="col-lg-12">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <a href="<?=base_url()?>user/certifications" type="submit" class="btn btn-success">Back</a>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-lg-6">

                        <form role="form" action="<?=base_url()?>user/do_request_payment_cert/<?=$cert->cert_id?>"  enctype="multipart/form-data" method="post">
                        <input type="hidden" value="<?=$cert->reference_code?>" name="reference_code" >
                        <input type="hidden" value="<?=$cert->cert_amount?>" name="amount_to_be_paid" >
                          <div class="form-group">
                            <label><img src="<?=base_url()?>assets/img/bpi.jpeg" ></label>
                            <br/>

                            Fill out a Deposit/Payment slip (available at any BPI regular branch nationwide) with the following information:
                            <br/><ul>
                            <li>MHOA Account Number : 1686-102256</li>
                            <li>Under Depositor’s/Merchant’s Name : Metroville Home Owners Association</li>
                          </ul>
                          <i>
                            Please pay only in cash. Only cash payment will be accepted by BPI regular branch.
                            Keep the Deposit/Payment slip which will serve as your Official Receipt.</i>

                          </div>

                          <?php if(validation_errors()): ?>
                            <div class="alert alert-danger alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                              <?=validation_errors()?>
                            </div>
                          <?php endif; ?>

                          <?php if($this->session->success): ?>
                            <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <h4>Success!</h4>
                              You have sucessfully submit your payment. Please wait for the confirmation.
                            </div>
                          <?php endif; ?>


                          <?php if($cert->image=="payment_default.png" && $cert->cert_status=="approved"): ?>
                          <div class="form-group">
                            <label>Payment Details</label>
                            <textarea name="payment_reference" class="textarea" 
                            placeholder="Enter payment description here (e.g. Payment reference number)" 
                            style="width: 100%; height: 100px; font-size: 14px; line-h"><?=set_value('payment_reference')?></textarea>
                          </div>

                          <div class="input-group">
                            <span class="input-group-addon">PhP</span>
                            <input value="<?=set_value('payment_amount',$cert->cert_amount)?>" type="text" name="payment_amount" class="form-control" placeholder="0.0" readonly>
                          </div>
                          <br/>
                          

                          <div class="form-group">
                            <label for="exampleInputFile">Payment Receit</label>
                            <input name="payment_image" type="file" id="exampleInputFile">
                            <p class="help-block">Choose image file to upload</p>
                          </div>
                          <input type="hidden" name="cert_id" value="<?=$cert->cert_id?>" >
                          <div class="form-group">
                            <button type="submit" name="add_amenity" value="true" class="btn btn-primary">ADD PAYMENT</button>
                          </div>
                        <?php endif; ?>
                        </form>



                      </div>


                      <div class="col-lg-6">
                        

                        <div class="form-group">
                          <label>NAME:<br/>
                            <?=$cert->cert_name?></label>

                          </div>
                          <div class="form-group">
                            <label>DESCRIPTION:<br/> <?=$cert->cert_description?></label>

                          </div>
                          <div class="form-group">
                            <label>DATE REQUESTED:<br/>  <?=$cert->created_at?></label>
                          </div>
                          

                          <div class="form-group">
                            <label>AMOUNT: <p class="text-red"> <?=$cert->cert_amount?></p></label>
                          </div>
                        </div>
                      </div>
                      <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->



                  </div>
                  <!-- /.row -->






                </div>
                <!-- /.row (nested) -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


      </section>
      <!-- /.content -->







    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>