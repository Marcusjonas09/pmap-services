
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    

      <!-- Marketing Icons Section -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3>REGISTER</h3>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                 <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                      </div>
                                  <?php endif; ?>
                                  <?php if($this->session->flashdata('registered')): ?>
                                      <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-envelope"></i> Thank You!</h4>
                                          <?=$this->session->flashdata('registered')?>
                                      </div>
                                  <?php endif; ?>
                            </div>
                        </div>
                        <form role="form" action="<?=base_url()?>user/do_register" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    
                                        <div class="form-group">
                                            <label>Residential Lot Number</label>
                                            <input value="<?=set_value('lotno')?>" name="lotno" class="form-control" placeholder="Enter Lot Number (Example: block 42-C.)">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input value="<?=set_value('fname')?>" name="fname" class="form-control" placeholder="Enter First Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Middle Name</label>
                                            <input  value="<?=set_value('mname')?>" name="mname" class="form-control" placeholder="Enter Middle Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input value="<?=set_value('lname')?>" name="lname" class="form-control" placeholder="Enter Last Name">
                                        </div>
                                         <div class="form-group">
                                            <label>Gender </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" id="gender" value="male" 
                                                <?php echo  set_radio('gender', 'male', TRUE); ?> > Male
                                            </label> 
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" id="gender" value="female"
                                                <?php echo  set_radio('gender', 'female'); ?> > Female
                                            </label>

                                            <label class="radio-inline"><b>Type</b> </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="type" value="tenant" checked> Tenant
                                            </label> 
                                            <label class="radio-inline">
                                                <input type="radio" name="type" value="owner"> Unit Owner
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="type" value="buyback"> Buy Back
                                            </label>

                                        </div>
                                        <div class="form-group">
                                            <label>Date of Birth (yyyy-mm-dd)</label>

                                            <div class="input-group date">
                                              <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                              </div>
                                              <input readonly value="<?=set_value('dob')?>" name="dob" class="form-control pull-right" id="datepicker" placeholder="Enter Date of Birth (yyyy-mm-dd)" >
                                        
                                              
                                            </div>
                                            <!-- /.input group -->
                                          </div>
                                          <!-- /.form group -->


                                          <div class="form-group">
                                            <label>Mobile Number (11 digits e.g. 0922xxxxxxx)</label>
                                            <input value="<?=set_value('cno')?>" name="cno" class="form-control" placeholder="Enter Mobile Number (09391234567)">
                                            
                                        </div>
                                        

                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input value="<?=set_value('email')?>" name="email" class="form-control" placeholder="Enter Email Address" >
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input name="uname" class="form-control" placeholder="Username" >
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                              <div class="col-md-6">
                                                <label>Password</label>
                                                  <input type="password" name="upass" class="form-control" placeholder="Password">
                                              </div>
                                              <div class="col-md-6">
                                                <label>Password Confirm</label>
                                            <input type="password" name="upass_confirmation" class="form-control" placeholder="Confirm Password">
                                              </div>

                                            </div>
                                            

                                        </div>

                                        <div class="form-group">
                                            <label>Car Description (<i>fill up if you owned a car</i>)</label>
                                            <textarea rows="6" class="form-control" name="car_description" 
                                            placeholder="Enter car description(plate number - model type): AAC 8314 - Hondo Civic White, XYA 1323 - Mitshubishi Montero ..."></textarea>
                                        </div>
                                        <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-8">
                                            <?php if($capt->settings_enable): ?>  
                                            <div class="g-recaptcha" data-sitekey="6Le9K2IUAAAAAFjhQbgC6Jmo7uTikZ1gld0lZeVI"></div>
                                            </div>
                                          <?php endif; ?>
                                            
                                          </div>
                                          <div class="col-md-4" style="padding-top: 40px;">
                                            <button type="submit" Value="Submit" class="btn btn-primary">Submit Registration</button>
                                          </div>
                                        </div>
                                        </div> 
                                        
                                </div>
                                 
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->



        </div>
        
        
      </div>
      <!-- /.row -->

      </div>
    </div>
    <!-- .row -->
  </div>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
