
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3">Contact Us
          <small></small>
        </h1>

        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?=base_url()?>user/index">Home</a>
          </li>
          <li class="breadcrumb-item active">Contact Us</li>
        </ol>
      </div>
    </div>



    <!-- Content Row -->
    <div class="row">
      <!-- Map Column -->
      <div class="col-lg-8 mb-4">
        <!-- Embedded Google Map -->

        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3864.297522895594!2d120.92142019981604!3d14.410004785534165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d31e65038c51%3A0x76a4f3beb3746abf!2sMetroville+Subdivision%2C+Imus%2C+Cavite!5e0!3m2!1sen!2sph!4v1521262954552" width="100%" height="400px" frameborder="0" marginheight="0" marginwidth="0" ></iframe>
      </div>
      <!-- Contact Details Column -->
      <div class="col-lg-4 mb-4">
        <!-- /. box -->
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Contact Details</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span class="fa fa-home"> </span> Brgy. Alapan 1-C
                Imus City, Cavite</a></li>
                <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span class="fa fa-phone"> </span>
                 (+63) 906-283-8811</a></li>
                 <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> <span class="fa fa-envelope"> </span> abandala@ehoaphillippines.com</a></li>
               </ul>
             </div>
             <!-- /.box-body -->
           </div>
           <!-- /.box -->



         </div>
       </div>
       <!-- /.row -->

       <!-- Contact Form -->
       <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
       <div class="row">
        <div class="col-lg-8 mb-4">
          
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compose New Message</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <input class="form-control" readonly placeholder="To: ehoa@gmail.com">
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Subject:">
              </div>
              <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 200px"></textarea>
              </div>
              
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-trash"></i> Reset</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>

      </div>
      <!-- /.row -->
    </div>
    <!-- /.row -->

  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
