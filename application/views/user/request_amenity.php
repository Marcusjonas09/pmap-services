<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Village Amenities
      <small>Request</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>user/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Amenities</li>
      <li class="active">Request</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="col-lg-12">

        <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
          <thead>
            <tr>
              <th>ID</th>
              <th>Reference Code</th>
              <th>Amenity</th>
              <th>Name</th>
              <th>Title</th>
              <th>Status</th>
              <th>Start Event</th>
              <th>End Event</th>
              <th>Date Filed</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($events as $a): ?>
              <tr>
              <td><?=$a->request_id?></td>r>
              <td><?=$a->reference_code?></td>

              <td><img src="<?=base_url()?>uploads_thumb/<?=$a->image?>" alt="<?=$a->name?>" class="img-rounded" width="50px"><br/>PhP <?=$a->price?></td>
              <td><?=$a->name?></td>
              <td><?=$a->title?></td>
              <?php
              $color = "text-red";
              if($a->status=="approved"){
                $color = "text-green";
              } else if($a->status=="pending") {
                $color = "text-orange";
              } 
              ?>
              <td><label class="<?=$color?>">
                
                <?php  if($a->status=='approved' && $a->with_payment && 
                $a->payment_status=='not paid' && $a->payment_image==""): ?>
                <a href="<?=base_url()?>user/request_payment/<?=$a->request_id?>">Click for Payment </a>
              <?php elseif($a->status=='approved' && $a->with_payment && 
              $a->payment_status=='not paid' && $a->payment_status=="not paid"): ?>
              Payment is for Review
            <?php else: ?>
              <?=$a->status?>
            <?php endif; ?>
          </label>
        </td>
        <td>
                <?php 
                $date = date_create($a->start);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
              <td>
                <?php 
                $date = date_create($a->end);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
              <td>
                <?php 
                $date = date_create($a->request_created_at);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
              <td align="center">
                
                <button type="button" value="<?=$a->id?>" data-requestid="<?=$a->request_id;?>" class="btn btn-info btn-modal" data-toggle="modal" data-target="#modal-info">
                  <i class="fa fa-search"></i>
                </button>

                <?php if($a->status != "approved"): ?>
                  <a onclick="deleteEvent('<?=$a->request_id?>')"  class="btn btn-danger btn-circle btn-md"><i class="fa fa-trash-o"></i>
                  </a>
                <?php endif; ?>

              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
      <!-- /.table-responsive -->
    </div>

    
    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>