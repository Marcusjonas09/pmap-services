

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Certifications
        <small>Request</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Certifications</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                xyz 
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">


        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    ABC
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                      </div>
                                  <?php endif; ?>

                            <?php if($this->session->flashdata('fee_success')): ?>
                                      <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-smile-o"></i> Success!</h4>
                                        You have added the fee.
                                      </div>
                                  <?php endif; ?>



                        <form role="form" action="<?=base_url()?>user/do_add_cert" method="post" >   
                              <!-- radio -->
			                <div class="form-group">
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios1" value="Proof of no derogatory record or file" checked>
			                      Proof of no derogatory record or file
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios2" value="Victim of calamity (fire, flashflood, earthquake, etc.)">
			                      Victim of calamity (fire, flashflood, earthquake, etc.)
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="As reference from (SSS, GSIS, PAGIBIG)" >
			                      As reference from (SSS, GSIS, PAGIBIG)
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="Application (Meralco, Maynilad, PLDT, Globle, Postal ID, Bank, Franchise)" >
			                      Application (Meralco, Maynilad, PLDT, Globle, Postal ID, Bank, Franchise)
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="Hospital Requirements" >
			                      Hospital Requirements
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="Local Employment" >
			                      Local Employment
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="Proof of Indigence" >
			                      Proof of Indigence
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label>
			                      <input type="radio" name="name" id="optionsRadios3" value="others" >
			                      Others
			                    </label>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label>Others</label>
			                  <input type="text" name="other_name" class="form-control" placeholder="Enter other description here...">
			                </div>

                              <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" placeholder="Place certification request description here"
                                style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=set_value('description')?></textarea>

                            </div>

                            
                        <!-- /.form group -->
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Request Certifications</button>
                         </div>

                    </form>
                            
                        </div>

                        
                           
                            
                    </div>
                    <div class="row">
                        
                        
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                        <div class="panel-heading">
                            Certification Request 
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Date Requested</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php foreach($certs as $r): ?>
                                    <tr>
                                        <td><?=$r->cert_id?></td>
                                        <td><?=$r->reference_code?></td>
                                        <td>
                                            <?=$r->cert_name?>
                                        </td>
                                        <td><?=$r->cert_amount?></td>
                                        <td>
                                        <?php if($r->is_paid): ?>                                        
                                            <p class="text-green">Approved</p>

                                        <?php elseif($r->image!='payment_default.png'): ?>
                                            <p class="text-orange">
                                            Your payment has been submitted under review
                                            </p>
                                            <?php elseif($r->cert_status=="approved" && !$r->is_paid): ?>
                                            Request has been approved for payment 
                                            <a href="<?=base_url()?>user/request_payment_cert/<?=$r->cert_id?>"  class="btn btn-primary btn-circle btn-md"><i class="fa fa-credit-card"></i> PAYMENT
                                            </a>
                                            <?php else: ?>    
                                                <?=$r->cert_status?>        
                                            <?php endif; ?>
                                        

                                        </td>  
                                        <td><?=$r->created_at?></td> 
                                        
                                        <td align="center">
                                          
                                            <button type="button" value="<?=$r->cert_id?>" data-requestid="<?=$r->cert_id;?>" class="btn btn-info btn-modal-cert" data-toggle="modal" data-target="#modal-info">
                                              <i class="fa fa-search"></i>
                                            </button>
                                        
                                            <a onclick="deleteCert('<?=$r->cert_id?>',<?=$r->cert_id?>)"  class="btn btn-danger btn-circle btn-md"><i class="fa fa-trash"></i> 
                                            </a>

                                            
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Date Requested</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

        </div>
    </div>  


    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


    
</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>