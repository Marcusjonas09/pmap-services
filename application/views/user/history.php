<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Histories
      <small>Request History</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>user/home"><i class="fa fa-dashboard"></i> History</a></li>
      <li>List</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="col-lg-12">
        <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
          <thead>
            <tr>
              <th>Action</th>
              <th>description</th>
              <th>Created</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($histories as $r): ?>
              <tr>
                <td><?=$r->history_action?></td>
                <td><?=$r->history_description?></td>
              <td>
                <?php 
                $date = date_create($r->created_at);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
              <td align="center">
                <a onclick="deleteHistory('<?=$r->history_id?>')"  class="btn btn-danger btn-circle btn-md"><i class="fa fa-trash-o"></i>
                  </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
      <!-- /.table-responsive -->
    </div>

    
    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>