<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Village Amenities
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Amenities</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
          <?php foreach($amenities as $a): ?>
        <div class="col-lg-4 mb-4">
          <!-- Box Comment -->
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <span class="username"><a href="#"><?=$a->name?></a></span> 
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive pad" src="<?=base_url()?>uploads_event/<?=$a->image?>" alt="Photo">
              
              <p>(Price: <?=$a->price?>) <?=$a->description?></p>
              <a href="<?=base_url()?>user/request_amenity/<?=$a->id?>" class="btn btn-primary"><span class="fa fa-star"></span> Request</a>  
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
      <?php endforeach; ?>  
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>