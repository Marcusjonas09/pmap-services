<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Resident
        <small>Fees Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Residents</li>
        <li class="active">Fee</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                View Resident Record Fee  
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    

    <div class="row">
        <div class="col-lg-9">
            <div class="panel panel-primary">
                        <div class="panel-heading">
                            Fees
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <?php foreach($fees as $r): ?>
                                    <?php $color = $r->is_paid?"text-green":"text-red";?> 
                                    <tr class="<?=$color?>">
                                        <td><?=$r->due_id?></td>
                                        <td><?=$r->reference_code?></td>
                                        <td>
                                        <?php
                                        $type = "";
                                        if($r->due_type=="parking"){
                                            $date = date_create($r->parking_date);
                                            $type = 'Parking ';
                                            $type .= date_format($date,"Y/m/d");
                                        } else if($r->due_type=="monthlydue"){
                                            $type = 'Monthly Dues';
                                        } else if($r->due_type!=''){
                                            $type = $r->due_type;

                                        }
                                        else {
                                            $type = 'Others';
                                        }
                                        ?>
                                        <?=$type?>
                                        </td>
                                        <td><b>
                                        <?=$r->due_amount?>
                                        </b>
                                        </td>
                                        <td>
                                        <?php 
                                          $date = date_create($r->due_date);
                                          echo date_format($date,"Y/m/d");
                                        ?>
                                        </td>
                                        <td>
                                        <?php if($r->is_paid): ?>
                                            <p class='text-green'>PAID</p>
                                        <?php elseif($r->image!="fee_default.png"): ?>
                                            <p class='text-blue'>WAITING TO CONFIRMED</p>
                                        <?php else: ?>
                                            <p class='text-red'>NOT PAID</p>
                                        <?php endif; ?>    
                                        </td>
                                        
                                        <td align="left">
                                          
                                            <button type="button" value="<?=$r->due_id?>" data-requestid="<?=$r->due_id;?>" class="btn btn-info btn-modal-fee" data-toggle="modal" data-target="#modal-info">
                                              <i class="fa fa-search"></i>
                                            </button>
                                            <?php if($r->is_paid): ?>
                                            <a href="<?=base_url()?>user/print_fee/<?=$r->due_id?>"  class="btn btn-success btn-circle btn-md"><i class="fa fa-print"></i>
                                            </a>
                                        <?php endif; ?>
                                            
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Reference Code</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

        </div>


        <div class="col-md-3">
                         <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="box-title">SUMMARY</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <img class="profile-user-img img-responsive img-circle" src="<?=base_url()?>uploads_thumb/<?=$this->session->image?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?=$this->session->name?></h3>
              <hr>
              <strong class="text-green"><i class="fa fa-arrow-circle-down margin-r-5"></i>CURRENT TOTAL PAID</strong>
              <p class="text-muted">
                <h2 class="text-green"><i class="fa fa-rub margin-r-5"></i> <?=number_format($resident->fees_collected,2)?></h2>
              </p>

              <hr>

              <strong class="text-red"><i class="fa fa-arrow-circle-up margin-r-5"></i>TOTAL AMOUNT BALANCE</strong>

              <p class="text-muted"><h2 class="text-red"><i class="fa fa-rub margin-r-5"></i> <?=number_format($resident->fees_unpaid,2)?></h2></p>

              <hr>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

                    </div>
                    <!-- /.col-md-4 -->
    </div>  


    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


    
</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>