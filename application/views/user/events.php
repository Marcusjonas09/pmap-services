
<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <div class="row">
      <div class="col-md-12">
        <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Activities / Events
        <small>Metroville HomeOwner's Association</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?=base_url()?>user/index">Home</a>
        </li>
        <li class="breadcrumb-item active">About</li>
      </ol>
      </div>
    </div>

    <!-- row -->
    <div class="row">
      <div class="col-md-12">

        <div class="row">
        
        <?php $cnt=0; foreach($events as $event): ?>
          <!-- <?=$cnt%3?'<div class="row">':'';?> -->
          <div class="col-lg-6 mb-6">
           <!-- Main content -->
           <section class="content">
            <!-- Default box -->
            <div class="box">
              <div class="box-header with-border">
                <img style="display:block; margin:auto;" class="img-responsive pad" src="<?=base_url()?>uploads_event/<?=$event->info_image?>" alt="Photo">
                <h3 class="box-title"><?=$event->info_title?></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
                  <i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                <div class="well well-sm pre-scrollable" style="height: 150px;">
                  <?=$event->info_desc?>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="#" class="btn btn-primary">Learn More</a>
                </div>
                <!-- /.box-footer-->
              </div>
              <!-- /.box -->
            </section>
            <!-- /.content -->
          </div>
          <!-- <?=$cnt%3?'</div>':'';?> -->
          <?php $cnt++ ?>
        <?php endforeach; ?>
           </div> 

          </div>

        </div>
        <!-- .row -->



      </div>
    </div>

  </div>


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
