






<div class="content-wrapper">

  <div class="container" style="padding-top: 5px; padding-bottom:5px; background-image: url('<?=base_url()?>assets/img/admin_bg3.jpg'); ">

    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3">Announcements
        <small>Metroville HomeOwner's Association</small>
      </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?=base_url()?>user/index">Home</a>
        </li>
        <li class="breadcrumb-item active">About</li>
      </ol>

      <!-- Intro Content -->
      <?php if(count($cms)>0): ?>
      <?php foreach($cms as $c): ?>
      
      <div class="row">
        <div class="col-lg-6">
          <img class="img-fluid rounded mb-4" width="100%" src="<?=base_url()?>uploads_event/<?=$c->cms_image?>" alt="">
        </div>
        <div class="col-lg-6">
          <h1>
            <?=$c->cms_title?>
            <p><small><?=$c->cms_description?></small></p>
          </h1>

        </div>
      </div>
      <!-- /.row -->
      <?php endforeach; ?>
    <?php endif; ?>
      
    </div>
    <!-- /.container -->
      </div>

    </div>
    <!-- .row -->



  </div>
</div>


<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>
