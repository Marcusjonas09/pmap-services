<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Amenity
        <small>Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Amenities</li>
        <li class="active">View</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Amenity Record View
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a href="<?=base_url()?>user/amenities" type="submit" class="btn btn-success">Back</a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label style=" padding-top: 50px;">Name</label>
                                <input class="form-control" value="<?=$amenity->name?>" readonly>
                            </div>

                            <div class="form-group">
                                <label>Created at</label>
                                <input class="form-control" value="<?=$amenity->created_at?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Description (Price: <?=$amenity->price?>)</label>
                                <?=$amenity->description?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <img src="<?=base_url()?>uploads_event/<?=$amenity->image?>" class="img-responsive center-block img-rounded" style=" padding-top: 50px;" width="200" alt="Responsive image">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                        <!-- Content Wrapper. Contains page content -->
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="box">
                                            <div class="box-header">
                                          <h3 class="box-title">Amenity Schedule
                                            <small>Set Amenity Schedule</small>
                                            </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="text-blue">ADD SCHEDULE</h2>
                                        <?php if($this->session->flashdata('error')): ?>
                                          <div class="alert alert-danger alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                            <?=$this->session->flashdata('error')?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if($this->session->flashdata('success')): ?>
                                          <div class="alert alert-success alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h4><i class="icon fa fa-warning"></i> Request Added!</h4>
                                            Your request has been added. You may check all your request
                                            <a href="<?=base_url()?>user/request" >
                                            here</a>

                                        </div>
                                    <?php endif; ?>


                                            <form role="form" action="<?=base_url()?>user/add_request_amenity/<?=$amenity->id?>" method="post">

                                                <!-- row -->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                            <input placeholder="Enter Title" class="form-control" value="<?=set_value('title')?>" type="text" name="title" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                 <!-- Date and time range -->
                                                  <div class="form-group">
                                                    <label>Date and time range:</label>

                                                    <div class="input-group">
                                                      <div class="input-group-addon">
                                                        <i class="fa fa-clock-o"></i>
                                                      </div>
                                                      <input name="schedule_date" type="text" class="form-control pull-right" id="reservationtime">
                                                    </div>
                                                    <!-- /.input group -->
                                                  </div>
                                                  <!-- /.form group -->
                                                  </div>

                                                  <div class="col-md-3">
                                                        <!-- Color Picker -->
                                                      <div class="form-group">
                                                        <label>Bar Color</label>

                                                        <div class="input-group my-colorpicker2">
                                                          <input value="<?=set_value('color')?>" name="color" type="text" class="form-control">

                                                          <div class="input-group-addon">
                                                            <i></i>
                                                          </div>
                                                        </div>
                                                        <!-- /.input group -->
                                                      </div>
                                                      <!-- /.form group -->
                                                    </div>
                                                


                                                </button>
                                                </div> <!-- .row-->
                                                <div class="row"><!-- row -->
                                                    <div class="col-md-10">
                                                        <!-- textarea -->
                                                        <div class="form-group">
                                                          <label>Description</label>
                                                          <textarea class="form-control" name="description" rows="3" placeholder="Enter Description"><?=set_value('description')?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label>&nbsp;</label>
                                                            <button name="submit_event" value="true" type="submit" class="btn btn-warning form-control">ADD SCHEDULE            
                                                        </div>
                                                    </div>
                                                </div> <!-- .row-->   

                                            </form>
                                    </div>
                                </div>


                        




                        <div class="row">

                            <!-- /.col -->
                            <div class="col-md-12">
                              <div class="box box-primary">
                                <div class="box-body no-padding">
                                  <!-- THE CALENDAR -->
                                  <div id="calendar"></div>
                              </div>
                              <!-- /.box-body -->
                          </div>
                          <!-- /. box -->
                      </div>
                      <!-- /.col -->
                  </div>
                  <!-- /.row -->
              </section>
              <!-- /.content -->


          </div>
          <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->



</div>
<!-- /.row -->

</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
</div>>


<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>