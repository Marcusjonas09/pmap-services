<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users fa-fw"></i> FAQs
        <small>Frequently Asked Questions</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> FAQs</a></li>
        
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div id="page-wrapper">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            eHOA Frequestly Asked Questions
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Category</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Posted By</th>
                                        <th>Date Posted</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($faqs as $a): ?>
                                    <tr>
                                        <td><?=$a->faq_id?></td>
                                        <td><?=$a->faq_category?></td>
                                        <td><?=$a->faq_question?></td>
                                        <td><?=$a->faq_answer?></td>
                                        <th><?=$a->posted_by?></th>
                                        <td>
                                            <?php 
                                              $date = date_create($a->created_at);
                                              echo date_format($date,"Y/m/d H:i:s");
                                            ?>
                                        </td>
                                        <td align="center">
                                       
                                           
                                        <button type="button" value="<?=$a->faq_id?>" data-requestid="<?=$a->faq_id;?>" class="btn btn-info btn-modal-faq tooltip_view" data-toggle="modal" data-target="#modal-info">
                  <i class="fa fa-search"></i>
                </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Category</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Posted By</th>
                                        <th>Date Posted</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </section>
    <!-- /.content -->



<div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->




  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>