


<html xmlns="http://www.w3.org/1999/xhtml">
<head><link href="../Stylesheets/styleV9.css" rel="Stylesheet" type="text/css" /><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta name="HandheldFriendly" content="true" /><title>
	Learning Systems Portal
</title>

    <script src="//www.gmetrix.net/Script/jquery-1.11.3.min.js"></script>
    <link href="//www.gmetrix.net/Content/themes/base/jquery-ui.min.css" rel="stylesheet" />
    <script src="//www.gmetrix.net/Scripts/jquery-ui-1.12.1.min.js"></script>
    <script src="//www.gmetrix.net/Script/ScriptV11.js"></script>
    <script src="//www.gmetrix.net/Assets/js/bootstrap.min.js"></script>
    <link href="//www.gmetrix.net/Assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//www.gmetrix.net/Stylesheets/FontAwesome/fontawesome-all.min.css" rel="stylesheet" />
    <link href="//www.gmetrix.net/Stylesheets/dashboard.css" rel="stylesheet" />
    <link id="customStyleSheet" rel="stylesheet" type="text/css" /><link href="../MasterPages/GMetrixSMS.png" rel="shortcut icon" />
    <style>
        .mainContainer {
            margin-top: 60px;
            padding: 0;
            margin-bottom: 20px;
        }

        @media (max-width:767px) {
            .mainContainer {
                margin-top: 100px;
            }
        }
    </style>

    
    <script type="text/javascript" src="//www.gmetrix.net/Script/TimerV2.js"></script>
    

    
    <script type="text/javascript" src="//www.gmetrix.net/Controls/SharedControlFunctions.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/MultipleChoiceControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/DragAndMatchControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/DragToParagraphControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/TrueFalseControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/DropDownControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/ShortAnswerControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/SimulationControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/SkormControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/FillInTheBlankControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/ParagraphDropDownControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Controls/CategorizeControl.js"></script>
    <script type="text/javascript" src="//www.gmetrix.net/Script/PageFunctions.js"></script>
    <script>

        var firstload = 1;
        window.onload = function () {

            if(firstload == 1) {
                setTimeout(function () {
                    firstload = 0;
                        document.getElementById("loadingDiv").style.display = "none";
                        document.getElementById("questionContentWrapper").style.display = "block";
                        instructionContainerResizer();
                        skormReset();
         
                }, 5000);
            }
       


            //Start the timer like normal for linear question types but for skorm and simulation, the control will handle starting timer
            //if ("10" !== "8") {
                var mode = "Testing";
                var minutesSpent = 7;
                var secondsSpent = 4;
                var allowedTime = 60;
                var display = 'timer';
                startTimer(minutesSpent,secondsSpent,allowedTime, display, mode);
            //}

            SetUserAnswers(document.getElementById("CurrentQuestionType").value);
        };

    </script>
      <script>
        
            function changeTextSize() {
                var textSize = document.getElementById('contentMain_TextSize').value;

                if (textSize == "nothing" || textSize == "13px") {
                    $('#contentMain_TextSize').val("17px");
                    document.getElementById("contentMain_InstructionText").style.fontSize = "17px";
                    document.getElementById("contentMain_lblHelp").style.fontSize = "17px";
                    document.getElementById("contentMain_answerContainer").style.fontSize = "17px";
                } 
                
                else if (textSize == "17px") {
                     $('#contentMain_TextSize').val("22px");
                    document.getElementById("contentMain_InstructionText").style.fontSize = "22px";
                    document.getElementById("contentMain_lblHelp").style.fontSize = "22px";
                    document.getElementById("contentMain_answerContainer").style.fontSize = "22px";
                }
                else if (textSize == "22px") {
                     $('#contentMain_TextSize').val("26px");
                    document.getElementById("contentMain_InstructionText").style.fontSize = "26px";
                    document.getElementById("contentMain_lblHelp").style.fontSize = "26px";
                    document.getElementById("contentMain_answerContainer").style.fontSize = "26px";
                }
                else if (textSize == "26px") {
                     $('#contentMain_TextSize').val("31px");
                    document.getElementById("contentMain_InstructionText").style.fontSize = "31px";
                    document.getElementById("contentMain_lblHelp").style.fontSize = "31px";
                    document.getElementById("contentMain_answerContainer").style.fontSize = "31px";
                }
                else if (textSize == "31px") {
                     $('#contentMain_TextSize').val("13px");
                    document.getElementById("contentMain_InstructionText").style.fontSize = "13px";
                    document.getElementById("contentMain_lblHelp").style.fontSize = "13px";
                    document.getElementById("contentMain_answerContainer").style.fontSize = "13px";
                }
            };
   </script>
    <style>
       

        .incorrectAnswer {
            border: 1px solid red;
            background-color: #ffcccc;
            padding: 20px;
            font-size: 16px;
            font-weight: bold;
            margin-top: 5px;
        }

        .correctAnswer {
            border: 1px solid green;
            background-color: #d6fee2;
            padding: 20px;
            font-size: 16px;
            font-weight: bold;
            margin-top: 5px;
        }

        .answerIcon {
            float: left;
            position: relative;
            bottom: 15px;
            right: 11px;
        }

        .explanationText {
            border: 1px solid #666666;
            background-color: #F3F3F3;
            padding: 20px;
            margin-top: 10px;
            line-height: 16px;
        }

        .LoadingTest {
            font-family: "Eras Medium ETC regular", Georgia, Serif;
            font-size: 24px;
            margin: auto;
            width: 200px;
        }

        #instructionBox { 
            max-height: 65%; 
            min-height: 5%; 
            width: 100%;

        }

        #instructionBox .instructionContainer{
            width: 100%;
            overflow-y: auto;
        }

        .ui-widget-content {
            border: none;
            background: #ffffff;
            color: #333333;

        }

        #helpTextModal{
            cursor: move;
            position: absolute;
            width: 40%;
            height: 20%;
            min-width: 300px;
            min-height: 75px;
            top: 45px;
            right: 0px;
            z-index: 100;
        }

        #helpTextModal #overflowController{
            width: 100%;          
            overflow: auto;
            height: 100%;
            padding-right: 5px;
        }

        .closeHelpModal{
            position: absolute;
            vertical-align: middle;
            text-align: center;
            top: 5px;
            right: 15px;
            cursor: pointer;
            width: 20px;
            height: 20px;
           
        }

        #resizeIcons{
            cursor: row-resize;
            height: 1px;
            background-color: #B4B4B4;
            text-align: center;
            margin-top: -15px;
        }



        .instructionContainer::-webkit-scrollbar {
            width: 3px;
        }

        /* Track */
        .instructionContainer::-webkit-scrollbar-track {
            background: grey;
            border: 4px solid transparent;
            margin-bottom: 4px;
            margin-top: 4px;
        }

        /* Handle */
        .instructionContainer::-webkit-scrollbar-thumb {
            background: #7EA1DE;
        }

        #overflowController::-webkit-scrollbar {
            width: 3px;
        }

        /* Track */
        #overflowController::-webkit-scrollbar-track {
            background: grey;
            border: 4px solid transparent;
            margin-bottom: 8px;
        }

        /* Handle */
        #overflowController::-webkit-scrollbar-thumb {
            background: #7EA1DE;
        }

        .mainContainerNew {
            /*height: 100%;*/
        }
    </style>



</head>
<body>
    <form method="post" action="./RunUnityTest.aspx" id="frmGmetrix">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="7g+nNw0VbAJdPXEQuyJndEupAuhdbo6+i8Cv4e2inpL2bxUKrWzrLq0HO5KH3+/iD1r/jfib5kpCyCQPjy5uv8FsdFPerDb8Sd++RxMbIo+fwWpKlTI2lmD+wh3TJi/Vj7IpD9NJ/GJzq8rnnAzvQ5QXE9qUEHXK6ptVKxsahVzKexEMoW2tlRRdXCnEiWsdp1OqzXVEoBc7OZPlhpDPxRiIIK8ix7HK5DCNE1EAuELihWzv9KXSCsuvVo4pxRymrehOOtx9soWT5/GxHUJsmj47spVTqHZZEqHx1o14LXN2vRrtL3rndbiWZ7wy7nF2WO9KthfJjej6LTpfmDxsvbP7M84+/68bMTMeLDN8U8ZVY/+teB94gl6fUSJAI2tNhworSm46XXIkgjtahTl5liJp7sGtN338HCY8WMl6hV2jAI3I+t6WIbS28s5abdlviZtqxcWI8m636t3qoRy2gjzbNGd8ZOPu8+rooHaAt4eOFL0VMEdmvgJg7tAWjSTLKCQFkNFqJazXfvHOQaJLEh2LscbRhg6t20TwXSe/B6tyBHjl4r2fbJGdmhpTA4EWhupgZyJiqwoMm1EotpxGITUSh57cqgcOEsPmtJwMvVp0w/o/7/0XTWCkfinQ9b8ln1rFtrx8DzITL3q6NvnKRu5HZukpCFanxYyDm02PtCXmvxJSAosopimAqIN4htwpBs0jn0TZJszVZx3zaGcEevG4FYnZlkU6JOelrh43RNfcOEESX8uF1/3oJw9Rt7x/vIaiTAHPNUnLLZxmAjU5MFMT9Bd018soG7Z9gxZQO6/ehyF2AyFvx4pof0eAPFWZZ4ah5FP2dQSROmgFZo+Ju0mamZNoSsiGy4AB2W0LDIk6fCQu2TDts/HnU3H1zI5OZwl3ipbHRfmAmJaN0k/YmFQ+XuCFI+bCq2gNb4u4NQ+lk1EmT/uPEy+j1PC2kEzIWV9k/ShLsqowJtqBa5LkKEg/EhtU8LkvtGuXRoqLq3dz18AtdpVGgsGUikgZyXadU7vAmWgLzIr7r+S6cGkW9WKNZiYlDV46jf20n67WH0ynJ/1rbIBu3tZJtYoK8gaekEkEmPNXpzRF+f5ozNzSY53ttKt4euYmIrllgY+/SK0hbAg6QwK/mEKZL0rPpNL0i2NAYzvjTX8cepnA/XG5oPFa0Qv6NDl4ZVs6xg6qdxtbbVi+X2waqkBgmczcGMqSxJxFjPkxrzZbq5rbwyHdOdRCUKH3ZPTrIXGi5iberkIBQ3dF8+0E73gyT0SyC7rxPbgr6FL/raCZ9ixsniFO9+BbsnsEYP814cMHssvuYoShC3ohk69R36mCu37ZGc7WFb+S/gopzaBIV3LZl6PolGnT2MGH3csAMlUWBGG2VbgXNYUAlW9e1j7UqWQgqPq2vO4+pkLQUwgqFh/YENm+awUy/hNGehXUdXiWU7bx/liclpz1Tj6jUT3cLsN4USmoegwbDXe2WD0Ar2rj2jdkAmDcb37EaWWNMyHPO6LoLh1M82UtUpfxmHC3Q/raDeSi6nx74kK+7KpYGWnRCgWuLfBEzxZLNAeES1DlNHujQ3JpLhlNwirEB7/9CrBLCB9sFJgT5c4MCOayvaM2BeYtQh463TT0l40Mo96FEIBtazcUO2jVYFbNy/mVOP6raj3gFPKd46xGFBsqrhWtn4FRLS4PWVTSoubSL4TAiQXUkN5912K/xc1Ag36VyO2y7TytzVZt2l6roo4yyryjrLR75A1h9n5TMqs2WfQ0d5RF6e6GBorq9ClMrxOXivbTsRzWwgsr3kSTfE36dFyihpK7Dk51dzbbRec+PrbZKWwlzUE=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['frmGmetrix'];
if (!theForm) {
    theForm = document.frmGmetrix;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZG0T3JKEzdwZ5HfsWnY8RbSQLIHNuRP85zrJRl5B9sUQduZ2UleEOjuQZQnnI3y8oQ2&amp;t=636093688341014055" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=NJmAwtEo3Ipnlaxl6CMhvoLBS94W3o1Fl_FV-F7V-iu4UKCCjOMm3UJrgy-JykDiE1Zy9CbLs17_qH1gfFvnpu2q-l9_3C_hVtTpl6gYr4YAzPGzkxNtdzJCP4LxVV1ZMwFSD_Bn01b2px_Ccv_wa2un19nQeh2hyMJ0-TZihes1&amp;t=ffffffffcc58dd65" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=dwY9oWetJoJoVpgL6Zq8OGDAhKfeSXNOcCGhFXEVTwdaK4Z8ikVVjnexsjC2oVc1jy3RU2-HIRA-D3tO872B-JtPQkde1BsDXeP3G3ulph8LZHZ2nh65O8zKy8LMEtbqF0v0FJ7bQFvVfQVbEFpdSOxLMEw8RNOMFh4E0cK3XkQ1&amp;t=ffffffffcc58dd65" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="29D71069" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="4YxsFNWbrE+WGX5lV7xtcDK/uovZoMWGcCTYaaI5Ehhz2W9PJ8xz0gkETiDfqLEYAVid7H3JNo1CACjNJxb/TnpM40J5RGcP1nksQEVvTD7IcaJF1YHMQaWNyiynK3JUlpclOJ+1yEOVEXg40ba+KLH6biTkhh+C9AhC8PSDclgOG12pro7qWlMZBtpNAFS4OGGQREn+kSQq1h05Yq828H5/UzdYu8BShp9Wb5axAYbOrhLipIFwIrqvfG/4Rt/NgTxTuco4GkJRHHOEfBr4oF0K5wJaaLfrqJQOpoW1UK9YjLPhepc1xbQDf/9nSIHSQApc/IBPqhoz2M2A53JZNTMEbbdW+4LsmhND770e//r6DN+1Mnacez2R6AiUUKaidXZqwD+kMb+kRQdpU1XtpBCNClUkujqgclfHR5Oi0DkueTz8DwSAruO7Kr02Dr55FW17TF1b3Ul3tuP4hR6gHIbhrzmT7GCLU+9CaAP+7JWKG5Mu5tr4s6h3WlfWfW1vKz8pnDAFztBQq/3iSwevxYqsrVOUtZrlWf9rBxhbAx1uAdZn2qfMXFRwgnSw9shKqpgQsPgHghHTtqvvaI2ZWRheOO8PvcC/ijNtpGo+u7xUQA0c/JDJwLhaJBzFKkzBlhE8e6+VP+9h7NG3FGBduTDg+aVc1kbwLQFRu6/TEcGb/89v41mzo6xRchE4uDK4H419xmeWBhsbY631eWsJPOWkyjjmBOiLBzk+nGx1CYpHqf5egIUeJVhxJZYvof1O+URAZWLX9h4VV57lG/KrcXiL9/aU9vrfVHyNP3afrTw=" />
</div>
        <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl07', 'frmGmetrix', ['tctl00$contentMain$QuestionContent','contentMain_QuestionContent'], ['ctl00$contentMain$resetButton','contentMain_resetButton','ctl00$contentMain$previousButton','contentMain_previousButton','ctl00$contentMain$skipButton','contentMain_skipButton','ctl00$contentMain$nextButton','contentMain_nextButton','ctl00$contentMain$FeedbackPreviousButton','contentMain_FeedbackPreviousButton','ctl00$contentMain$FeedbackNextButton','contentMain_FeedbackNextButton','ctl00$contentMain$btnTwoNext','btnTwoNext','ctl00$contentMain$btnTwoSkip','btnTwoSkip','ctl00$contentMain$btnTwoBack','btnTwoBack'], [], 90, 'ctl00');
//]]>
</script>

        <div class="mainContainer mainContainerNew">
            
            
    
    <div id="questionContentWrapper" class="mainContainerNew" style="display: none">
    <div id="contentMain_QuestionContent">
	
            <div id="hiddenOverlay"style="width:100%; height:100%; background-color:grey; position:absolute; left:0; top: 0;opacity: 0; visibility:hidden" ></div>
            <div id="helpTextModal" class="helpPopOut noSelect" style="display: none;">
                <div id="overflowController">
                    <div class="closeHelpModal" onclick="return toggleHelp();"><b>x</b></div>
                    <div id="lblHelpHead">
                        Help:
                    </div>
                    <span id="contentMain_lblHelp" style="font-size:13px;"></span>
                </div>
            </div>

            <div id="QuestionDiv" class="questionContainer separateTop">
                <input type="hidden" name="ctl00$contentMain$hidden" id="hidden" />
                <input type="hidden" name="ctl00$contentMain$CurrentQuestionId" id="CurrentQuestionId" value="32126" />
                <input type="hidden" name="ctl00$contentMain$CurrentQuestionType" id="CurrentQuestionType" value="10" />
                <input type="hidden" name="ctl00$contentMain$CurrentQuestionSceneName" id="CurrentQuestionSceneName" />
                <input type="hidden" name="ctl00$contentMain$IframeQid" id="IframeQid" value="0" />
                <input type="hidden" name="ctl00$contentMain$CurrentQuestionNumToSelect" id="CurrentQuestionNumToSelect" value="0" />
                <input type="hidden" name="ctl00$contentMain$LanguageId" id="LanguageId" value="1" />
                <input type="hidden" name="ctl00$contentMain$CurrentQuestionSource" id="CurrentQuestionSource" />


                <div class="questionMenu">
                    <div class="questionTopMenu">
                        <div class="gmetrixTestTitle notVisiblePhone"></div>
                        
                        <img class="leftMenuItem hoverCursor" style="bottom: 16px;" src="../Images/SaveButton.png" data-toggle="modal" data-target="#savedSuccessfully" />
                        <input type="image" name="ctl00$contentMain$ctl03" class="leftMenuItem" src="../Images/ListButton.png" />
                        <div class="leftMenuItem leftMenuQuestionInfo">
		
                            <span id="contentMain_QuestionInfo">7/40 (ID: 32126)</span>
                        
	</div>
                         <div id="textChange" class="textSizeContainer leftMenuItem" onclick="changeTextSize()">
                            <span class="bigA">A</span>
                            <span class="mediumA">A</span>
                            <span class="smallA">A</span>
                        </div>
                    <input type="hidden" name="ctl00$contentMain$TextSize" id="contentMain_TextSize" value="13px" />
                    </div>
                    <div class="questionBottomMenu">
                        <span class="practiceTestCheckBox notVisiblePhone"><input id="contentMain_reviewCheckbox" type="checkbox" name="ctl00$contentMain$reviewCheckbox" /><label for="contentMain_reviewCheckbox">Mark For Review</label></span>
                        <a onclick="skormReset();" id="contentMain_resetButton" class="testQuestionResetButton practiceTestButton" href="javascript:__doPostBack(&#39;ctl00$contentMain$resetButton&#39;,&#39;&#39;)">
                            <i class="fas fa-redo" style="margin-right: 2px;"></i>
                            Reset</a>
                        <a onclick="return validateQuestion(5);" id="contentMain_previousButton" class="practiceTestButton" href="javascript:__doPostBack(&#39;ctl00$contentMain$previousButton&#39;,&#39;&#39;)">
                            <i class="fas fa-arrow-left" style="margin-right: 2px;"></i>
                            Previous</a>
                        
                        <a onclick="return validateQuestion(4);" id="contentMain_skipButton" class="practiceTestButton" href="javascript:__doPostBack(&#39;ctl00$contentMain$skipButton&#39;,&#39;&#39;)">Skip
                            <i class="fas fa-share" style="margin-left: 2px;"></i>       
                        </a>
                        <a onclick="return validateQuestion(3);" id="contentMain_nextButton" class="practiceTestButton" href="javascript:__doPostBack(&#39;ctl00$contentMain$nextButton&#39;,&#39;&#39;)">Next 
                            <i class="fas fa-arrow-right" style="margin-left: 2px;"></i>
                        </a>
                    </div>
                </div>

                <div id="instructionBox" class="ui-widget-content">
                    <div class="instructionContainer bottomMargin15">
                        
                        <div id="contentMain_InstructionText" class="instructionText noSelect" style="font-size:13px;"><P>You are building a game with a series of questions and answers. Using the drop-down arrow, choose the best data type for each variable declaration. Not every data type will be used.</P>
</div>
                        
                    </div>      
                    <div id="resizeIcons" ></div>
                </div>
                
                
                <hr class="instructionSeperator" />
                
                <div id="contentMain_answerContainer" class="answerContainer" style="font-size:13px;">

<style>
    code {
        line-height: 30px;
        background: none;
    }
     .answerContainer {
         display: flex;
     }
    .dmCell {
        width: 40%;
        text-align: center;
        float: left;
    }
    .dmCellLeft {
        text-align: left;
    }
    .dmContainer {
        border: 1px solid black;
        margin: 5px;
        min-height: 11px;
        width: 96%;
    }
    .optionContainer {
        border: 1px outset #d7d7d7;
        min-height: 40px;

    }
    .optionDiv {
        padding: 10px;
        background-color: #d7d7d7;

    }
    .descriptionContainer {
        border: 1px outset #D9D9D9;

    }
    .descriptionText {
        padding: 10px;
        background-color: #D9D9D9;

    }
    .targetContainer {
        border: 1px inset #568abd;
        min-height: 39px;
        width: 250px;
        display: inline-block;
        margin-bottom: -6px;
    }
    .targetContainer .optionDiv {
        background-color: #568abd;
        color: white;
        padding: 10px 4px;
    }
    .targetDiv {
        padding: 10px;
        background-color: #D9D9D9;
    }

	.codeblock {
        background-color: gray !important;

        width: 450px;
		min-height:40px;
        display: inline-block;
        margin:10px;
		border:none !important;
		border-radius:5px;
    }

	.codeblock .optionDiv{
        background-color: green !important;

		width: 450px;
		height: 50px;
        display: inline-block;
    }

	.dmContainerCodeBlock {
		text-align: left !important;
		display:inline-block;
		margin:5px;
	}

</style>

<div class="leftFloat dmCell">
    
            			
			<div id="optionContainer0" class="dmContainer optionContainer" ondrop="dpDropDelete(event);" ondragover="allowDrop(event)">
                <div id="optionDiv0" class="optionDiv"  draggable="true" ondragstart="dpDrag(event);">
                    Boolean
                </div>
            </div>
        
            			
			<div id="optionContainer1" class="dmContainer optionContainer" ondrop="dpDropDelete(event);" ondragover="allowDrop(event)">
                <div id="optionDiv1" class="optionDiv"  draggable="true" ondragstart="dpDrag(event);">
                    char
                </div>
            </div>
        
            			
			<div id="optionContainer2" class="dmContainer optionContainer" ondrop="dpDropDelete(event);" ondragover="allowDrop(event)">
                <div id="optionDiv2" class="optionDiv"  draggable="true" ondragstart="dpDrag(event);">
                    String
                </div>
            </div>
        
            			
			<div id="optionContainer3" class="dmContainer optionContainer" ondrop="dpDropDelete(event);" ondragover="allowDrop(event)">
                <div id="optionDiv3" class="optionDiv"  draggable="true" ondragstart="dpDrag(event);">
                    double
                </div>
            </div>
        
            			
			<div id="optionContainer4" class="dmContainer optionContainer" ondrop="dpDropDelete(event);" ondragover="allowDrop(event)">
                <div id="optionDiv4" class="optionDiv"  draggable="true" ondragstart="dpDrag(event);">
                    int
                </div>
            </div>
        
</div>
<div class="leftFloat dmCell dmCellLeft" style="width: 60%;">

      <span>
    
            
        
            <div id="DPtargetContainer1"  class="dmContainer targetContainer" ondrop="dpDropNonExclusive(event);" ondragover="allowDrop(event)"></div>
        
             question1 = &quot;Switzerland is a land-locked country.&quot;;<br />


        
            <div id="DPtargetContainer3"  class="dmContainer targetContainer" ondrop="dpDropNonExclusive(event);" ondragover="allowDrop(event)"></div>
        
             answer1 = false;<br />


        
            <div id="DPtargetContainer5"  class="dmContainer targetContainer" ondrop="dpDropNonExclusive(event);" ondragover="allowDrop(event)"></div>
        
             points1 = 1;<br />

        
    </span>
</div>
</div>

            </div>



            <div id="FeedbackDiv" class="questionContainer">
                <input type="hidden" name="ctl00$contentMain$IsFeedbackShowing" id="IsFeedbackShowing" value="False" />
                <input type="hidden" name="ctl00$contentMain$hfIsAnswerCorrect" id="hfIsAnswerCorrect" />
                <input type="hidden" name="ctl00$contentMain$hfExplanationText" id="hfExplanationText" />

                <div class="questionMenu">
                    <div class="leftFloat">
                        <img alt="Gmetrix SMS" src="../Images/QuestionMenuLogo.png" style="margin-left: -1px;" />
                    </div>
                    <div class="rightFloat">
                        <input type="submit" name="ctl00$contentMain$FeedbackPreviousButton" value="Back" id="contentMain_FeedbackPreviousButton" class="practiceTestButton" />
                        <input type="submit" name="ctl00$contentMain$FeedbackNextButton" value="Next" id="contentMain_FeedbackNextButton" class="practiceTestButton" />
                    </div>
                </div>
                <div class="instructionContainer">
                    <div id="contentMain_Div1" class="instructionText">
                        <h3 style="margin-top: 0">
                            Feedback:</h3>
                        <div id="incorrectAnswer" class="incorrectAnswer">
                            <img src="../Images/IncorrectX.png" alt="incorrect x" class="answerIcon" />
                            Your answer is NOT correct!
                        </div>
                        <div id="correctAnswer" class="correctAnswer">
                            <img src="../Images/correctCheck.png" alt="correct check" class="answerIcon" />
                            Your answer is correct!
                        </div>
                    </div>
                </div>
                <div id="contentMain_Div2" class="answerContainer">
                    <h3 id="contentMain_explanationTitle">
                        Explanation:</h3>
                    <div id="contentMain_explanationText" class="explanationText noSelect">
                    </div>
                </div>
            </div>


            <div id="timerContainer" class="timerContainer">
                Time Remaining:
                <span id="timer"></span>
            </div>
            <input type="hidden" name="ctl00$contentMain$timerValue" id="timerValue" />
            <input type="hidden" name="ctl00$contentMain$minutesSpent" id="minutesSpent" />
            <input type="hidden" name="ctl00$contentMain$secondsSpent" id="secondsSpent" />
            <input type="submit" name="ctl00$contentMain$finishButton" value="" id="finishButton" style="display: none;" />
            <p style="display: none;">
                <input type="submit" name="ctl00$contentMain$btnTwoNext" value="" id="btnTwoNext" />
                <input type="submit" name="ctl00$contentMain$btnTwoSave" value="" id="btnTwoSave" data-toggle="modal" data-target="#savedSuccessfully" />
                <input type="submit" name="ctl00$contentMain$btnTwoFinish" value="" id="btnTwoFinish" />
                <input type="submit" name="ctl00$contentMain$btnTwoSkip" value="" id="btnTwoSkip" />
                <input type="submit" name="ctl00$contentMain$btnTwoBack" value="" id="btnTwoBack" />
                <input type="image" name="ctl00$contentMain$btnTwoMenu" id="btnTwoMenu" />

                
            </p>




            <script type="text/javascript">

                function instructionContainerResizer() {
                    if ($("body").width() < $("body").height()) {
                        $("#instructionBox").css("max-height", "100%");
                    }

                    if (document.getElementById("CurrentQuestionType").value == "8") {
                        $("#instructionBox").css("display", "none");
                        $(".instructionSeperator").css("display", "none");
                    }
                    else {
                        if ($("#instructionBox").height() < "90" || $("body").width() < $("body").height()) {
                            $("#resizeIcons").css("visibility", "hidden");
                        } else {
                           
                            if ($("#instructionBox").height() > "300") {
                                $("#instructionBox").css("height", "335")
                                $(".instructionContainer").css("height", "335")
                            }
                            $("#resizeIcons").css("visibility", "visible");
                            $("#instructionBox").resizable({
                                handles: { s: $("#resizeIcons") },
                                grid: [10000, 1],
                                start: function () {
                                    $("#instructionBox").css("height", "10%");
                                    $(".instructionContainer").css("height", "100%");
                                    $("#hiddenOverlay").css("visibility", "visible");
                                },
                                stop: function () {
                                    $("#hiddenOverlay").css("visibility", "hidden");
                                },

                            });
                        }
                    }
                }

                
                var instructionHeight;
                function helpTextLocationSetter() {
                    var bodyWidth = $("body").width();
                    var bodyHeight = $("body").height();
                    var containerWidth = $(".mainContainer").width();
                    var containerHeight = $(".mainContainer").height();
                    var helpTextWidth;

                    if (instructionHeight == undefined) {
                        instructionHeight = $(".instructionContainer").height();
                    }

                    if (bodyWidth > bodyHeight) {
                        helpTextWidth = ((bodyWidth - containerWidth) / 2.5);
                        $("#helpTextModal").css("width", helpTextWidth.toString());
                        $("#helpTextModal").css("height", "auto");
                    } else if (bodyWidth < bodyHeight) {
                        $("#helpTextModal").appendTo($(".instructionContainer"));                   
                        $("#helpTextModal").css("width", "93%");
                        $("#helpTextModal").css("cursor", "default");
                        $("#helpTextModal").css( "position", "relative" );
                        $("#helpTextModal").draggable("disable");
                        $("#helpTextModal").resizable("disable");
                       

                    }
                    

                }


                function pageLoad(sender, args) {// this method gets called when any of the async calls completes.
                    //set the font of the answer text
                    console.log("pageloading");
                    //end of setting answer text
                    
                    $("#resizeIcons").text("o o o o");
                    instructionContainerResizer(); 

               
                    
                    $("#helpTextModal").draggable({
                        containment: "window",
                        start: function () {
                            $("#hiddenOverlay").css("visibility", "visible");
                        },
                        stop: function () {
                            $("#hiddenOverlay").css("visibility", "hidden");
                        } 
                    });

                    $("#helpTextModal").resizable({
                        handles: "all",
                        containment: "body",
                        start: function () {
                            $("#hiddenOverlay").css("visibility", "visible");
                        },
                        stop: function () {
                            $("#hiddenOverlay").css("visibility", "hidden");
                        }   
                    });

                   
                    
                    
                        
                    





                    if (document.getElementById("IsFeedbackShowing").value == "False") {
                        document.getElementById("QuestionDiv").style.display = 'block';
                        document.getElementById("FeedbackDiv").style.display = 'none';

                    } else {
                        document.getElementById("QuestionDiv").style.display = 'none';
                        document.getElementById("FeedbackDiv").style.display = 'block';

                        try {
                            
                        
                            if (document.getElementById("hfIsAnswerCorrect").value == "1") {
                                document.getElementById("incorrectAnswer").style.display = 'none';
                                document.getElementById("correctAnswer").style.display = 'block';
                            } else {
                                document.getElementById("incorrectAnswer").style.display = 'block';
                                document.getElementById("correctAnswer").style.display = 'none';
                            }

                        } catch (e) {

                        } 
                    }

                    if (document.getElementById("IsFeedbackShowing").value == "False") { // if feedback isn't showing
                        SetIFrameSource();
                        if (document.getElementById("CurrentQuestionType").value == "12") { // if this is an iframe question
                            
                            document.getElementById("PreloadIFrameContainer").style.display = 'block';

                            try {
                                window.frames[0].setSceneAndQid(document.getElementById("CurrentQuestionSceneName").value, document.getElementById("IframeQid").value);
                            } catch (err){
                                console.log("setSceneAndQid() does not exist yet: " + err);
                            }

                            try {
                                window.frames[0].loadScene();
                            } catch (err){
                                console.log("loadScene() does not exist yet: " + err);
                            }
                            document.getElementById('contentIFrame').focus();
                        
                        } else { // if this is not an iframe question and feedback isn't showing
                          
                            document.getElementById("PreloadIFrameContainer").style.display = 'none';
                            try {
                                SetUserAnswers(document.getElementById("CurrentQuestionType").value);
                            } catch (e) {
                                console.log("Set User Answers called in error Feedback = " + document.getElementById("IsFeedbackShowing").value + " Question Type = " + document.getElementById("CurrentQuestionType").value);
                            }
                            
                        }


                    } else { // if feedback is showing. do nothing?
                        try {
                            document.getElementById("PreloadIFrameContainer").style.display = 'none';
                        } catch (e) {
                            console.log("Iframe Container Missing");
                        }
                        
                        
                    }

                    try {
                    
                    var textSize = document.getElementById('contentMain_TextSize').value;

                    if (textSize == "nothing" || textSize == "13px") {
                        document.getElementById("contentMain_answerContainer").style.fontSize = "13px";
                    } 
                    else if (textSize == "17px") {
                        document.getElementById("contentMain_answerContainer").style.fontSize = "17px";
                    }
                    else if (textSize == "22px") {
                        document.getElementById("contentMain_answerContainer").style.fontSize = "22px";
                    }
                    else if (textSize == "26px") {
                        document.getElementById("contentMain_answerContainer").style.fontSize = "26px";
                    }
                    else if (textSize == "31px") {
                        document.getElementById("contentMain_answerContainer").style.fontSize = "31px";
                    }

                    }
                    catch(e) {}

                   
                }

                function GetSceneandQuestionFromParent() {
                    try {
                        window.frames[0].setSceneAndQid(document.getElementById("CurrentQuestionSceneName").value, GetQid());
                    } catch(e) {
                        console.log("Scene Name is null");
                    }
                }

            </script>




        
</div>
         <div id="PreloadIFrameContainer" class="answerContainer" style="display: block">

        

<style>
    iframe {
         display: block;
         width: 930px;
         height: 700px;
         margin: auto;
	 border:none;
     }
</style>

<script>

    var questionScore = 0;

    function SetScore(score) { // called second
        //console.log("iScore: " + score);
        if (document.getElementById("CurrentQuestionType").value == "8") {
            document.getElementById("spnScore").value = score;
            questionScore = score;
        } else {
            document.getElementById("spnScore").value = score;
            document.getElementById("hidden").value = score;
        }
        

    }

    function IFrameReset() {
        ResetIframeQuestion();
        return false;
    }

    function ResetIframeQuestion() {

        document.getElementById('hidden').value = '';
        document.getElementById('spnScore').value = '';
        window.frames[0].loadScene();

    }

    function GetsceneName() {return "No Scene Set";} //Unity Needs This
    function GetQid() { return ""; } //Unity Needs This

   

    function IFramevalidateQuestion(buttonType) {
        //nothing to validate here really.
        return true;
       
    }

    $("#contentIFrame").load(function () {
        SetIFrameSource();
        if (document.getElementById('contentIFrame').src != "") {
            $("#IsSourceSet").val("1");
        }
        
    });

    //New Unity & Captivate Source settings Machias 10/06/17
    function SetIFrameSource() {
        //console.log("setIframe 1/2");
        var template = $('#CurrentQuestionSource').val();
        console.log(template);
        if (template == "" || template.indexOf("index.html") < 0) {
            return;
        } 

        var protocol = window.location.protocol;
        var baseUrl = window.location.host;
        var root = protocol + "//" + baseUrl;
        var folder = "/Content/iframe/";

        var NewSource = root + folder + template;

        var currentSource = document.getElementById('contentIFrame').src;
        //console.log(currentSource);
        if (currentSource !== NewSource ) {
            document.getElementById('contentIFrame').src = NewSource;
        } else if (currentSource == NewSource && currentSource.indexOf("Captivate") > -1) {
            document.getElementById('contentIFrame').src = NewSource;
        }
        //console.log("setIframe 2/2");
    }
        


</script>
<input type="hidden" name="ctl00$contentMain$PreloadedIframe$IsSourceSet" id="IsSourceSet" />
    <input type="hidden" name="ctl00$contentMain$PreloadedIframe$spnScore" id="spnScore" />


<iframe  ID="contentIFrame" src="" ></iframe>

        
    </div>
    </div>
    
    <div id="loadingDiv" style="display: block; height: 400px; width: 400px; margin: auto; position: relative; top: 100px;" class="questionContainer separateTop">
        <img style="margin: auto;" src="../Images/logoLoaderGraphic.gif" />
        <p class="LoadingTest">Loading Test...</p>
       
    </div>


   <script type="text/javascript"> $('.multipleChoiceButton:has(img)').addClass('optionHasImage'); </script>
    
    



            <div class="modal fade" id="savedSuccessfully" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalTitle">
                                Save Test</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to Save and Exit?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close</button>
                            <a id="saveTest" class="btn btn-primary" href="javascript:__doPostBack(&#39;ctl00$saveTest&#39;,&#39;&#39;)">Save and Exit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
