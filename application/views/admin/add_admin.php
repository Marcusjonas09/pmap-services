<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Administrators
        <small>Record</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Administrators</li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    Administrators Add Record
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <a href="<?=base_url()?>admin/admins" 
                            class="btn btn-success btn-md" role="button">BACK</a>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                 <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                      </div>
                                  <?php endif; ?>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?=base_url()?>admin/do_add_admin" method="post">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input value="<?=set_value('admin_name')?>" name="admin_name" class="form-control" placeholder="Enter Admin Name">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="radio-inline"><b>Role</b> </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="admin_role" value="admin" checked> Admin
                                            </label> 
                                            <label class="radio-inline">
                                                <input type="radio" name="admin_role" value="cashier"> Cashier
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="admin_role" value="officer"> Officer
                                            </label>

                                        </div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <label>Username</label>
                                        <input name="admin_username" class="form-control" placeholder="Username" >
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="admin_password" class="form-control" placeholder="Password">
                                    </div>
                                    
                                    <button type="submit" class="btn btn-primary">Submit Button</button>
                                        
                                </div>
                                 </form>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->



            
        </div>
        <!-- /#page-wrapper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>