<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Map Request
        <small>Record</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Maps</li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    Map Record
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <a href="<?=base_url()?>admin/home" 
                            class="btn btn-success btn-md" role="button">BACK (Home) </a>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                 <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                      </div>
                                  <?php endif; ?>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User ID</th>
                                        <th>Request</th>
                                        <th>Latitude</th>
                                        <th>Longtitude</th>
                                        <th>Date</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($maps as $m): ?>
                                    <tr>
                                        <td><?=$m->e_id?></td>
                                        <td><?=$m->u_id?></td>
                                        <td><?=$m->e_request?></td>
                                        <td><?=$m->e_lat?></td>
                                        <td><?=$m->e_lng?></td>
                                        
                                        <td>
                                            <?php 
                                              $date = date_create($m->e_date_request);
                                              echo date_format($date,"Y/m/d H:i:s");
                                            ?>
                                        </td>
                                        
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User ID</th>
                                        <th>Request</th>
                                        <th>Latitude</th>
                                        <th>Longtitude</th>
                                        <th>Date</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->     



                                        
                                </div>
                               
                                 
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->



            
        </div>
        <!-- /#page-wrapper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>