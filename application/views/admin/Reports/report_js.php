<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>assets/dist/js/demo.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<!-- date-range-picker -->
<script src="<?= base_url() ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    $('#reservation').daterangepicker()
    google.charts.load("current", {
        packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(JobsVsVendors);

    function JobsVsVendors() {

        $.ajax({
            url: "<?php echo site_url('Admin/JobsVsVendors'); ?>",
            method: "GET",
            dataType: 'json',
            success: function(data) {

                var data = google.visualization.arrayToDataTable(data);

                var options = {
                    pieHole: 1,
                    legend: {
                        position: 'bottom'
                    },

                };

                var chart1 = new google.visualization.BarChart(document.getElementById('barchart'));
                chart1.draw(data, options);
            }
        });
    }
</script>