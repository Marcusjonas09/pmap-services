<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Reports
        </h1>
    </section>

    <section class="content">
        <div id="page-wrapper">



            <div class="row">

                <div class="col-md-12">
                    <!-- Bar chart -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>

                            <h3 class="box-title">Subscriber Count</h3>
                        </div>
                        <div class="box-body">
                            <!-- <div class="form-group">
                                <label>Date range:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="reservation">
                                </div>
                            </div> -->
                            <div id="barchart"></div>
                        </div>
                        <!-- /.box-body-->
                    </div>
                    <!-- /.box -->
                </div>

                <!-- <div class="col-md-6">
                    Donut chart
                    <div class="box box-primary">

                        <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>
                            <h3 class="box-title">Jobs and Vendor Subscriptions</h3>
                        </div>

                        <div class="box-body">
                            <div id="donutchart"></div>
                        </div>

                    </div>

                </div> -->

                <!-- <div class="col-md-12">
                    <div class="box box-primary">

                        <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>
                            <h3 class="box-title">Jobs and Vendor Subscriptions</h3>
                        </div>

                        <div class="box-body">
                            <div id="curve_chart"></div>
                        </div>

                    </div>
                </div> -->

            </div>

        </div>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>