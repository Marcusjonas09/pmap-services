<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Company Profiles
        </h1>
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($message)) : ?>
                    <?= $message ?>
                <?php endif; ?>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-success pull-right" href="<?= base_url() ?>Admin/add_company"><span class="fa fa-plus"></span> &nbsp; Add Company</a>
                                </div>
                            </div>
                        </div>

                        <div class="box-body">

                            <table width="100%" class="table table-striped table-hover table-bordered" id="all_companies">
                                <thead>
                                    <th style="width:5%;">#</th>
                                    <th style="width:15%;">Company Name</th>
                                    <th style="width:25%;">Company Email</th>
                                    <th style="width:15%;">Contact Person</th>
                                    <th class="text-center" style="width:15%;">Action</th>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($companies as $company) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $company->CompanyName ?></td>
                                            <td><?= $company->CompanyEmail ?></td>
                                            <td><?= $company->ContactFullname ?></td>
                                            <td class="text-center">
                                                <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_company/<?= $company->CompanyId ?>"><span class="fa fa-pencil"></span></a>
                                                <button class="btn btn-primary" onclick="view_company(<?= $company->CompanyId ?>)"><span class="fa fa-eye"></span></button>
                                                <button class="btn btn-danger" onclick="delete_company(<?= $company->CompanyId ?>)"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

    <div data-keyboard="false" class="modal fade" id="view_company_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>