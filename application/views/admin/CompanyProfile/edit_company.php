<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Company
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/companies">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/update_company') ?>" method="post">
                            <input type="hidden" name="CompanyId" value="<?= $company->CompanyId ?>">
                            <div class="box-body">

                                <div class="row">
                                    <div class="form-group col-md-6 <?= form_error('CompanyName') ? 'has-warning' : ''; ?>">
                                        <label for="CompanyName">Company Name</label>
                                        <input value="<?= $company->CompanyName ?>" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('CompanyAddress') ? 'has-warning' : ''; ?>">
                                        <label for="CompanyAddress">Company Address</label>
                                        <input value="<?= $company->CompanyAddress ?>  " type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                    </div>

                                    <div class="form-group col-md-3 <?= form_error('CompanyTelNo') ? 'has-warning' : ''; ?>">
                                        <label for="CompanyTelNo">Telephone No. </label>
                                        <input value="<?= $company->CompanyTelNo ?>  " type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                    </div>

                                    <div class="form-group col-md-3 <?= form_error('CompanyCelNo') ? 'has-warning' : ''; ?>">
                                        <label for="CompanyCelNo">Mobile No.</label>
                                        <input value="<?= $company->CompanyCelNo ?>  " type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('CompanyEmail') ? 'has-warning' : ''; ?>">
                                        <label for="CompanyEmail">Company Email</label>
                                        <input value="<?= $company->CompanyEmail ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('HRemail')  ? 'has-warning' : ''; ?>">
                                        <label for="HRemail">HR Email Address</label>
                                        <input value="<?= $company->HRemail ?>" type="email" name="HRemail" id="HRemail" class="form-control" placeholder="example@example.com">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Contact Person</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 <?= form_error('ContactFullname') ? 'has-warning' : ''; ?>">
                                        <label for="ContactFullname">FullName</label>
                                        <input value="<?= $company->ContactFullname ?>  " type="text" name="ContactFullname" id="ContactFullname" placeholder="Juan Dela Cruz" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactPosition') ? 'has-warning' : ''; ?>">
                                        <label for="ContactPosition">Position in the company</label>
                                        <input value="<?= $company->ContactPosition ?>  " type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactNo') ? 'has-warning' : ''; ?>">
                                        <label for="ContactNo">Telephone No.</label>
                                        <input value="<?= $company->ContactNo ?>  " type="text" name="ContactNo" id="ContactNo" placeholder="Telephone No" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactEmail') ? 'has-warning' : ''; ?>">
                                        <label for="ContactEmail">Email</label>
                                        <input value="<?= $company->ContactEmail ?>  " type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control">
                                    </div>
                                </div>


                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>