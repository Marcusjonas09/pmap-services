<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Company
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/companies">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/create_company') ?>" method="post">

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 <?= form_error('CompanyName') ? 'has-warning' : ''; ?> ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="CompanyName">Company Name</label>
                                                <label class="pull-right"><?= form_error('CompanyName') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>

                                        <input value="<?= set_value('CompanyName') ?>" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('CompanyAddress')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="CompanyAddress">Company Address</label>
                                                <label class="pull-right"><?= form_error('CompanyAddress') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('CompanyAddress') ?>" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                    </div>

                                    <div class="form-group col-md-3 <?= form_error('CompanyTelNo')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="CompanyTelNo">Telephone No.</label>
                                                <label class="pull-right"><?= form_error('CompanyTelNo') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('CompanyTelNo') ?>" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                    </div>

                                    <div class="form-group col-md-3 <?= form_error('CompanyCelNo')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="CompanyCelNo">Mobile No.</label>
                                                <label class="pull-right"><?= form_error('CompanyCelNo') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('CompanyCelNo') ?>" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('CompanyEmail') ? 'has-warning' : ''; ?> ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="CompanyEmail">Company Email</label>
                                                <label class="pull-right" for="CompanyEmail"><?= form_error('CompanyEmail') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>

                                        <input value="<?= set_value('CompanyEmail') ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                    </div>


                                    <div class="form-group col-md-6 <?= form_error('HRemail')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="HRemail">HR Email Address</label>
                                                <label class="pull-right"><?= form_error('HRemail') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('HRemail') ?>" type="email" name="HRemail" id="HRemail" class="form-control" placeholder="example@example.com">
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Contact Person</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 <?= form_error('ContactFullname')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="ContactFullname">Fullname</label>
                                                <label class="pull-right"><?= form_error('ContactFullname') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('ContactFullname') ?>" type="text" name="ContactFullname" id="ContactFullname" placeholder="Juan Dela Cruz" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactPosition')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="ContactPosition">Position in the company</label>
                                                <label class="pull-right"><?= form_error('ContactPosition') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('ContactPosition') ?>" type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactNo')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="ContactNo">Contact No.</label>
                                                <label class="pull-right"><?= form_error('ContactNo') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('ContactNo') ?>" type="text" name="ContactNo" id="ContactNo" placeholder="Mobile/Landline" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('ContactEmail')  ? 'has-warning' : ''; ?>">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="ContactEmail">Email Address</label>
                                                <label class="pull-right"><?= form_error('ContactEmail') ? 'Required' : ''; ?></label>
                                            </div>
                                        </div>
                                        <input value="<?= set_value('ContactEmail') ?>" type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>