<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users fa-fw"></i> Administrators
        <small>Management</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Administrators</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                    Administrators Record Management 
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <h2>
                </h2>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <a href="<?=base_url()?>admin/add_admin" 
                            class="btn btn-success btn-md active" role="button"><i class="fa fa-plus"></i> Add New Admin</a>
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Picture</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($admins as $r): ?>
                                    <tr>
                                        <td><?=$r->id?></td>
                                        <td><img src="<?=base_url()?>uploads_thumb/<?=$r->image?>" alt="<?=$r->admin_name?>" class="img-circle" width="50px"></td>
                                        <td><?=$r->admin_name?></td>
                                        <td><?=$r->admin_role?></td>
                                        <td>
                                            <?=$r->status==1?"<p class='text-green'>Actived</p>":
                                            "<p class='text-red'>Not Activated</p>"?>
                                        </td>
                                        <td>
                                            <?php 
                                              $date = date_create($r->created_at);
                                              echo date_format($date,"Y/m/d H:i:s");
                                            ?>
                                        </td>
                                        <td align="center">
                                          <?php if ($r->status==0): ?>
                                            <a onclick="updateAdminStatus(<?=$r->id?>)"  class="btn btn-danger btn-circle  btn-md"><i class="fa fa-lock"></i>
                                          </a>
                                          <?php else: ?>
                                            <a onclick="updateAdminStatus(<?=$r->id?>)"   class="btn btn-success btn-circle  btn-md"><i class="fa fa-unlock"></i> 
                                          </a>
                                          <?php endif; ?>
                                            
                                            <a href="<?=base_url()?>admin/edit_admin/<?=$r->id?>"   class="btn btn-warning btn-circle btn-md"><i class="fa fa-edit"></i>
                                            </a>
                                            <a onclick="deleteAdmin(<?=$r->id?>)" class="btn btn-danger btn-circle btn-md"><i class="fa fa-trash-o"></i>
                                            </a>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                                 <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Picture</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>