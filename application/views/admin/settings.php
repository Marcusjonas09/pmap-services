<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
        <small>for Demonstration</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>

      <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">


        <form role="form" action="<?=base_url()?>admin/do_update_settings"  method="post">
        <div class="panel panel-primary">
            <div class="panel-heading">SMS and EMAIL</div>
            <div class="panel-body">


              <div class="form-group">
                <?php foreach($settings as $r): ?>
                  <input type="checkbox" value="<?=$r->settings_id?>" name="<?=$r->settings_type?>"
                  <?=$r->settings_enable==1?'checked':''?> > <?=$r->settings_description?> <br/>
                <?php endforeach; ?>
              </div>

              <div class="form-group">
                <button type="submit" name="emailsms" value="true" class="btn btn-primary">COMMIT CHANGES</button>
              </div>

            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-heading">EXPORT TABLES</div>
            <div class="panel-body">


              <div class="form-group">
                
                <button type="submit" name="exportpayments" value="true" class="btn btn-primary">EXPORT PAYMENTS </button>
                
              </div>

            </div>
          </div>


          <div class="panel panel-primary">
            <div class="panel-heading">BACKUP DATABASE TABLES</div>
            <div class="panel-body">

              <div class="form-group">
                <button type="submit" name="backupdb" value="true" class="btn btn-primary">BACKUP DATABASE</button>
                <button type="submit" name="backuptables" value="true" class="btn btn-primary">BACKUP TABLES </button>
                <button type="submit" name="backuptrails" value="true" class="btn btn-primary">BACKUP AUDIT TRAIL </button>
              </div>

            </div>
          </div>

          <div class="panel panel-danger">
            <div class="panel-heading">RESET TABLES</div>
            <div class="panel-body">

              <div class="form-group">
              

                <button type="submit" name="tablereset" value="payments" class="btn btn-danger swa-confirm" id="btn-submit-reset">RESET PAYMENTS </button>
                <button type="submit" name="tablereset" value="request" class="btn btn-danger">RESET REQUEST </button>
                <button type="submit" name="tablereset" value="complaints" class="btn btn-danger">RESET COMPLAINS </button>
                <button type="submit" name="tablereset" value="certifications" class="btn btn-danger">RESET CERTIFICATIONS </button>
                <button type="submit" name="tablereset" value="messages" class="btn btn-danger">RESET MESSAGES </button>
                <button type="submit" name="tablereset" value="trails" class="btn btn-danger">RESET AUDIT TRAILS </button>


              </div>

            </div>
          </div>

<!-- 
          <div class="panel panel-primary">
            <div class="panel-heading">IMPORT TABLES</div>
            <div class="panel-body">


              <div class="form-group">
                
                <button type="submit" name="exportpayments" value="true" class="btn btn-primary">EXPORT PAYMENTS </button>
                
              </div>

            </div>
          </div>

          </form>


        </div> -->

        <?php if($this->session->flashdata('update_success_emailsms')): ?>
        <div class="col-md-6">
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Message!</strong> You have successfully update the record.<br/>
            <?=$this->session->flashdata('update_success_emailsms')?>
          </div>
        </div>
      <?php endif; ?>


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>