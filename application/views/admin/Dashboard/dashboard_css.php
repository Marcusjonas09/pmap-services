<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Status', 'Number'],
      <?= $report_events ?>
    ]);

    var options = {
      title: 'Request Chart Report'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data, options);
  }
</script>


  <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Amenity', 'For Payment', 'Disapproved', 'Pending', 'Approved'],
          <?= $report_amenities ?>
        ]);

        var options = {
          width: 800,
          chart: {
            title: 'Amenities Request',
            subtitle: 'Number of request per amenities'
          },
          bars: 'horizontal', // Required for Material Bar Charts.
          // series: {
          //   0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
          //   1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
          // },
          axes: {
            x: {
              distance: {label: 'Number of Request'}, // Bottom x-axis.
              brightness: {side: 'top', label: 'Current Request'} // Top x-axis.
            }
          }
        };

      var chart = new google.charts.Bar(document.getElementById('dual_x_div'));
      chart.draw(data, options);
    };
    </script>


    <script type="text/javascript">
       google.charts.load('current', {'packages':['table']});
    google.charts.setOnLoadCallback(drawSort);
      function drawSort() {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('number', 'Amount');
  data.addColumn('boolean', 'Full Time');
  data.addRows(5);
  
    data.setCell(0, 0, 'Amenities');
    data.setCell(0, 1, <?= $amenities_total_amount ?>);
    data.setCell(0, 2, true);
    data.setCell(1, 0, 'Certifications');
    data.setCell(1, 1, <?= $certifications_total_amount ?>);
    data.setCell(1, 2, true);
    data.setCell(2, 0, 'Monthly Dues');
    data.setCell(2, 1, <?= $monthly_dues_total_amount ?>);
    data.setCell(2, 2, true);
    data.setCell(3, 0, 'Parking');
    data.setCell(3, 1, <?= $parking_total_amount ?>);
    data.setCell(3, 2, true);
    data.setCell(4, 0, 'Others');
    data.setCell(4, 1, <?= $others_total_amount ?>);
    data.setCell(4, 2, true);
      
  
  
  var formatter = new google.visualization.NumberFormat({prefix: 'P'});
  formatter.format(data, 1); // Apply formatter to second column

  var view = new google.visualization.DataView(data);
  view.setColumns([0, 1]);

  var table = new google.visualization.Table(document.getElementById('table_sort_div'));
  table.draw(view, {width: '100%', height: '100%'});

  var chart = new google.visualization.BarChart(document.getElementById('chart_sort_div'));
  chart.draw(view);

  google.visualization.events.addListener(table, 'sort',
      function(event) {
        data.sort([{column: event.column, desc: !event.ascending}]);
        chart.draw(view);
      });
}

    </script>

 -->