<!-- DataTables -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>

<script>
    $(document).ready(function() {

        setInterval(() => {

            // get registered companies (JOBS)
            $.ajax({
                url: "<?php echo site_url('Admin/JobsRegisteredCompanies'); ?>",
                method: "GET",
                success: function(data) {
                    $('#JobsRegisteredCompanies').text(data);
                }
            });

            // get registered companies (JOBS)
            $.ajax({
                url: "<?php echo site_url('Admin/VendorRegisteredCompanies'); ?>",
                method: "GET",
                success: function(data) {
                    $('#VendorRegisteredCompanies').text(data);
                }
            });

            // get registered companies (JOBS)
            $.ajax({
                url: "<?php echo site_url('Admin/PendingJobsRequest'); ?>",
                method: "GET",
                success: function(data) {
                    $('#PendingJobsRequest').text(data);
                }
            });

            // get registered companies (JOBS)
            $.ajax({
                url: "<?php echo site_url('Admin/PendingVendorsRequest'); ?>",
                method: "GET",
                success: function(data) {
                    $('#PendingVendorsRequest').text(data);
                }
            });

        }, 1000);

    });
</script>