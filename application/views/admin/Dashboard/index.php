<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php if ($this->session->role == 'admin') : ?>
            <div id="page-wrapper">

                <div class="row">

                    <div class="col-lg-3 col-xs-6">

                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3 id="JobsRegisteredCompanies">0</h3>
                                <p>Jobs Registered Companies</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-building-0"></i>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">

                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3 id="VendorRegisteredCompanies">0</h3>
                                <p>Vendors Registered Companies</p>
                            </div>
                        </div>
                    </div>




                    <div class="col-lg-3 col-xs-6">

                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3 id="PendingJobsRequest">0</h3>
                                <p>Pending Jobs Request</p>
                            </div>
                        </div>
                    </div>




                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3 id="PendingVendorsRequest">0</h3>
                                <p>Pending Vendors Request</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                Dashboard
                            </div>
                            <div class="box-body">
                                <div id="piechart" style="width: 800px; height: 300px"></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        <?php else : ?>
            <div id="page-wrapper">
            </div>
        <?php endif; ?>

    </section>

</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>