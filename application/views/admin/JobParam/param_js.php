<!-- DataTables -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>


<script src="<?= base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function() {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
        })
    })
</script>

<script>
    function delete_job_parameter(id, type) {
        swal({
                title: "Are you sure?",
                text: "All the values associated with this will be deleted!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // alert(name);
                    location.href = "<?= base_url() ?>admin/delete_job_parameter/" + id + "/" + type;
                }

            });
    }

    function delete_job_param_val(id, param_id) {
        swal({
                title: "Are you sure?",
                text: "This action is irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    location.href = "<?= base_url() ?>admin/delete_job_param_val/" + id + "/" + param_id;
                }

            });
    }
</script>

<script>
    $(function() {
        $('#all_job_parameters').DataTable({
            ordering: false,
            lengthChange: false,
        })

        $('#job_param_values').DataTable({
            searching: false,
            ordering: false,
            lengthChange: false,
        })
    })
</script>