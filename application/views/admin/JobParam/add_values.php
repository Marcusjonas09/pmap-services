<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Parameter Values
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-warning" role="alert"><?= validation_errors() ?></div>
                    <?php endif; ?>
                    <?php if (isset($message)) {
                        echo $message;
                    } ?>
                </div>
                <div class="col-lg-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/JobParam">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                            <button class="btn btn-success pull-right" data-toggle="modal" data-target="#addValue">
                                <span class="fa  fa-plus"></span>&nbsp Add Value
                            </button>
                        </div>
                        <form action="<?= site_url('Admin/edit_job_param') ?>" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Parameter</label>
                                    <input disabled class="form-control" type="text" name="param_name" param_name placeholder="Enter Parameter Name" value="<?= $parameter->param_name ?>">
                                    <input type="hidden" name="param_id" value="<?= $parameter->param_id ?>">
                                </div>

                                <table width="100%" class="table table-striped table-bordered table-hover" id="job_param_values">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Value</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;
                                        foreach ($values as $value) : ?>
                                            <tr>
                                                <td><?= $i++ ?></td>
                                                <td><?= $value->pv_value ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-danger" onclick="delete_job_param_val('<?= $value->pv_id ?>','<?= $parameter->param_id ?>')"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>

<!-- Modal -->
<div class="modal fade" id="addValue" tabindex="-1" role="dialog" aria-labelledby="addValueLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addValueLabel">Add parameter value</h4>
            </div>
            <form action="<?= base_url() ?>Admin/add_job_value" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="param_value">Parameter Value:</label>
                        <input type="text" name="param_value" id="param_value" class="form-control">
                        <!-- <input type="hidden" name="param_name" id="param_name" value="<?= $parameter->param_name ?>" class="form-control"> -->
                        <input type="hidden" name="param_id" id="param_id" value="<?= $parameter->param_id ?>" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary col-md-2 pull-right">Save</button>
                    <button type="button" style="margin-right:10px;" class="btn btn-default col-md-2 pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>