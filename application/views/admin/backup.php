<!-- PAYMENT RESIDENT -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Payments
      <small>Request and Monthly Dues</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>user/home"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Payments</li>
      <li class="active">Records</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">
      <div class="col-lg-12">
<div class="panel panel-primary">
                        <div class="panel-heading">
                           PAID PAYMENTS (TOTAL: AMOUNT: PhP <?=number_format($totalpaid->payment_amount,2)?>)
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="example1">
          <thead>
            <tr>
              <th>Id</th>
              <th>Reference</th>
              <th>Amount</th>
              <th>For</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($paids as $p): ?>
              <tr>
                <td><?=$p->payment_id?></td>
                <td><?=$p->payment_reference?></td>
                <td><?=$p->payment_amount?></td>
                <td><?=$p->payment_for?></td>
              <td>
                <?php 
                $date = date_create($p->created_at);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
      <!-- /.table-responsive -->
    </div>
    </div>
    </div>

    
    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



    </div>
    <!-- /.row -->

<div class="row">
      <div class="col-lg-12">
<div class="panel panel-danger">
                        <div class="panel-heading">
                           NEED TO CONFIRM PAID PAYMENTS (TOTAL AMOUNT: PhP <?=number_format($totalnotpaid->payment_amount,2)?> )
                        </div>

                        <!-- /.panel-heading -->
                        <div class="panel-body">
        <table width="100%" class="table table-striped table-bordered table-hover" id="example2">
          <thead>
            <tr>
              <th>Id</th>
              <th>Reference</th>
              <th>Amount</th>
              <th>For</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($not_paids as $p): ?>
              <tr>
                <td><?=$p->payment_id?></td>
                <td><?=$p->payment_reference?></td>
                <td><?=$p->payment_amount?></td>
                <td><?=$p->payment_for?></td>
              <td>
                <?php 
                $date = date_create($p->created_at);
                echo date_format($date,"Y/m/d H:i:s");
                ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
       
      </table>
      <!-- /.table-responsive -->
    </div>
    </div>
    </div>

    
    <div class="modal modal-default fade" id="modal-info">
      <div class="modal-dialog">
        <div class="modal-content" id="modal_request">
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->



    </div>
    <!-- /.row -->




  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0
    <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
    reserved.
  </footer>