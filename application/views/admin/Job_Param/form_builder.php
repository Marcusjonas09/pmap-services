<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Job Parameters
        </h1>
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($message)) : ?>
                    <?= $message ?>
                <?php endif; ?>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Textfield</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Textarea</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Select</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Radio Button</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Checkbox</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Email</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Number</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Password</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Date</button>
                                    <button class="btn btn-primary col-md-12" style="margin-bottom:10px;">Button</button>
                                </div>
                                <div class="col-md-10">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <strong>Copyright &copy; <?= date('Y', time()) ?> <?= $this->config->item('footer') ?></strong> All rights
        reserved.
</footer> -->