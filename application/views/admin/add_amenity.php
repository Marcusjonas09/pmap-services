<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Amenity
        <small>Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Amenities</li>
        <li class="active">Add</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Amenity Add Record
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a href="<?=base_url()?>admin/amenities" 
                        class="btn btn-success btn-md" role="button">BACK</a>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?=base_url()?>admin/do_add_amenity"  enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input value="<?=set_value('name')?>" name="name" class="form-control" placeholder="Enter Amenity Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input value="<?=set_value('price')?>" name="price" class="form-control" placeholder="Enter Amenity Price">
                                    </div>

                                    <div class="form-group">
                                      <label for="exampleInputFile">Image</label>
                                      <input name="profilepict" type="file" id="exampleInputFile">
                                      <p class="help-block">Choose image to upload</p>

                                  </div>

                                  <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <!-- /.col-lg-6 (nested) -->

                            <div class="col-lg-6">


                                <textarea name="description" class="textarea" placeholder="Place amenity description here"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=set_value('description')?></textarea>


                                <div class="form-group">
                                    <button type="submit" name="add_amenity" value="true" class="btn btn-primary">ADD AMENITY</button>
                                </div>
                            </div>

                        </div>
                        <!-- /.row (nested) -->



                    </form>
                </div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->




</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>