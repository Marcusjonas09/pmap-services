<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> FAQs
        <small>Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> CMS</a></li>
        <li>FAQs</li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                FAQ Edit Record
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a href="<?=base_url()?>admin/faqs" 
                        class="btn btn-success btn-md" role="button">BACK</a>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<?=base_url()?>admin/do_edit_faq/<?=$faq->faq_id?>" method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Date Posted: </label>
                                        <input value="<?=$faq->created_at?>" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Posted By: </label>
                                        <input value="<?=$faq->posted_by?>" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <input value="<?=set_value('category',$faq->faq_category)?>" name="category" class="form-control" placeholder="Enter Category">
                                    </div>
                                    <div class="form-group">
                                        <label>Question</label>
                                        <input value="<?=set_value('question',$faq->faq_question)?>" name="question" class="form-control" placeholder="Enter Category">
                                    </div>
                                  <?php if(validation_errors()): ?>
                                      <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                        <?=validation_errors()?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <!-- /.col-lg-6 (nested) -->

                            <div class="col-lg-6">


                                <textarea name="answer" class="textarea" placeholder="Enter FAQ here "
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=set_value('answer',$faq->faq_answer)?></textarea>


                                <div class="form-group">
                                    <button type="submit" name="add_amenity" value="true" class="btn btn-primary">UPDATE FAQ</button>
                                </div>
                            </div>

                        </div>
                        <!-- /.row (nested) -->



                    </form>
                </div>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->




</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>