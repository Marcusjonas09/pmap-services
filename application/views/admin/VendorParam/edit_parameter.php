<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Parameter
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/parameters">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/edit_param') ?>" method="post">
                            <div class="box-body">
                                <?= form_error('param_name')  ? form_error('param_name', '<div class="alert alert-warning">', '</div>') : ''; ?>
                                <div class="form-group <?= form_error('param_name') ? 'has-warning' : ''; ?>">
                                    <label>Parameter</label>
                                    <input class="form-control" type="text" name="param_name" param_name placeholder="Enter Parameter Name" value="<?= $parameter->param_name ?>">
                                    <input type="hidden" name="param_id" value="<?= $parameter->param_id ?>">
                                </div>
                                <div class="radio">
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" <?= $parameter->param_type == 'textfield' ? 'checked' : ''; ?> name="param_type" value="textfield"> Textfield
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            <input type="radio" <?= $parameter->param_type == 'select' ? 'checked' : ''; ?> name="param_type" value="select"> Select
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right col-md-2" type="submit"> Save</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>