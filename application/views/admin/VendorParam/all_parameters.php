<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Vendor Parameters
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($message)) : ?>
                    <?= $message ?>
                <?php endif; ?>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-success pull-right" href="<?= base_url() ?>Admin/add_parameter"><span class="fa fa-plus"></span> Add Parameter</a>
                                </div>
                            </div>

                        </div>

                        <div class="box-body">

                            <table width="100%" class="table table-striped table-hover" id="all_parameters">
                                <thead>
                                    <th>Id</th>
                                    <th>Parameter</th>
                                    <th>Type</th>
                                    <th style="width:20%;">Action</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($parameters as $parameter) : ?>
                                        <tr>
                                            <td><?= $parameter->param_id ?></td>
                                            <td><?= $parameter->param_name ?></td>
                                            <td><?= $parameter->param_type ?></td>
                                            <td>
                                                <button class="btn btn-danger pull-right" onclick="delete_parameter('<?= $parameter->param_id ?>','<?= $parameter->param_type ?>')"><i class="fa fa-trash"></i></button>
                                                <a class="btn btn-warning pull-right" href="<?= base_url() ?>Admin/edit_parameter/<?= $parameter->param_id ?>"><span class="fa fa-pencil"></span></a>
                                                <?php if ($parameter->param_type == 'select') : ?>
                                                    <a class="btn btn-primary pull-right" href="<?= base_url() ?>Admin/add_values/<?= $parameter->param_id ?>"><span class="fa fa-plus"></span></a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>