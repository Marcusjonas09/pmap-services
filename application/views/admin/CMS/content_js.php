<!-- DataTables -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- CK Editor -->
<script src="<?= base_url() ?>assets/bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(function() {
        CKEDITOR.replace('content_description');
    })
</script>

<script>
    if ($("#content_layout").val() == "card_regular") {
        $("#upload_image").hide();
        $("#uploaded_image").hide();

    } else {
        $("#upload_image").show();
        $("#uploaded_image").show();
    }
    $("#content_layout").change(function() {
        if ($("#content_layout").val() == "card_regular") {
            $("#upload_image").hide();
            $("#uploaded_image").hide();
        } else {
            $("#upload_image").show();
            $("#uploaded_image").show();
        }
    })

    function delete_content(id) {
        swal({
                title: "Are you sure?",
                text: "You wont be able to undo this action",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    console.log(id);
                    $.ajax({
                        url: "<?php echo base_url() . 'Admin/delete_content'; ?>",
                        method: "POST",
                        data: {
                            content_id: id
                        },
                        success: function(data) {
                            swal("Deleted", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/contents";
                            });
                        }
                    });
                }

            });
    }

    function publish_content(id) {
        swal({
                title: "Are you sure?",
                text: "This will now be included in the live site.",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/publish_content'); ?>",
                        method: "POST",
                        data: {
                            content_id: id
                        },
                        success: function(data) {
                            response = JSON.parse(data);
                            if (response.status) {
                                swal(response.message, {
                                    icon: response.icon,
                                    buttons: false,
                                    timer: 1100,
                                }).then((value) => {
                                    location.href = "<?= base_url() ?>Admin/contents";
                                });
                            } else {
                                swal(response.message, {
                                    icon: response.icon,
                                    buttons: false,
                                    timer: 1100,
                                }).then((value) => {
                                    location.href = "<?= base_url() ?>Admin/contents";
                                });
                            }

                        }
                    });
                }

            });
    }

    function unpublish_content(id) {
        swal({
                title: "Are you sure?",
                text: "This will removed from the live site.",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/unpublish_content'); ?>",
                        method: "POST",
                        data: {
                            content_id: id
                        },
                        success: function(data) {
                            swal("Unpublished", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/contents";
                            });
                        }
                    });
                }

            });
    }
</script>

<script>
    $(function() {

        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#CMSTabs a[href="' + activeTab + '"]').tab('show');
        }

        $('.select2').select2()
        $('#LandingPageTable').DataTable({
            lengthChange: false,
            sorting: false,
            searching: false
        })

        $('#JobPricingTable').DataTable({
            lengthChange: false,
            sorting: false,
            searching: false
        })

        $('#VendorPricingTable').DataTable({
            lengthChange: false,
            sorting: false,
            searching: false
        })
        $('.slider').slider()
    })
</script>