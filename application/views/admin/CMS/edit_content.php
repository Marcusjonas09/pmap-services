<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Content
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <form role="form" action="<?= base_url() ?>admin/update_content" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="content_id" value="<?= $contents->content_id ?>">
                            <div class="box-header with-border">
                                <a class="btn btn-default" href="<?= base_url() ?>Admin/contents">
                                    <span class="fa  fa-chevron-left"></span>&nbsp Back
                                </a>
                                <button type="submit" name="add_cms" value="true" class="btn btn-primary pull-right">Save Changes</button>
                            </div>

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if (validation_errors() || isset($message)) : ?>
                                            <div class="alert alert-warning alert-dismissible">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                                                <?= validation_errors() ?>
                                                <p><?= $message ?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="content_title">Title</label>
                                            <input value="<?= $contents->content_title ?>" name="content_title" class="form-control" placeholder="Enter Title">
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea name="content_description" id="content_description" class="textarea" placeholder="Place content description here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $contents->content_description ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12" style="margin-top:10px;">
                                                    <div class="form-group">
                                                        <label for="page_assignment">Page</label>

                                                        <select class="form-control select2" name="page_assignment" id="page_assignment">
                                                            <option <?= ($contents->page_assignment == "Landing Page") ? 'selected' : ''; ?> value="Landing Page">Landing Page</option>
                                                            <option <?= ($contents->page_assignment == "Job Pricing Page") ? 'selected' : ''; ?> value="Job Pricing Page">Job Pricing Page</option>
                                                            <option <?= ($contents->page_assignment == "Vendor Pricing Page") ? 'selected' : ''; ?> value="Vendor Pricing Page">Vendor Pricing Page</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="margin-top:10px;">
                                                    <div class="form-group">
                                                        <label for="content_layout">Layout</label>

                                                        <select class="form-control select2" name="content_layout" id="content_layout">
                                                            <option <?= ($contents->content_layout == "card_regular") ? 'selected' : ''; ?> value="card_regular">Card - Regular</option>
                                                            <option <?= ($contents->content_layout == "card_image_vertical") ? 'selected' : ''; ?> value="card_image_vertical">Card with image - vertical</option>
                                                            <option <?= ($contents->content_layout == "card_image_horizontal") ? 'selected' : ''; ?> value="card_image_horizontal">Card with image - horizontal</option>
                                                            <option <?= ($contents->content_layout == "card_image_overlay") ? 'selected' : ''; ?> value="card_image_overlay">Card with image - overlay</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (!empty($contents->content_image)) : ?>
                                            <div class="form-group" id="uploaded_image">
                                                <img src="<?= base_url() ?>uploads/<?= $contents->content_image ?>" class="col-md-6" alt="">
                                            </div>
                                        <?php endif; ?>
                                        <div class="form-group" id="upload_image">
                                            <label for="content_image">Choose Image</label>
                                            <input name="content_image" type="file" id="content_image">
                                            <p class="help-block" style="margin-bottom:0px;">Max file size: 500kb</p>
                                            <p class="help-block" style="margin-top:0px; margin-bottom:0px;">Max size: 1024x768</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
</div>

<!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>