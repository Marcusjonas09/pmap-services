<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Content Management System
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- <div id="page-wrapper"> -->
        <?php if (isset($message)) : ?>
            <?= $message ?>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="CMSTabs">
                        <li class="active"><a href="#LandingPage" data-toggle="tab">Landing Page</a></li>
                        <li><a href="#JobPricing" data-toggle="tab">Job Pricing</a></li>
                        <li><a href="#VendorPricing" data-toggle="tab">Vendor Pricing Page</a></li>
                        <a href="<?= base_url() ?>Admin/add_content" style="margin:10px; margin-left:0px;" class="btn btn-success pull-right"><i class="fa fa-plus"></i> &nbsp; Add Content Page</a>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="LandingPage">
                            <button type="button" style="margin:10px; margin-left:0px;" class="btn btn-primary" data-toggle="modal" data-target="#preview_landing">
                                <i class="fa fa-eye"></i> &nbsp; Preview
                            </button>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="LandingPageTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Page</th>
                                        <th>Created At</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($contents as $content) : ?>
                                        <?php if ($content->page_assignment == "Landing Page") : ?>
                                            <tr>
                                                <td><?= $content->content_id ?></td>
                                                <td><?= $content->content_image ? $content->content_image : 'N/A'; ?></td>
                                                <td><?= $content->content_title ?></td>
                                                <td><?= $content->page_assignment ?></td>
                                                <td><?= date('M-d-Y h:i:s A', strtotime($content->created_at)) ?></td>
                                                <td>
                                                    <?php if (!$content->is_published) : ?>
                                                        <button onclick="publish_content(<?= $content->content_id ?>)" class="btn btn-success"><span class="fa fa-check"></button>
                                                    <?php else : ?>
                                                        <button onclick="unpublish_content(<?= $content->content_id ?>)" class="btn btn-default"><span class="fa fa-close"></button>
                                                    <?php endif; ?>
                                                    <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_content/<?= $content->content_id ?>"><span class="fa fa-pencil"></span></a>
                                                    <button onclick="delete_content(<?= $content->content_id ?>)" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="JobPricing">
                            <button type="button" style="margin:10px; margin-left:0px;" class="btn btn-primary" data-toggle="modal" data-target="#preview_job_pricing">
                                <i class="fa fa-eye"></i> &nbsp; Preview
                            </button>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="JobPricingTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Page</th>
                                        <th>Created At</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($contents as $content) : ?>
                                        <?php if ($content->page_assignment == "Job Pricing Page") : ?>
                                            <tr>
                                                <td><?= $content->content_id ?></td>
                                                <td><?= $content->content_image ? $content->content_image : 'N/A'; ?></td>
                                                <td><?= $content->content_title ?></td>
                                                <td><?= $content->page_assignment ?></td>
                                                <td><?= date('M-d-Y h:i:s A', strtotime($content->created_at)) ?></td>
                                                <td>
                                                    <?php if (!$content->is_published) : ?>
                                                        <button onclick="publish_content(<?= $content->content_id ?>)" class="btn btn-success"><span class="fa fa-check"></button>
                                                    <?php else : ?>
                                                        <button onclick="unpublish_content(<?= $content->content_id ?>)" class="btn btn-default"><span class="fa fa-close"></button>
                                                    <?php endif; ?>
                                                    <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_content/<?= $content->content_id ?>"><span class="fa fa-pencil"></span></a>
                                                    <button onclick="delete_content(<?= $content->content_id ?>)" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="VendorPricing">
                            <button type="button" style="margin:10px; margin-left:0px;" class="btn btn-primary" data-toggle="modal" data-target="#preview_vendor_pricing">
                                <i class="fa fa-eye"></i> &nbsp; Preview
                            </button>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="VendorPricingTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Page</th>
                                        <th>Created At</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($contents as $content) : ?>
                                        <?php if ($content->page_assignment == "Vendor Pricing Page") : ?>
                                            <tr>
                                                <td><?= $content->content_id ?></td>
                                                <td><?= $content->content_image ? $content->content_image : 'N/A'; ?></td>
                                                <td><?= $content->content_title ?></td>
                                                <td><?= $content->page_assignment ?></td>
                                                <td><?= date('M-d-Y h:i:s A', strtotime($content->created_at)) ?></td>
                                                <td>
                                                    <?php if (!$content->is_published) : ?>
                                                        <button onclick="publish_content(<?= $content->content_id ?>)" class="btn btn-success"><span class="fa fa-check"></button>
                                                    <?php else : ?>
                                                        <button onclick="unpublish_content(<?= $content->content_id ?>)" class="btn btn-default"><span class="fa fa-close"></button>
                                                    <?php endif; ?>
                                                    <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_content/<?= $content->content_id ?>"><span class="fa fa-pencil"></span></a>
                                                    <button onclick="delete_content(<?= $content->content_id ?>)" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->

    </section>
    <!-- /.content -->

    <div data-keyboard="false" class="modal fade" id="view_job_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>

<!-- Modal -->
<div class="modal fade" id="preview_landing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:100vw; margin:0px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <object style="height:83vh; width:100%;" type="text/html" data="<?= base_url() ?>Admin/preview_landing"></object>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="preview_job_pricing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:100vw; margin:0px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <object style="height:83vh; width:100%;" type="text/html" data="<?= base_url() ?>Admin/preview_job_pricing"></object>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="preview_vendor_pricing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:100vw; margin:0px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <object style="height:83vh; width:100%;" type="text/html" data="<?= base_url() ?>Admin/preview_vendor_pricing"></object>
            </div>
        </div>
    </div>
</div>