<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Subscriber
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/subscribers">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/create_subscriber') ?>" method="post">

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Select a company</strong></h4>
                                    </div>
                                    <div class="col-md-6">
                                        <h4><strong>Select a package</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-6 <?= form_error('company_id')  ? 'has-warning' : ''; ?>">
                                        <label for="company_id">Company Name</label>
                                        <select class="form-control" name="company_id" id="company_id">
                                            <option value="">--</option>
                                            <?php foreach ($companies as $company) : ?>
                                                <option <?= set_value('company_id') == $company->CompanyName ? 'selected' : ''; ?> value="<?= $company->CompanyId ?>"><?= $company->CompanyName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('package_id')  ? 'has-warning' : ''; ?>">
                                        <label for="package_id">Package Name</label>
                                        <select class="form-control" name="package_id" id="package_id">
                                            <option value="">--</option>
                                            <?php foreach ($packages as $package) : ?>
                                                <option <?= set_value('package_id') == $package->package_name ? 'selected' : ''; ?> value="<?= $package->package_id ?>"><?= $package->package_name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div id="company_details"></div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>