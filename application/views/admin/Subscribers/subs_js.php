<!-- DataTables -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- CK Editor -->
<script src="<?= base_url() ?>assets/bower_components/ckeditor/ckeditor.js"></script>

<script>
    $(function() {

        $('#JobServiceTable').DataTable();
        $('#VendorServiceTable').DataTable();
        $('#ProcessedServiceTable').DataTable();
    })

    $('#company_id').change(function() {
        // alert('sample');
        $.ajax({
            url: "<?php echo site_url('Admin/get_company'); ?>",
            method: "POST",
            data: {
                CompanyId: $('#company_id').val()
            },
            success: function(data) {
                $('#company_details').html(data);
            }
        });
    });

    function delete_company(CompanyId) {
        swal({
                title: "Are you sure?",
                text: "You wont be able to undo this action",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/delete_company'); ?>",
                        method: "POST",
                        data: {
                            CompanyId: CompanyId
                        },
                        success: function(data) {
                            swal("Deleted", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/companies";
                            });
                        }
                    });
                }
            });
    }

    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('#JobTabs a[href="' + activeTab + '"]').tab('show');
    }
</script>