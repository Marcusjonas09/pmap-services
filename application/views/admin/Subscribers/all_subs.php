<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subscription Requests
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- <div id="page-wrapper"> -->
        <?php if (isset($message)) : ?>
            <?= $message ?>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="JobTabs">
                        <li class="active"><a href="#JSR" data-toggle="tab">Job Service Requests</a></li>
                        <li><a href="#VSR" data-toggle="tab">Vendors Service Requests</a></li>
                        <li><a href="#processed" data-toggle="tab">Processed Requests</a></li>
                        <a href="<?= base_url() ?>Admin/add_subscriber" style="margin-right:10px; margin-top:5px;" class="btn btn-success pull-right"><i class="fa fa-plus"></i> &nbsp; Add Subscriber</a>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="JSR">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="JobServiceTable">
                                <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Date Submitted</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center" width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="unpublished_body">
                                    <?php foreach ($jobs as $job) : ?>
                                        <tr>
                                            <td><?= $job->sub_id ?></td>
                                            <td><?= $job->CompanyName ?></td>
                                            <td><?= $job->ContactFullname ?></td>
                                            <td><?php if ($job->date_submitted == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y h:i:s A', strtotime($job->date_submitted));
                                                } ?></td>
                                            <td class="text-center">
                                                <?php
                                                if ($job->sub_status  == 1) {
                                                    echo "<span class='label label-success'>Approved</span>";
                                                } elseif ($job->sub_status  == 2) {
                                                    echo "<span class='label label-warning'>Pending</span>";
                                                } else {
                                                    echo "<span class='label label-danger'>Denied</span>";
                                                } ?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url() ?>Admin/view_subscriber/<?= $job->sub_id ?>" class="btn btn-primary"><span class="fa fa-eye"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="VSR">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="VendorServiceTable">
                                <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Date Submitted</th>
                                        <th>Status</th>
                                        <th class="text-center" width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vendors as $vendor) : ?>
                                        <tr>
                                            <td><?= $vendor->sub_id ?></td>
                                            <td><?= $vendor->CompanyName ?></td>
                                            <td><?= $vendor->ContactFullname ?></td>
                                            <td><?php if ($vendor->date_submitted == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y h:i:s A', strtotime($vendor->date_submitted));
                                                } ?></td>
                                            <td class="text-center">
                                                <?php
                                                if ($vendor->sub_status  == 1) {
                                                    echo "<span class='label label-success'>Approved</span>";
                                                } elseif ($vendor->sub_status  == 2) {
                                                    echo "<span class='label label-warning'>Pending</span>";
                                                } else {
                                                    echo "<span class='label label-danger'>Denied</span>";
                                                } ?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url() ?>Admin/view_subscriber/<?= $vendor->sub_id ?>" class="btn btn-primary"><span class="fa fa-eye"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="processed">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="ProcessedServiceTable">
                                <thead>
                                    <tr>
                                        <th>Request ID</th>
                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Date Submitted</th>
                                        <th>Status</th>
                                        <th class="text-center" width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($subscribers as $subscriber) : ?>
                                        <tr>
                                            <td><?= $subscriber->sub_id ?></td>
                                            <td><?= $subscriber->CompanyName ?></td>
                                            <td><?= $subscriber->ContactFullname ?></td>
                                            <td><?php if ($subscriber->date_submitted == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y h:i:s A', strtotime($subscriber->date_submitted));
                                                } ?></td>
                                            <td class="text-center">
                                                <?php
                                                if ($subscriber->sub_status  == 1) {
                                                    echo "<span class='label label-success'>Approved</span>";
                                                } elseif ($subscriber->sub_status  == 2) {
                                                    echo "<span class='label label-warning'>Pending</span>";
                                                } else {
                                                    echo "<span class='label label-danger'>Denied</span>";
                                                } ?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url() ?>Admin/view_subscriber/<?= $subscriber->sub_id ?>" class="btn btn-primary"><span class="fa fa-eye"></span></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->

    </section>
    <!-- /.content -->

    <div data-keyboard="false" class="modal fade" id="view_job_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>