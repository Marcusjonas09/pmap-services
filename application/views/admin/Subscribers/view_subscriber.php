<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subscriber Details
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/subscribers">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>

                            <?php if ($subscriber->sub_status == 2) : ?>
                                <a class="btn btn-success pull-right" href="<?= base_url() ?>Admin/approve_subscriber/<?= $this->uri->segment(3) ?>"><span class="fa fa-check"></span> &nbsp; Approve</a>
                            <?php endif; ?>
                        </div>

                        <div class="box-body">

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h4><strong>Subscription Status: <?= ($subscriber->sub_status == 1) ? '<span class="label bg-green">Approved</span>' : '<span class="label bg-yellow">Pending</span>'; ?></strong></h4>
                                        <label>Date Submitted</label>
                                        <input disabled value="<?= ($subscriber->date_submitted != 0) ? date('M-d-Y h:i:s A', strtotime($subscriber->date_submitted)) : 'N/A'; ?>" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Start Date</label>
                                        <input disabled value="<?= ($subscriber->sub_start != 0) ? date('M-d-Y h:i:s A', strtotime($subscriber->sub_start)) : 'N/A'; ?>" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>End Date</label>
                                        <input disabled value="<?= ($subscriber->sub_end != 0) ? date('M-d-Y h:i:s A', strtotime($subscriber->sub_end)) : 'N/A'; ?>" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>

                            </div>

                            <hr style="border:1px solid black;">

                            <div class="row">
                                <div class="col-md-6">
                                    <h4><strong>Company Details</strong></h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="CompanyName">Company Name</label>

                                    <input disabled value="<?= $subscriber->CompanyName ?>" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="CompanyAddress">Company Address</label>
                                    <input disabled value="<?= $subscriber->CompanyAddress ?>" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="CompanyTelNo">Telephone No. </label>
                                    <input disabled value="<?= $subscriber->CompanyTelNo ?>" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                </div>

                                <div class="form-group col-md-3">
                                    <label for="CompanyCelNo">Mobile No.</label>
                                    <input disabled value="<?= $subscriber->CompanyCelNo ?>" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="CompanyEmail">Company Email</label>
                                    <input disabled value="<?= $subscriber->CompanyEmail ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="CompanyEmail">HR Email Address</label>
                                    <input disabled value="<?= $subscriber->HRemail ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <h4><strong>Contact Person</strong></h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="ContactFname">Fullname</label>
                                    <input disabled value="<?= $subscriber->ContactFullname ?>" type="text" name="ContactFname" id="ContactFname" placeholder="Fullname" class="form-control">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="ContactPosition">Position in the company</label>
                                    <input disabled value="<?= $subscriber->ContactPosition ?>" type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="ContactTelNo">Telephone No.</label>
                                    <input disabled value="<?= $subscriber->ContactNo ?>" type="text" name="ContactTelNo" id="ContactTelNo" placeholder="Telephone No" class="form-control">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="ContactEmail">Email</label>
                                    <input disabled value="<?= $subscriber->ContactEmail ?>" type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control">
                                </div>
                            </div>

                            <hr style="border:1px solid black;">

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>Package Details</strong></h4>
                                        <label>Package Name</label>
                                        <input disabled value="<?= $subscriber->package_name ?>" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Package Duration</label>
                                        <input disabled value="<?= $subscriber->package_duration ?> day/s" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Post Limit (Daily)</label>
                                        <input disabled value="<?= $subscriber->post_limit_daily ?> post/s" class="form-control" type="text" name="post_limit_daily" param_name placeholder="Number of posts allowed per day">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Post Limit (Monthly)</label>
                                        <input disabled value="<?= $subscriber->post_limit_monthly ?> post/s" class="form-control" type="text" name="post_limit_monthly" param_name placeholder="Number of posts allowed per month">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Post Duration</label>
                                        <input disabled value="<?= $subscriber->post_duration ?> day/s" class="form-control" type="text" name="post_duration" param_name placeholder="Duration of a single job post in days">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Package Cost</label>
                                        <input disabled value="<?= '&#8369;' . $subscriber->package_cost ?>" class="form-control" type="text" name="package_cost" param_name placeholder="Package Cost (Philippine Peso)">
                                    </div>
                                </div>

                            </div>

                            <hr style="border:1px solid black;">

                            <div class="row">
                                <div class="col-md-6">
                                    <h4><strong>Current Usage</strong></h4>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Daily posts left</label>
                                        <input disabled value="<?= ($subscriber->post_limit_daily - $subscriber->daily_post) ?> post/s" class="form-control" type="text" name="post_limit_daily" param_name placeholder="Number of posts allowed per day">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Monthly posts left</label>
                                        <input disabled value="<?= $subscriber->post_limit_monthly - $subscriber->monthly_post ?> post/s" class="form-control" type="text" name="post_limit_monthly" param_name placeholder="Number of posts allowed per month">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>