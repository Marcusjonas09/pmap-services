<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Edit Package</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php if (isset($message)) : ?>
                    <?= $message ?>
                <?php endif; ?>
            </div>
        </div>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/packages">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/edit_package_function') ?>" method="post">
                            <div class="box-body">
                                <input type="hidden" name="package_id" value="<?= $package->package_id ?>">
                                <div class="col-md-6">
                                    <div class="form-group <?= form_error('package_name') ? 'has-warning' : ''; ?>">
                                        <h4><strong>Package Details</strong></h4>
                                        <label>Package Name</label>
                                        <input value="<?= $package->package_name ?>" class=" form-control" type="text" name="package_name" param_name placeholder="Package Name">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group <?= form_error('package_duration') ? 'has-warning' : ''; ?>">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Package Duration</label>
                                        <input value="<?= $package->package_duration ?>" class=" form-control" type="text" name="package_duration" param_name placeholder="Package duration in days">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group <?= form_error('package_type') ? 'has-warning' : ''; ?>">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Package Type</label>
                                        <select class="form-control" name="package_type" id="package_type">
                                            <option <?= ($package->package_type == 'job') ? 'selected' : '' ?> value="job">Job</option>
                                            <option <?= ($package->package_type == 'vendor') ? 'selected' : '' ?> value="vendor">Vendor</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group <?= form_error('post_limit_daily') ? 'has-warning' : ''; ?>">
                                        <h4><strong>Job Posting Details</strong></h4>
                                        <label>Post Limit (Daily)</label>
                                        <input value="<?= $package->post_limit_daily ?>" class="form-control" type="number" name="post_limit_daily" id="post_limit_daily" param_name placeholder="Number of posts allowed per day">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group <?= form_error('post_limit_monthly') ? 'has-warning' : ''; ?>">
                                        <h4><strong>&nbsp;</strong></h4>
                                        <label>Post Limit (Monthly)</label>
                                        <input value="<?= $package->post_limit_monthly ?>" class="form-control" type="number" name="post_limit_monthly" id="post_limit_monthly" param_name placeholder="Number of posts allowed per month">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group <?= form_error('post_duration') ? 'has-warning' : ''; ?>">
                                        <label>Post Duration</label>
                                        <input value="<?= $package->post_duration ?>" class="form-control" type="number" name="post_duration" param_name placeholder="Duration of a single job post in days">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group <?= form_error('package_cost') ? 'has-warning' : ''; ?>">
                                        <label>Package Cost</label>
                                        <input value="<?= $package->package_cost ?>" class="form-control" type="number" name="package_cost" param_name placeholder="Package Cost (Philippine Peso)">
                                    </div>


                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>