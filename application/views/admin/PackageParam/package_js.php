<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>

<script>
    $(document).ready(function() {
        $('#post_limit_daily').keyup(function() {
            var daily = $('#post_limit_daily').val() * 30;
            $('#post_limit_monthly').val(daily);
        });

        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#PackageTabs a[href="' + activeTab + '"]').tab('show');
        }
    });

    function delete_package(id) {
        swal({
                title: "Are you sure?",
                text: "You wont be able to undo this action",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/delete_package'); ?>",
                        method: "POST",
                        data: {
                            package_id: id
                        },
                        success: function(data) {
                            swal("Deleted", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/packages";
                            });
                        }
                    });
                }
            });
    }

    function view_package(id) {
        $.ajax({
            url: "<?php echo site_url('Admin/view_package'); ?>",
            method: "POST",
            data: {
                package_id: id
            },
            success: function(data) {
                $('#view_package_modal').html(data);
                $('#view_package_modal').modal('show');
            }
        });
    }
</script>