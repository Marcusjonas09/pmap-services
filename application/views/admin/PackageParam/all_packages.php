<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Packages
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- <div id="page-wrapper"> -->
        <?php if (isset($message)) : ?>
            <?= $message ?>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="PackageTabs">
                        <li class="active"><a href="#jobs" data-toggle="tab">Job Packages</a></li>
                        <li><a href="#vendors" data-toggle="tab">Vendors Packages</a></li>
                        <a class="btn btn-success pull-right" style="margin-right:10px; margin-top:5px;" href="<?= base_url() ?>Admin/create_package"><span class="fa fa-plus"></span> Create Package</a>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="jobs">
                            <table width="100%" class="table table-striped table-hover" id="all_packages">
                                <thead>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Limit (daily)</th>
                                    <th>Limit (monthly)</th>
                                    <th>Post Duration</th>
                                    <th>Price</th>
                                    <th style="width:20%;">Action</th>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($job_packages as $job_package) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $job_package->package_name ?></td>
                                            <td><?= $job_package->post_limit_daily . ' posts' ?></td>
                                            <td><?= $job_package->post_limit_monthly . ' posts' ?></td>
                                            <td><?= $job_package->post_duration . ' day/s' ?></td>
                                            <td><?= $job_package->package_cost ?></td>
                                            <td>
                                                <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_package/<?= $job_package->package_id ?>"><span class="fa fa-pencil"></span></a>
                                                <button class="btn btn-primary" onclick="view_package(<?= $job_package->package_id ?>)"><span class="fa fa-eye"></span></button>
                                                <button class="btn btn-danger" onclick="delete_package(<?= $job_package->package_id ?>)"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="vendors">
                            <table width="100%" class="table table-striped table-hover" id="all_packages">
                                <thead>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Limit (daily)</th>
                                    <th>Limit (monthly)</th>
                                    <th>Post Duration</th>
                                    <th>Price</th>
                                    <th style="width:20%;">Action</th>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($vendor_packages as $vendor_package) : ?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $vendor_package->package_name ?></td>
                                            <td><?= $vendor_package->post_limit_daily . ' posts' ?></td>
                                            <td><?= $vendor_package->post_limit_monthly . ' posts' ?></td>
                                            <td><?= $vendor_package->post_duration . ' day/s' ?></td>
                                            <td><?= $vendor_package->package_cost ?></td>
                                            <td>
                                                <a class="btn btn-warning" href="<?= base_url() ?>Admin/edit_package/<?= $vendor_package->package_id ?>"><span class="fa fa-pencil"></span></a>
                                                <button class="btn btn-primary" onclick="view_package(<?= $vendor_package->package_id ?>)"><span class="fa fa-eye"></span></button>
                                                <button class="btn btn-danger" onclick="delete_package(<?= $vendor_package->package_id ?>)"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->

    </section>
    <!-- /.content -->

    <div data-keyboard="false" class="modal fade" id="view_job_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>

<div data-keyboard="false" class="modal fade" id="view_package_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

</div>