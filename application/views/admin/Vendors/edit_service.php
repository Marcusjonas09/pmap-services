<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Service
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if (validation_errors()) : ?>
                        <?= validation_errors() ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/vendors">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/update_service') ?>" method="post">

                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Choose a company</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 <?= form_error('sub_id') ? 'has-warning' : ''; ?> ">
                                        <label for="sub_id">Company Name</label>
                                        <select disabled class="form-control select2" name="sub_id" id="sub_id">
                                            <option value="">--</option>
                                            <?php foreach ($companies as $company) : ?>
                                                <option <?= $Service->sub_id == $company->sub_id ? 'selected' : ''; ?> value="<?= $company->sub_id ?>"><?= $company->CompanyName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('PostDuration')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Post Duration (In days)</label>
                                        <input readonly type="number" name="PostDuration" id="PostDuration" placeholder="Duration in days" value="<?= $Service->PostDuration ?>" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div id="company_details"></div>

                                <div class="row">

                                    <div class="form-group col-md-12 <?= form_error('ServiceTitle')  ? 'has-warning' : ''; ?>">
                                        <label for="ServiceTitle">Service Title</label>
                                        <input value="<?= $Service->ServiceTitle ?>" type="text" name="ServiceTitle" id="ServiceTitle" placeholder="Service Title" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12 <?= form_error('ServiceDescription')  ? 'has-warning' : ''; ?>">
                                        <label for="ServiceDescription">Service Description</label>
                                        <textarea class="form-control" style="resize:none;" name="ServiceDescription" id="ServiceDescription" cols="30" rows="8" placeholder="Description"><?= $Service->ServiceDescription ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="ServiceId" value="<?= $Service->ServiceId ?>">

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>