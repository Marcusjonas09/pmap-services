<!-- DataTables -->
<script src="<?= base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script src="<?= base_url() ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- CK Editor -->
<script src="<?= base_url() ?>assets/bower_components/ckeditor/ckeditor.js"></script>
<script>
    $(function() {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
        })
        CKEDITOR.replace('ServiceDescription');
    })
</script>

<script>
    $.ajax({
        url: "<?php echo site_url('Admin/get_company_for_vendor'); ?>",
        method: "POST",
        data: {
            sub_id: $('#sub_id').val()
        },
        success: function(data) {
            $('#company_details').html(data);
        }
    });

    $('#sub_id').change(function() {
        $.ajax({
            url: "<?php echo site_url('Admin/get_company_for_vendor'); ?>",
            method: "POST",
            data: {
                sub_id: $('#sub_id').val()
            },
            success: function(data) {
                $('#company_details').html(data);
            }
        });
    });

    function delete_unpublished_service(id) {
        swal({
                title: "Are you sure?",
                text: "You wont be able to undo this action",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/delete_unpublished_service'); ?>",
                        method: "POST",
                        data: {
                            ServiceId: id
                        },
                        success: function(data) {
                            swal("Deleted", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/vendors";
                            });
                        }
                    });
                }

            });
    }

    function repost_service(id) {
        swal({
                title: "Are you sure?",
                text: "Reactivating will transfer this post to the unpublished tab and will be counted as a new post",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/repost_service'); ?>",
                        method: "POST",
                        data: {
                            ServiceId: id
                        },
                        success: function(data) {
                            swal("Reposted", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/vendors";
                            });
                        }
                    });
                }

            });
    }

    function publish_service(id) {
        swal({
                title: "Are you sure?",
                text: "The post's validity will start after publishing",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/publish_service'); ?>",
                        method: "POST",
                        data: {
                            ServiceId: id
                        },
                        success: function(data) {
                            response = JSON.parse(data);
                            if (response.status) {
                                swal(response.message, {
                                    icon: response.icon,
                                    buttons: false,
                                    timer: 1100,
                                }).then((value) => {
                                    location.href = "<?= base_url() ?>Admin/vendors";
                                });
                            } else {
                                swal(response.message, {
                                    icon: response.icon,
                                    buttons: false,
                                    timer: 1100,
                                }).then((value) => {
                                    location.href = "<?= base_url() ?>Admin/vendors";
                                });
                            }
                        }
                    });
                }

            });
    }

    function unpublish_service(id) {
        swal({
                title: "Are you sure?",
                text: "The post's validity will not be affected after unpublishing",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "<?php echo site_url('Admin/unpublish_service'); ?>",
                        method: "POST",
                        data: {
                            ServiceId: id
                        },
                        success: function(data) {
                            swal("Unpublished", {
                                icon: "success",
                                buttons: false,
                                timer: 1100,
                            }).then((value) => {
                                location.href = "<?= base_url() ?>Admin/vendors";
                            });
                        }
                    });
                }

            });
    }

    function view_service(id) {
        $.ajax({
            url: "<?php echo site_url('Admin/view_service'); ?>",
            method: "POST",
            data: {
                ServiceId: id
            },
            success: function(data) {
                $('#view_vendor_modal').html(data);
                $('#view_vendor_modal').modal('show');
            }
        });
    }

    function edit_vendor(id) {
        $.ajax({
            url: "<?php echo site_url('Admin/edit_vendor'); ?>",
            method: "POST",
            data: {
                ServiceId: id
            },
            success: function(data) {
                $('#edit_vendor_modal').html(data);
                $('#edit_vendor_modal').modal('show');
            }
        });
    }























    $('.btn-modal-fee').click(function() {
        var request_id = $(this).data("requestid");
        //alert(request_id);
        $.ajax({
            url: "<?php echo site_url('admin/modal_fee'); ?>",
            method: "POST",
            data: {
                id: request_id
            },
            success: function(data) {
                $('#modal_request').html(data);
            }
        });
    });

    $('.btn-modal-cert').click(function() {
        var request_id = $(this).data("requestid");
        //alert(request_id);
        $.ajax({
            url: "<?php echo site_url('user/modal_cert'); ?>",
            method: "POST",
            data: {
                id: request_id
            },
            success: function(data) {
                $('#modal_request').html(data);
            }
        });
    });

    function updateFee(id, status, uid) {
        //alert('wahaha');
        swal({
                title: "Are you sure?",
                text: "You want to update the payment",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    location.href = "<?= base_url() ?>admin/update_fee/" + id + "/" + status + "/" + uid;
                }

            });
    }


    function approveRequest(id, rid) {
        swal({
                title: "Approve Request",
                text: "You want to approve the request",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    location.href = "<?= base_url() ?>admin/update_approve_request/" + id + "/" + rid;
                }

            });
    }


    function deleteFee(id, uid) {
        //alert('wahaha');
        swal({
                title: "Are you sure?",
                text: "You want to delete the payment",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    location.href = "<?= base_url() ?>admin/delete_fee/" + id + "/" + uid;
                }

            });
    }

    function deleteCert(id) {
        //alert('wahaha');
        swal({
                title: "Are you sure?",
                text: "You want to delete the certification request",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    location.href = "<?= base_url() ?>user/delete_cert/" + id;
                }

            });
    }

    function updateFeeParking(id, uid) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;
        //alert('wahaha');
        swal({
                title: "Are you sure?",
                text: "You want set the parking expiration",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    //alert(id);
                    swal("Enter Parking Expiration Date:(yyyy-mm-dd)", {
                            content: {
                                element: "input",
                                attributes: {
                                    value: today,
                                },
                            },
                        })
                        .then((value) => {
                            location.href = "<?= base_url() ?>admin/update_fee_parking/" + id + "/" + value + "/" + uid;
                        });

                }

            });
    }
</script>

<script>
    $(function() {

        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if (activeTab) {
            $('#VendorTabs a[href="' + activeTab + '"]').tab('show');
        }

        $('.select2').select2()
        $('#publishedtbl').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
        $('#unpublishedtbl').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
        $('#expiredtbl').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
        $('#deletedtbl').DataTable({
            "order": [
                [0, "desc"]
            ]
        })


        $('#example1').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
        $('#example2').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
        $('#example3').DataTable({
            "order": [
                [0, "desc"]
            ]
        })
    })
</script>