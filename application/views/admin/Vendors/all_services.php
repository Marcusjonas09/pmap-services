<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Vendor Postings
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- <div id="page-wrapper"> -->
        <?php if (isset($message)) : ?>
            <?= $message ?>
        <?php endif; ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="VendorTabs">
                        <li class="active"><a href="#Unpublished" data-toggle="tab">Unpublished</a></li>
                        <li><a href="#Published" data-toggle="tab">Published</a></li>
                        <li><a href="#Expired" data-toggle="tab">Expired</a></li>
                        <li><a href="#Deleted" data-toggle="tab">Deleted</a></li>
                        <a href="<?= base_url() ?>Admin/add_service" style="margin-right:10px; margin-top:5px;" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Post New Service</a>


                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Unpublished">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="unpublishedtbl">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Service Title</th>
                                        <th>Company</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Post Duration</th>
                                        <th width="25%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="unpublished_body">
                                    <?php foreach ($unpublished as $post) : ?>
                                        <tr>
                                            <td><?= $post->ServiceId ?></td>
                                            <td><?= $post->ServiceTitle ?></td>
                                            <td><?= $post->CompanyName ?></td>
                                            <td><?php if ($post->CreatedAt == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y H:i:s A', strtotime($post->CreatedAt));
                                                } ?></td>
                                            <td><?php if ($post->ExpireAt == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y H:i:s A', strtotime($post->ExpireAt));
                                                } ?></td>
                                            <td><?= $post->PostDuration . ' day/s' ?></td>
                                            <td>
                                                <button class="col-md-4 btn btn-success" style="margin-right:5px;" onclick="publish_service(<?= $post->ServiceId ?>)"><?= $post->WasPublished ? 'Republish' : 'Publish'; ?></button>

                                                <button class="btn btn-primary" onclick="view_service(<?= $post->ServiceId ?>)"><span class="fa fa-eye"></span></button>

                                                <a href="<?= base_url() ?>Admin/edit_service/<?= $post->ServiceId ?>" class="btn btn-warning"><span class=" fa fa-pencil"></span></a>

                                                <button class="btn btn-danger" onclick="delete_unpublished_service(<?= $post->ServiceId ?>)"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="Published">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="publishedtbl">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Days Left</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($published as $post) : ?>
                                        <tr>
                                            <td><?= $post->ServiceId ?></td>
                                            <td><?= $post->ServiceTitle ?></td>
                                            <td><?= $post->CompanyName ?></td>
                                            <td><?= date("M-d-Y h:i:s A", strtotime($post->CreatedAt)) ?></td>
                                            <td><?= date("M-d-Y h:i:s A", strtotime($post->ExpireAt)) ?></td>
                                            <td><?= number_format((strtotime($post->ExpireAt) - time()) / (3600 * 24), 0) . ' day/s' ?></td>
                                            <td>
                                                <button class="btn btn-danger" onclick="unpublish_service(<?= $post->ServiceId ?>)">Unpublish</button>
                                                <button class="btn btn-primary" onclick="view_service(<?= $post->ServiceId ?>)"><span class="fa fa-eye"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="Expired">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="expiredtbl">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Company</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($expired as $post) : ?>
                                        <tr>
                                            <td><?= $post->ServiceId ?></td>
                                            <td><?= $post->ServiceTitle ?></td>
                                            <td><?= $post->CompanyName ?></td>
                                            <td><?= date("M-d-Y h:i:s A", strtotime($post->CreatedAt)) ?></td>
                                            <td><?= date("M-d-Y h:i:s A", strtotime($post->ExpireAt)) ?></td>
                                            <td>
                                                <button onclick="repost_service(<?= $post->ServiceId ?>)" class="btn btn-success">Repost</button>
                                                <button class="btn btn-primary" onclick="view_service(<?= $post->ServiceId ?>)"><span class="fa fa-eye"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="Deleted">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="deletedtbl">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Job Title</th>
                                        <th>Company</th>
                                        <th>Start</th>
                                        <th>End</th>
                                        <th>Days Left</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($deleted as $post) : ?>
                                        <tr>
                                            <td><?= $post->ServiceId ?></td>
                                            <td><?= $post->ServiceTitle ?></td>
                                            <td><?= $post->CompanyName ?></td>
                                            <td><?php if ($post->CreatedAt == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y H:i:s A', strtotime($post->CreatedAt));
                                                } ?></td>
                                            <td><?php if ($post->ExpireAt == 0) {
                                                    echo "N/A";
                                                } else {
                                                    echo date('M-d-Y H:i:s A', strtotime($post->ExpireAt));
                                                } ?></td>
                                            <td><?= number_format(((strtotime($post->ExpireAt) - time()) / (3600 * 24)) > 0 ? ((strtotime($post->ExpireAt) - time()) / (3600 * 24)) : 0, 0) . ' day/s' ?></td>
                                            <td>
                                                <button class="btn btn-primary" onclick="view_service(<?= $post->ServiceId ?>)"><span class="fa fa-eye"></span></button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->

    </section>
    <!-- /.content -->

    <div data-backdrop="static" data-keyboard="false" class="modal fade" id="view_vendor_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>

    <div data-backdrop="static" data-keyboard="false" class="modal fade" id="edit_vendor_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    </div>

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>