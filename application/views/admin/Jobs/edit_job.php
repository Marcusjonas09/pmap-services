<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Job
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/jobs">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/update_job') ?>" method="post">

                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><strong>Choose Company</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-6 <?= form_error('sub_id')  ? 'has-warning' : ''; ?>">
                                        <label for="sub_id">Company Name</label>
                                        <select disabled class="form-control select2" name="sub_id" id="sub_id">
                                            <option value="">--</option>
                                            <?php foreach ($companies as $company) : ?>
                                                <option <?= $Job->sub_id == $company->sub_id ? 'selected' : ''; ?> value="<?= $company->sub_id ?>"><?= $company->CompanyName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('PostDuration')  ? 'has-warning' : ''; ?>">
                                        <label for="PostDuration">Post Duration</label>
                                        <input readonly value="<?= $Job->PostDuration ?>" type="text" name="PostDuration" id="PostDuration" placeholder="Duration in day/s" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div id="company_details"></div>

                                <hr style="border:1px solid black;">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Job Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-6 <?= form_error('JobPositionTitle')  ? 'has-warning' : ''; ?>">
                                        <label for="JobPositionTitle">Job Title</label>
                                        <input value="<?= $Job->JobPositionTitle ?>" type="text" name="JobPositionTitle" id="JobPositionTitle" placeholder="Job Title" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobLocation')  ? 'has-warning' : ''; ?>">
                                        <label for="JobLocation">Location</label>
                                        <input value="<?= $Job->JobLocation ?>" type="text" name="JobLocation" id="JobLocation" placeholder="Location" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12 <?= form_error('JobDescription')  ? 'has-warning' : ''; ?>">
                                        <label for="JobDescription">Job Description</label>
                                        <textarea class="form-control" style="resize:none;" name="JobDescription" id="JobDescription" cols="30" rows="8" placeholder="Job Description"><?= $Job->JobDescription ?></textarea>
                                    </div>

                                    <div class="form-group col-md-12 <?= form_error('JobRequirements')  ? 'has-warning' : ''; ?>">
                                        <label for="JobRequirements">Job Requirements</label>
                                        <textarea class="form-control" style="resize:none;" name="JobRequirements" id="JobRequirements" cols="30" rows="8" placeholder="Job Requirements"><?= $Job->JobRequirements ?></textarea>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('Industry')  ? 'has-warning' : ''; ?>">
                                        <label for="Idustry">Industry</label>

                                        <select class="form-control" name="Industry" id="Industry">
                                            <?php foreach ($Industries as $Industry) : ?>
                                                <option <?= $Job->Industry == $Industry->pv_value ? 'selected' : ''; ?> value="<?= $Industry->pv_value ?>"><?= $Industry->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobFunction')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Job Function</label>
                                        <select class="form-control" name="JobFunction" id="JobFunction">
                                            <?php foreach ($JobFunctions as $JobFunction) : ?>
                                                <option <?= $Job->JobFunction == $JobFunction->pv_value ? 'selected' : ''; ?> value="<?= $JobFunction->pv_value ?>"><?= $JobFunction->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobType')  ? 'has-warning' : ''; ?>">
                                        <label for="JobType">Job Type</label>
                                        <select class="form-control" name="JobType" id="JobType">
                                            <?php foreach ($JobTypes as $JobType) : ?>
                                                <option <?= $Job->JobType == $JobType->pv_value ? 'selected' : ''; ?> value="<?= $JobType->pv_value ?>"><?= $JobType->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobDuration')  ? 'has-warning' : ''; ?>">
                                        <label for="JobDuration">Job Duration</label>
                                        <select class="form-control" name="JobDuration" id="JobDuration">
                                            <?php foreach ($JobDurations as $JobDuration) : ?>
                                                <option <?= $Job->JobDuration == $JobDuration->pv_value ? 'selected' : ''; ?> value="<?= $JobDuration->pv_value ?>"><?= $JobDuration->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinEducation')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Education</label>
                                        <select class="form-control" name="MinEducation" id="MinEducation">
                                            <?php foreach ($MinEducations as $MinEducation) : ?>
                                                <option <?= $Job->MinEducation == $MinEducation->pv_value ? 'selected' : ''; ?> value="<?= $MinEducation->pv_value ?>"><?= $MinEducation->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinExperience')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Experience</label>
                                        <select class="form-control" name="MinExperience" id="MinExperience">
                                            <?php foreach ($MinExperiences as $MinExperience) : ?>
                                                <option <?= $Job->MinExperience == $MinExperience->pv_value ? 'selected' : ''; ?> value="<?= $MinExperience->pv_value ?>"><?= $MinExperience->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinSalary')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Salary</label>
                                        <input type="text" name="MinSalary" id="MinSalary" placeholder="Minimum Salary" value="<?= $Job->MinSalary ?>" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MaxSalary')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Maximum Salary</label>
                                        <input type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="<?= $Job->MaxSalary ?>" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('RequiredTravel')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Required Travel</label>
                                        <select class="form-control" name="RequiredTravel" id="MinExperience">
                                            <?php foreach ($RequiredTravels as $RequiredTravel) : ?>
                                                <option <?= $Job->RequiredTravel == $RequiredTravel->pv_value ? 'selected' : ''; ?> value="<?= $RequiredTravel->pv_value ?>"><?= $RequiredTravel->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <input type="hidden" name="JobId" value="<?= $Job->JobId ?>">

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                        <a class="btn btn-default pull-right" style="margin-right:10px;" href="<?= base_url() ?>Admin/jobs"> Cancel
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>