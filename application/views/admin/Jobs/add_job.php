<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Post Job
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($message)) : ?>
                        <?= $message ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <a class="btn btn-default" href="<?= base_url() ?>Admin/jobs">
                                <span class="fa  fa-chevron-left"></span>&nbsp Back
                            </a>
                        </div>
                        <form action="<?= site_url('Admin/post_job') ?>" method="post">

                            <div class="box-body">


                                <?php if ($this->session->role == 'admin') : ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4><strong>Choose Company</strong></h4>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="form-group col-md-6 <?= form_error('sub_id')  ? 'has-warning' : ''; ?>">
                                            <label for="sub_id">Company Name</label>
                                            <select class="form-control select2" name="sub_id" id="sub_id">
                                                <option <?= set_select('sub_id', "", true) ?> value="">--</option>
                                                <?php foreach ($companies as $company) : ?>
                                                    <option <?= set_select('sub_id', $company->sub_id) ?> value="<?= $company->sub_id ?>"><?= $company->CompanyName ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="form-group col-md-6 <?= form_error('PostDuration')  ? 'has-warning' : ''; ?>">
                                            <label for="PostDuration">Post Duration</label>
                                            <input value="<?= set_value('PostDuration') ?>" type="text" name="PostDuration" id="PostDuration" placeholder="Duration in day/s" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><strong>Company Details</strong></h4>
                                        </div>
                                    </div>

                                    <div id="company_details"></div>

                                <?php else : ?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4><strong>Company Details</strong></h4>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="CompanyName">Company Name</label>

                                            <input readonly value="<?= $this->session->services_user['company']['name'] ?>" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="CompanyAddress">Company Address</label>
                                            <input readonly value="<?= $this->session->services_user['company']['address'] ?>" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="CompanyTelNo">Contact No. </label>
                                            <input readonly value="<?= $this->session->services_user['company']['contact'] ?>" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                        </div>

                                        <!-- <div class="form-group col-md-3">
                                            <label for="CompanyCelNo">Mobile No.</label>
                                            <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                        </div> -->

                                        <div class="form-group col-md-6">
                                            <label for="CompanyEmail">Company Email</label>
                                            <input readonly value="<?= $this->session->services_user['company']['email'] ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="CompanyEmail">HR Email Address</label>
                                            <input readonly value="<?= $this->session->services_user['company']['email'] ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                        </div>
                                    </div>
                                    <input type="hidden" name="CompanyId" value="<?= $this->session->services_user['company']['id'] ?>">
                                    <input type="hidden" name="CompanyName" value="<?= $this->session->services_user['company']['name'] ?>">
                                    <input type="hidden" name="CompanyLocation" value="<?= $this->session->services_user['company']['address'] ?>">
                                    <input type="hidden" name="PackageType" value="' . $company->package_type . '">
                                <?php endif; ?>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Job Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-6 <?= form_error('JobPositionTitle')  ? 'has-warning' : ''; ?>">
                                        <label for="JobPositionTitle">Job Title</label>
                                        <input value="<?= set_value('JobPositionTitle') ?>" type="text" name="JobPositionTitle" id="JobPositionTitle" placeholder="Job Title" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobLocation')  ? 'has-warning' : ''; ?>">
                                        <label for="JobLocation">Location</label>
                                        <input value="<?= set_value('JobLocation') ?>" type="text" name="JobLocation" id="JobLocation" placeholder="Location" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12 <?= form_error('JobDescription')  ? 'has-warning' : ''; ?>">
                                        <label for="JobDescription">Job Description</label>
                                        <textarea class="form-control" style="resize:none;" name="JobDescription" id="JobDescription" cols="30" rows="8" placeholder="Job Description"><?= set_value('JobDescription') ?></textarea>
                                    </div>

                                    <div class="form-group col-md-12 <?= form_error('JobRequirements')  ? 'has-warning' : ''; ?>">
                                        <label for="JobRequirements">Job Requirements</label>
                                        <textarea class="form-control" style="resize:none;" name="JobRequirements" id="JobRequirements" cols="30" rows="8" placeholder="Job Requirements"><?= set_value('JobRequirements') ?></textarea>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('Industry')  ? 'has-warning' : ''; ?>">
                                        <label for="Idustry">Industry</label>

                                        <select class="form-control" name="Industry" id="Industry">
                                            <?php foreach ($Industries as $Industry) : ?>
                                                <option <?= set_value('Industry') == $Industry->pv_value ? 'selected' : ''; ?> value="<?= $Industry->pv_value ?>"><?= $Industry->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobFunction')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Job Function</label>
                                        <select class="form-control" name="JobFunction" id="JobFunction">
                                            <?php foreach ($JobFunctions as $JobFunction) : ?>
                                                <option <?= set_value('JobFunction') == $JobFunction->pv_value ? 'selected' : ''; ?> value="<?= $JobFunction->pv_value ?>"><?= $JobFunction->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobType')  ? 'has-warning' : ''; ?>">
                                        <label for="JobType">Job Type</label>
                                        <select class="form-control" name="JobType" id="JobType">
                                            <?php foreach ($JobTypes as $JobType) : ?>
                                                <option <?= set_value('JobType') == $JobType->pv_value ? 'selected' : ''; ?> value="<?= $JobType->pv_value ?>"><?= $JobType->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('JobDuration')  ? 'has-warning' : ''; ?>">
                                        <label for="JobDuration">Job Duration</label>
                                        <select class="form-control" name="JobDuration" id="JobDuration">
                                            <?php foreach ($JobDurations as $JobDuration) : ?>
                                                <option <?= set_value('JobDuration') == $JobDuration->pv_value ? 'selected' : ''; ?> value="<?= $JobDuration->pv_value ?>"><?= $JobDuration->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinEducation')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Education</label>
                                        <select class="form-control" name="MinEducation" id="MinEducation">
                                            <?php foreach ($MinEducations as $MinEducation) : ?>
                                                <option <?= set_value('MinEducation') == $MinEducation->pv_value ? 'selected' : ''; ?> value="<?= $MinEducation->pv_value ?>"><?= $MinEducation->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinExperience')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Experience</label>
                                        <select class="form-control" name="MinExperience" id="MinExperience">
                                            <?php foreach ($MinExperiences as $MinExperience) : ?>
                                                <option <?= set_value('MinExperience') == $MinExperience->pv_value ? 'selected' : ''; ?> value="<?= $MinExperience->pv_value ?>"><?= $MinExperience->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MinSalary')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Minimum Salary</label>
                                        <input type="text" name="MinSalary" id="MinSalary" placeholder="Minimum Salary" value="<?= set_value('MinSalary') ?>" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('MaxSalary')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Maximum Salary</label>
                                        <input type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="<?= set_value('MaxSalary') ?>" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6 <?= form_error('RequiredTravel')  ? 'has-warning' : ''; ?>">
                                        <label for="JobFunction">Required Travel</label>
                                        <select class="form-control" name="RequiredTravel" id="MinExperience">
                                            <?php foreach ($RequiredTravels as $RequiredTravel) : ?>
                                                <option <?= set_value('RequiredTravel') == $RequiredTravel->pv_value ? 'selected' : ''; ?> value="<?= $RequiredTravel->pv_value ?>"><?= $RequiredTravel->pv_value ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <?php if ($this->session->role == 'normal') : ?>
                                        <div class="form-group col-md-6 <?= form_error('PostDuration')  ? 'has-warning' : ''; ?>">
                                            <label for="PostDuration">Post Duration</label>
                                            <input value="<?= set_value('PostDuration') ?>" type="text" name="PostDuration" id="PostDuration" placeholder="Duration in day/s" class="form-control">
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="box-footer">
                                <div class="row">
                                    <div class="container-fluid">
                                        <button class="btn btn-success pull-right" type="submit"> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?= $this->config->item('year') ?> <?= $this->config->item('footer') ?></strong> All rights
    reserved.
</footer>