<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user fa-fw"></i> Adminsitrator
        <small>Record</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Administrators</li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Administrator Edit Record
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a href="<?=base_url()?>admin/admins" 
                        class="btn btn-success btn-md" role="button">BACK</a>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                               <?php if(validation_errors()): ?>
                                  <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <h4><i class="icon fa fa-warning"></i> Invalid!</h4>
                                    <?=validation_errors()?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-12 text-blue"><h2>Edit Profile</h2></div>
                        <form role="form" action="<?=base_url()?>admin/update_admin" method="post">
                            <div class="col-lg-6">
                                <input type="hidden" name="a_id" value="<?=$admin->id?>">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="<?=set_value('admin_name',$admin->admin_name)?>" name="admin_name" class="form-control" placeholder="Enter Admin Name">
                                </div>
                                 <div class="form-group">
                            <label>Status </label>
                            <label class="radio-inline">
                                <input type="radio" name="status" id="gender" value="1" 
                                <?php echo set_value('status', $admin->status)=='1'?'checked':''; ?> 
                                > Active
                            </label> 
                            <label class="radio-inline">
                                <input type="radio" name="status" id="gender" value="0"
                                <?php echo set_value('status', $admin->status)=='0'?'checked':''; ?>
                                > Not Active
                            </label>
                        </div>
                        <button name="update_info" type="submit" class="btn btn-primary">Update Record</button>
                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        
                </form>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->

            <div class="row">
                <hr/>

                <div class="col-lg-6">
                    <h2 class="text-blue">Change Username and Password</h2>
                    <form role="form" action="<?=base_url()?>admin/update_admin" method="post">
                        <input type="hidden" name="a_id" value="<?=$admin->id?>">
                        <div class="form-group">
                        <label>Username</label>
                            <input value="<?=set_value('admin_username',$admin->admin_username)?>" type="text" name="admin_username" class="form-control"  >
                        </div>
                        <!-- <div class="form-group">
                            <label>Old Password</label>
                            <input type="password" name="upass" class="form-control" placeholder="Old Password" >
                        </div>
 -->
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="admin_password" class="form-control" placeholder="Password">

                        </div>

                        <!-- <div class="form-group">
                            <label>Password Confirm</label>
                            <input type="password" name="upassnewconf" class="form-control" placeholder="Confirm Password">
                        </div> -->
                        <button name="updatepassword"  type="submit" class="btn btn-warning">Update Password</button>
                    </form>


                </div>

                <!-- /.col-lg-6 (nested) -->
                <div class="col-lg-6">
                    <h2 class="text-blue">Upload Picture</h2>
                    <form role="form" action="<?=base_url()?>admin/update_admin" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="a_id" value="<?=$admin->id?>">
                        <div class="form-group">
                          <label for="exampleInputFile">Profile Picture</label>
                          <input name="profilepict" type="file" id="exampleInputFile">
                          <p class="help-block">Choose your profile picture to upload</p>
                        </div>
                      <button name="updatepicture" value="updatepicture" type="submit" class="btn btn-success">Upload Picture</button>
                  </form>
              </div>    
          </div>
          <!-- /.row (nested) -->



      </div>
      <!-- /.panel-body -->
  </div>
  <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->




</div>
<!-- /#page-wrapper -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.0
  </div>
  <strong>Copyright &copy; 2018 <?=$this->config->item('footer')?></strong> All rights
  reserved.
</footer>