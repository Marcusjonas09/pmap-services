<div class="container-fluid ">

    <div class="row p-2 px-4" style="background-color:#2F308C;">
        <h4 class="col-md-2 my-2 text-light">Search Jobs</h4>
        <input type="text" class="form-control col-md-3 col-sm-12 my-2 mx-2 myfilter" name="JobTitleField" id="JobTitleField" placeholder="Keyword or Job Title">
        <input type="text" class="form-control col-md-3 col-sm-12 my-2 mx-2 myfilter" name="JobLocationField" id="JobLocationField" placeholder="Location">
        <button type="button" class="btn btn-dark col-md-1 col-sm-12 my-2 mx-2" data-toggle="modal" data-target="#filter"><span class="fa fa-sliders-h"></span> Filter</button>
        <button type="button" id="search_job" class="btn btn-primary col-md-1 col-sm-12 my-2 mx-2"><span class="fa fa-search"></span> Search</button>
    </div>
    <div class="row " style="height:48px; background-color:#CEAF3A;">
        <div class="col-md-12 d-flex">
            <p class="text-light ml-3 my-auto" id="show_results"></p>
        </div>
    </div>
    <div class="row" style="background-color:lightgray;">
        <div class="col-md-4">
            <div class="row">
                <div class="overlay col-md-12"></div>
                <div class="d-flex justify-content-center col-md-12">
                    <div class="loader" style="margin-top:40%;"></div>
                </div>
                <div id="jobs_table" class="col-md-12 px-2 py-0"></div>
            </div>
        </div>

        <div class="col toHide">
            <div class="row">
                <div class="overlay col-md-12"></div>
                <div class="d-flex justify-content-center col-md-12">
                    <div class="loader" style="margin-top:20%;"></div>
                </div>
                <div id="JobSinglePane" class="col-md-12 pl-0 pr-5 py-0"></div>
            </div>
        </div>
    </div>
    <div class="row" style="height:48px;">
        <div id="pagination_link" class="col-md-12 p-2 float-right" style="background-color:#CEAF3A;"></div>
    </div>
    <div onclick="topFunction()" id="myBtn" title="Go to top"><span class="fa fa-chevron-up"></span></div>
</div>

<div class="modal fade" id="view_job_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

</div>

<!-- Modal -->
<div class="modal fade" data-backdrop="static" id="filter" tabindex="-1" role="dialog" aria-labelledby="filter" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#2F308C;">
                <h5 class="modal-title text-light" id="filter">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group form-check">
                            <input id="isPreferred" type="checkbox" class="form-check-input" value="1">
                            <label class="form-check-label" for="isPreferred">Preferred</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-check">
                            <input id="isSpotlight" type="checkbox" class="form-check-input" value="1">
                            <label class="form-check-label" for="isSpotlight">Spotlight</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-check">
                            <input id="isMemberCompany" type="checkbox" class="form-check-input" value="1">
                            <label class="form-check-label" for="isMemberCompany">Member Company</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Job Function (multi-select)</label>
                            <select id="filter_job_function" class="form-control select2" multiple="multiple" style="width: 100%;">
                                <?php foreach ($JobFunctions as $JobFunction) : ?>
                                    <option value="<?= $JobFunction->pv_value ?>"><?= $JobFunction->pv_value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Industry (multi-select)</label>
                            <select id="filter_industry" class="form-control select2" multiple="multiple" style="width: 100%;">
                                <?php foreach ($Industries as $Industry) : ?>
                                    <option value="<?= $Industry->pv_value ?>"><?= $Industry->pv_value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <!-- <input type="text" value="" class="slider form-control" data-slider-min="-200" data-slider-max="200" data-slider-step="5" data-slider-value="[-100,100]" data-slider-orientation="horizontal" data-slider-selection="before" data-slider-tooltip="show" data-slider-id="blue"> -->

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="reset_filter" class="btn btn-primary">Reset filters</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>














<!-- 



<div class="row">
        <div class="col-md-12">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active py-3">
                        <div class="card bg-dark" style="width:100vw; height:300px;"></div>
                        <img src="..." class="d-block w-100" alt="...">
</div>
<div class="carousel-item py-3">
    <div class="bg-dark" style="width:100vw; height:300px;"></div>
    <img src="..." class="d-block w-100" alt="...">
</div>
<div class="carousel-item py-3">
    <div class="bg-dark" style="width:100vw; height:300px;"></div>
    <img src="..." class="d-block w-100" alt="...">
</div>
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>
</div>
</div> -->