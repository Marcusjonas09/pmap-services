<div id="wallpaper"></div>
<div class="container-fluid">
    <div class="row align-items-center justify-content-center">

        <!-- <div class="col-md-6">
            <div class="card shadow-lg">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <h4 class="card-title">Title</h4>
                    <p class="card-text">Text</p>
                </div>
            </div>
        </div> -->
        <div class="col-md-6 p-5">
            <div class="card shadow-lg">
                <div class="card-body ">
                    <h2 class="text-center"><strong>Login</strong></h2>

                    <?php if ($this->session->flashdata('services_access_fail')) : ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <?= $this->session->flashdata('services_access_fail'); ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?= base_url() ?>Profiling/login" method="POST">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="email"><strong>Email Address</strong></label>
                                <input value="<?= set_value('email') ?>" type="text" name="email" id="email" class="form-control <?= (form_error('email') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="example@yourmail.com">
                            </div>

                            <div class="form-group col-md-12">
                                <label for="password"><strong>Password</strong></label>
                                <input value="<?= set_value('password') ?>" type="password" name="password" id="password" class="form-control <?= (form_error('password') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Password">
                            </div>
                            <?php $this->session->set_userdata('current_uri', $this->uri->segment(1)); ?>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <strong>
                                    No Account? <a href="http://pmap.site/" target="_blank">Register here.</a>
                                </strong>
                            </div>

                            <div class="col-md-6"><button type="submit" id="company_login" class="btn float-right btn-primary text-light">Login</button></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-12" style="height:48px; background-color:#CEAF3A;"></div> -->
    </div>

    <!-- <div onclick="topFunction()" id="myBtn" title="Go to top"><span class="fa fa-chevron-up"></span></div> -->
</div>