<div id="wallpaper"></div>
<div class="container-fluid">

    <div class="row d-flex justify-content-center">
        <div class="col-md-10">
            <div class="card my-5 shadow-lg">
                <div class="card-body">
                    <h2><strong>Vendor Services Request</strong></h2>

                    <div class="row mb-4">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5><?= $package->package_name ?> Plan Inclusions</h5>
                                    <ul>
                                        <li><?= $package->post_limit_daily . ' posts/day' ?></li>
                                        <li><?= $package->post_limit_monthly . ' posts/month' ?></li>
                                        <li><?= $package->post_duration . ' day post validity' ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($this->session->logged_in) : ?>
                        <form action="<?= base_url() ?>submit_vendor_request" method="post">
                            <input type="hidden" name="package_id" value="<?= $package->package_id ?>">
                            <input type="hidden" name="package_type" value="<?= $package->package_type ?>">
                            <input type="hidden" name="company_id" value="<?= $this->session->services_user['info']['company']['id'] ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Company Details</strong></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="CompanyName">Company Name</label>

                                    <input readonly value="<?= $this->session->services_user['info']['company']['name'] ?>" type="text" name="CompanyName" id="CompanyName" class="form-control <?= (form_error('CompanyName') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Company Name">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="CompanyTelNo">Contact Number</label>
                                    <input readonly value="<?= $this->session->services_user['info']['company']['contact'] ?>" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control <?= (form_error('CompanyTelNo') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Telephone No">
                                </div>

                                <!-- <div class="form-group col-md-3">
                                    <label for="CompanyCelNo">Mobile No.</label>
                                    <input value="<?= set_value('CompanyCelNo') ?>" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control <?= (form_error('CompanyCelNo') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Mobile No">
                                </div> -->

                                <div class="form-group col-md-4">
                                    <label for="CompanyEmail">Company Email</label>
                                    <input readonly value="<?= $this->session->services_user['info']['company']['email'] ?>" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control <?= (form_error('CompanyEmail') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="example@example.com">
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="CompanyAddress">Company Address</label>
                                    <input readonly value="<?= $this->session->services_user['info']['company']['address'] ?>" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control <?= (form_error('CompanyAddress') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Company Address">
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4><strong>Contact Person</strong></h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="ContactFullname">Full Name</label>
                                    <input readonly value="<?= $this->session->services_user['name'] ?>" type="text" name="ContactFullname" id="ContactFullname" placeholder="Juan Dela Cruz" class="form-control <?= (form_error('ContactFullname') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>">
                                </div>

                                <!-- <div class="form-group col-md-6">
                                    <label for="ContactPosition">Position in the company</label>
                                    <input value="<?= set_value('ContactPosition') ?>" type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control <?= (form_error('ContactPosition') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>">
                                </div> -->

                                <div class="form-group col-md-4">
                                    <label for="ContactNo">Contact Number.</label>
                                    <input readonly value="<?= $this->session->services_user['info']['contact_number'] ?>" type="text" name="ContactNo" id="ContactNo" placeholder="Mobile/Landline" class="form-control <?= (form_error('ContactNo') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="ContactEmail">Email</label>
                                    <input readonly value="<?= $this->session->services_user['email'] ?>" type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control <?= (form_error('ContactEmail') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>">
                                </div>

                            </div>
                            <button type="submit" class="btn btn-lg float-right primarybtn text-light">Submit</button>
                        </form>
                    <?php else : ?>
                        <h4>Please login with your PMAP account to continue</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="height:48px; background-color:#CEAF3A;">
    </div>
    <div onclick="topFunction()" id="myBtn" title="Go to top"><span class="fa fa-chevron-up"></span></div>
</div>