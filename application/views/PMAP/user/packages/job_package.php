<div id="wallpaper"></div>
<div class="container-fluid">

    <div class="row d-flex justify-content-center">

        <?php
        if (!empty($contents)) {
            foreach ($contents as $content) {
                $data['content'] = $content;
                if ($content->page_assignment == "Job Pricing Page" && $content->is_published == 1) {
                    $this->load->view('content_layouts/' . $content->content_layout, $data);
                }
            }
        }
        ?>

        <div class="col-md-10 my-5">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card shadow-lg">
                        <div class="card-header bg-white">
                            <h4 class="font-weight-bold">Pricing</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php foreach ($job_packages as $job_package) : ?>
                                    <div class="col-md-4">
                                        <div class="card shadow-sm my-2">
                                            <div class="card-header">
                                                <h4 class="font-weight-bold"><?= $job_package->package_name ?></h4>
                                            </div>
                                            <div class="card-body">
                                                <h5>Inclusions</h5>
                                                <ul>
                                                    <li><?= $job_package->post_limit_daily . ' posts/day' ?></li>
                                                    <li><?= $job_package->post_limit_monthly . ' posts/month' ?></li>
                                                    <li><?= $job_package->post_duration . ' day post validity' ?></li>
                                                </ul>
                                            </div>
                                            <form action="<?= base_url() ?>job_request" method="POST">
                                                <input type="hidden" name="package_id" value="<?= $job_package->package_id ?>">
                                                <input type="hidden" name="package_type" value="<?= $job_package->package_type ?>">
                                                <div class="card-footer text-center">
                                                    <h4 class="font-weight-bold">Price: <?= "&#8369;" . $job_package->package_cost ?></h4>
                                                    <button type="button" class="<?= ($_SESSION['logged_in']) ? 'need-login' : '' ?> btn btn-primary col-md-12">Get Started</button>
                                                    <!-- <a href="<?= base_url() ?>job_request?id=<?= $job_package->package_id ?>&type=<?= $job_package->package_type ?>" class="btn btn-primary col-md-12">Get Started</a> -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row" style="height:48px; background-color:#CEAF3A;">
    </div>
    <div onclick="topFunction()" id="myBtn" title="Go to top"><span class="fa fa-chevron-up"></span></div>
</div>