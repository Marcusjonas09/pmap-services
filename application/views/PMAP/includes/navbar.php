<nav class="navbar navbar-expand-lg bg-white navbar-light shadow-lg py-0">
    <a class="navbar-brand" href="<?= base_url() ?>welcome"><img style="width:48px;" src="<?= base_url() ?>assets/img/jobs/pmap-logo-portrait.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav inline">
            <li class="nav-item <?= ($this->uri->segment(1) == 'jobs' || $this->uri->segment(1) == '') ? 'active' : '' ?>">
                <a class="nav-link " href="<?= base_url() ?>jobs">
                    <h5>Jobs</h5><span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item <?= ($this->uri->segment(1) == 'vendors') ? 'active' : '' ?>">
                <a class="nav-link" href="<?= base_url() ?>vendors">
                    <h5>Vendors</h5>
                </a>
            </li>

            <li class="nav-item <?= ($this->uri->segment(1) == 'pmap') ? 'active' : '' ?>">
                <a class="nav-link" href="http://pmap.org.ph" target="_blank">
                    <h5>PMAP</h5>
                </a>
            </li>


        </ul>


        <ul class="navbar-nav inline ml-auto">

            <?php if ($this->session->logged_in) : ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle btn btn-sm secondarybtn mr-2 text-light font-weight-bold my-2" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="fa fa-user mx-2"></span>
                        <span class="nav-link-inner--text mr-2"><?= $this->session->services_user['name'] ?></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?= base_url() ?>home">Manage Services</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url() ?>Admin/logout"><span class="fa fa-sign-out-alt mr-2"></span>Log out</a>
                    </div>
                </li>

            <?php else : ?>
                <li class="nav-item ">
                    <a data-toggle="modal" data-target="#login_mdl" class="shadow nav-link btn btn-sm secondarybtn mr-2 text-light font-weight-bold my-2" href="<?= base_url() ?>login">
                        <span class="fa fa-sign-in-alt mx-2"></span>
                        <span class="nav-link-inner--text mr-2">LOGIN</span>
                    </a>
                </li>
            <?php endif; ?>

            <!-- <?php if ($this->uri->segment(1) == 'jobs') : ?>
                <li class="nav-item ">
                    <a class="nav-link btn btn-sm primarybtn text-light font-weight-bold my-2" href="<?= base_url() ?>job_pricing">
                        <span class="fa fa-briefcase mx-2"></span>
                        <span class="nav-link-inner--text mr-2">POST A JOB</span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if ($this->uri->segment(1) == 'vendors') : ?>
                <li class="nav-item">
                    <a class="nav-link btn btn-sm primarybtn text-light font-weight-bold my-2" href="<?= base_url() ?>vendor_pricing">
                        <span class="fa fa-list mx-2"></span>
                        <span class="nav-link-inner--text mr-2">POST A SERVICE</span>
                    </a>
                </li>
            <?php endif; ?> -->
        </ul>


        <!-- 
        <ul class="navbar-nav ml-auto">


        </ul> -->

        <!-- 
        <?php if ($this->uri->segment(1) == 'welcome') : ?>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link btn btn-sm text-light font-weight-bold my-2" href="<?= base_url() ?>welcome" style="background-color:#CEAF3A;">
                        <span class="fa fa-user mx-2"></span>
                        <span class="nav-link-inner--text mr-2">JOIN US</span>
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link btn btn-sm primarybtn text-light font-weight-bold my-2" href="<?= base_url() ?>login">
                        <span class="fa fa-sign-in-alt mx-2"></span>
                        <span class="nav-link-inner--text mr-2">LOGIN</span>
                    </a>
                </li>
            </ul>

        <?php endif; ?> -->

    </div>
</nav>