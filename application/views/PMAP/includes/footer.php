<!-- <footer style=""> -->
<div class="container-fluid">
  <div class="row py-5 d-flex justify-content-center" style="background-color:#2F308C;">
    <div class="col-md-10">
      <h5 class="text-light">People Management Association of the Philippines.
        <br />Copyright 2019
      </h5>
      <h6 class="text-light"> PMAP Center, 670 Lee Street Addition Hills, Mandaluyong City<br />
        Tel : +63 (2) 8726-1532, Email : pmap@pmap.org.ph</h6>
    </div>
  </div>
</div>
<!-- </footer> -->
</div>

<!-- Modal -->
<div class="modal fade" id="login_mdl" tabindex="-1" role="dialog" aria-labelledby="login_modal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img width="100px" src="<?= base_url() ?>assets/img/jobs/pmap-logo.PNG" alt="">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <?php if ($this->session->flashdata('services_access_fail')) : ?>
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?= $this->session->flashdata('services_access_fail'); ?>
          </div>
        <?php endif; ?>
        <form action="<?= base_url() ?>Profiling/login" method="POST">
          <div class="row">
            <div class="form-group col-md-12">
              <label for="email"><strong>Email Address</strong></label>
              <input value="<?= set_value('email') ?>" type="text" name="email" id="email" class="form-control <?= (form_error('email') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="example@yourmail.com">
            </div>

            <div class="form-group col-md-12">
              <label for="password"><strong>Password</strong></label>
              <input value="<?= set_value('password') ?>" type="password" name="password" id="password" class="form-control <?= (form_error('password') ? 'is-invalid' : ((isset($_POST['submit'])) ? 'is-valid' : '')); ?>" placeholder="Password">
            </div>

            <input type="hidden" name="url" value="<?= $this->uri->uri_string() ?>">
          </div>
          <div class="row align-items-center">
            <!-- <div class="col-md-6">
              <strong>
                No Account? <a href="http://pmap.site/" target="_blank">Register here.</a>
              </strong>
            </div> -->

            <div class="col-md-6">
              <button type="submit" class="col-md-12 btn float-right primarybtn text-light shadow"><strong>Login</strong>
              </button>
            </div>

            <div class="col-md-6">
              <a href="http://pmap.site/" class="col-md-12 btn float-right btn-default shadow"><strong>Register</strong>
              </a>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>
</div>
</body>

</html>