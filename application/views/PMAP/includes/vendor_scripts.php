<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="<?= base_url() ?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="<?= base_url() ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script> -->

<script>
    var baselink = "<?= base_url() ?>";

    $('.carousel').carousel()

    var myVar;

    function loadingscreen() {
        myVar = setTimeout(showPage, 500);
    }

    function showPage() {
        $('.loader,.overlay').fadeOut(100).delay(150);
        $('#my_content').fadeIn(100);
    }

    function load_vendors_data(page) {
        var CompanyName = "";
        var ServiceOffered = "";

        company_name = $('#VendorNameField').val().trim();
        service_offered = $('#ServiceTitleField').val().trim();

        CompanyName = (company_name != "" ? company_name : "_");
        ServiceOffered = (service_offered != "" ? service_offered : "_");
        $.ajax({
            url: "<?php echo base_url() ?>Services/pagination_vendor/" + page,
            method: "POST",
            dataType: "json",
            data: {
                CompanyName: CompanyName,
                ServiceOffered: ServiceOffered,
            },
            success: function(data) {
                $('#vendors_table').html(data.vendors_table);
                $('#pagination_link').html(data.pagination_link);
                $('#show_results').html("<strong>showing ( " + data.results + " ) result/s</strong>");
            }
        });
    }

    function view_vendor_single(ServiceId) {
        $.ajax({
            url: baselink + "Services/getVendorSingle/" + ServiceId,
            method: "GET",
            dataType: "json",
            success: function(data) {
                $('#VendorSinglePane').html(data.VendorDetails);
                if (window.innerWidth < 768) {
                    $('#view_services_modal').html(data.VendorDetails);
                    $('#view_services_modal').modal('show');
                } else {
                    $('#close_view').hide();
                    $('#web_modal_header').hide();
                }
            }
        });
    }

    // fetch latest entry

    $.ajax({
        url: baselink + "Services/getVendorRecent/",
        method: "GET",
        dataType: "json",
        success: function(data) {
            console.log(data);
            if (window.innerWidth > 768 && data) {
                view_vendor_single(data.VendorDetails.ServiceId);
            } else {
                view_vendor_single(false);
            }
        }
    });



    $(document).ready(function() {

        $('.myfilter').keyup(function() {
            load_vendors_data(1);
        });

        load_vendors_data(1);

        $(document).on("click", ".pagination li a", function(event) {
            event.preventDefault();
            var page = $(this).data("ci-pagination-page");
            load_vendors_data(page);
            topFunction();
        });
    });

    ////////////////////////////
    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $('#myBtn').fadeIn(100);
        } else {
            $('#myBtn').fadeOut(100);
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    /////////////////////
</script>

<!--     

    <script>
        var baselink = "<?= base_url() ?>";

        var myVar;

        function loadingscreen() {
            myVar = setTimeout(showPage, 500);
        }

        function showPage() {
            $('.loader,.overlay').fadeOut(800).delay(500);
            $('#my_content').fadeIn(800);
        }

        function load_job_data(page) {
            var jobtitle = "";
            var joblocation = "";

            job_title = $('#JobTitleField').val().trim();
            job_location = $('#JobLocationField').val().trim();

            jobtitle = (job_title != "" ? job_title : "_");
            joblocation = (job_location != "" ? job_location : "_");

            isPreferred = $('#isPreferred').is(":checked");
            isSpotlight = $('#isSpotlight').is(":checked");
            isMemberCompany = $('#isMemberCompany').is(":checked");

            $.ajax({
                url: "<?php echo base_url(); ?>Services/pagination/" + page,
                method: 'POST',
                dataType: "json",
                data: {
                    jobtitle: jobtitle,
                    joblocation: joblocation,
                    isSpotlight: isSpotlight,
                    isPreferred: isPreferred,
                    isMemberCompany: isMemberCompany
                },
                success: function(data) {
                    $('#jobs_table').html(data.jobs_table);
                    $('#pagination_link').html(data.pagination_link);
                    $('#show_results').html("<strong>" + data.results + " Job Post/s</strong>");
                }
            });
        }

        function view_job_single(JobId) {
            $.ajax({
                url: baselink + "Services/getJobSingle/" + JobId,
                method: "GET",
                dataType: "json",
                success: function(data) {
                    $('#JobSinglePane').html(data.JobDetails);

                    if (window.innerWidth < 768) {
                        $('#view_job_modal').html(data.JobDetails);
                        $('#view_job_modal').modal('show');
                    } else {
                        $('#close_view').hide();
                        $('#web_modal_header').hide();
                        $('#jobInfoLeft').hide();
                    }
                }
            });
        }

        $(document).ready(function() {

            $("#reset_filter").click(function() {
                $(".form-check-input").prop("checked", false);
                $(".select2").select2().val('')
                $(".select2").select2().val('');
                load_job_data(1);
            });

            load_job_data(1);

            $('.myfilter').change(function() {
                load_job_data(1);
            });

            $(document).on("click", ".pagination li a", function(event) {
                event.preventDefault();
                var page = $(this).data("ci-pagination-page");
                load_job_data(page);
            });

            $.ajax({
                url: baselink + "Services/getJobRecent/",
                method: "POST",
                dataType: "json",
                success: function(data) {
                    if (window.innerWidth > 768) {
                        view_job_single(data.JobId.JobId);
                    }
                }
            });

            $('#search_job').click(function() {
                load_job_data(1);
            });

        });
        $(document).on('keypress', function(e) {
            if (e.which == 13) {
                load_job_data(1);
            }
        });

        $('.select2').select2();



        ////////////////////////////
        //Get the button
        var mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                $('#myBtn').fadeIn(100);
            } else {
                $('#myBtn').fadeOut(100);
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
        /////////////////////
    </script> -->