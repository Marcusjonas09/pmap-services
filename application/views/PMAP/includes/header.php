<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bower_components/select2/dist/css/select2.min.css">
    <script defer src="<?= base_url() ?>assets/fontawesome/js/all.min.js"></script>

    <title>PMAP - Jobs and Vendors</title>
    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 10px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #666;
            border-radius: 5px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #444;
        }

        @media (max-width: 768px) {

            .toHide,
            #JobInfoRight {
                display: none !important;
            }

            .modal-full,
            .modal-content {
                min-width: 100%;
                min-height: 100vh;
            }

            #jobdesc {
                margin-top: 30px;
            }
        }

        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            border: none;
            outline: none;
            background-color: #CEAF3A;
            color: white;
            cursor: pointer;
            padding-top: 13px;
            padding-left: 17px;
            width: 50px;
            height: 50px;
            border-radius: 25px;
        }

        html {
            scroll-behavior: smooth;
        }

        .loader {
            position: absolute;
            z-index: 2;
            border: 10px solid rgba(0, 0, 0, 0.3);
            border-radius: 50%;
            border-top: 10px solid #CEAF3A;
            width: 5rem;
            height: 5rem;
            background-color: rgba(0, 0, 0, 0);
            -webkit-animation: spin 1s linear infinite;
            animation: spin 1s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .overlay {
            background: none repeat scroll 0 0 black;
            opacity: 0.5;
            position: absolute;
            display: block;
            z-index: 1;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
        }

        .row:before,
        .row:after {
            display: none !important;
        }

        .primarybtn {
            background-color: #2F308C;
        }

        .secondarybtn {
            background-color: #CEAF3A;
        }

        #wallpaper {
            position: absolute;
            width: 100%;
            height: 200px;
            clip-path: polygon(0 0, 100% 0, 100% 20%, 0 100%);
            background-color: #2F308C;
            z-index: 0;
        }

        .scrolling-wrapper {
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;
        }

        .mycard {
            display: inline-block;
        }

        .scrolling-wrapper {
            -webkit-overflow-scrolling: touch;
        }
    </style>
</head>

<body onload="loadingscreen()" style="overflow-x:hidden;">
    <div class="container-fluid p-0" id="my_content">