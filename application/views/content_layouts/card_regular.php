<div class="col-md-10">
    <div class="card my-5 shadow-lg">
        <div class="card-body ">
            <h5 class="card-title">
                <h1><?= $content->content_title ?></h1>
            </h5>
            <div>
                <?= $content->content_description ?>
            </div>
        </div>
    </div>
</div>