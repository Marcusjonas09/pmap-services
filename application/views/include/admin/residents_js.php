<!-- DataTables -->
<script src="<?=base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script>
    function updateStatus(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to update the status",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      location.href="<?=base_url()?>admin/update_resident_status/"+id; 
                         
                    } else {
                        swal("Status update has been canceled!");
            }

        });
    }

    function updateAdminStatus(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to update the status",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      location.href="<?=base_url()?>admin/update_admin_status/"+id; 
                         
                    } else {
                        swal("Status update has been canceled!");
            }

        });
    }

    function deleteAdmin(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the admin",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      location.href="<?=base_url()?>admin/delete_admin/"+id; 
                         
                    }

        });
    }
</script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>


