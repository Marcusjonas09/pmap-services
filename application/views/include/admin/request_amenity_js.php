<!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?=base_url()?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>

<script>
    $(function(){
        $('#right').tooltip();
        $(".tooltip_approved").tooltip({
            placement: "top",
            title: "Approve Request"
        });
        $(".tooltip_payment").tooltip({
            placement: "top",
            title: "Approve for Payment"
        });
        $(".tooltip_view").tooltip({
            placement: "top",
            title: "View Request"
        });
        $(".tooltip_delete").tooltip({
            placement: "top",
            title: "Delete Request"
        });
        $(".tooltip_setfree").tooltip({
            placement: "top",
            title: "Approve as Free Request"
        });
        $(".tooltip_disapprove").tooltip({
            placement: "top",
            title: "Disapprove Request"
        });
        $(".tooltip_disapprove_complaint").tooltip({
            placement: "top",
            title: "Disapprove Request"
        });
    });


    $('.btn-modal').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('admin/modal_request');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-cert').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('admin/modal_cert');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-faq').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('user/modal_faq');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-complaint').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('user/modal_complaint');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-member').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('admin/modal_member');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-payment').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('admin/modal_request_payment');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });


    function deleteEvent(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the schedule",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_request/"+id; 
                    } else {
                        swal("Status update has been canceled!");
            }

        });
    }

    function updateFee(id, status, uid) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to update the payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_fee/"+id+"/"+status+"/"+uid; 
                    }

        });
    }

    function deleteHistory(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "Deleting history",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>user/delete_history/"+id; 
                    } 

        });
    }

    function deleteEventInfo(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the Event",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_event_info/"+id; 
                    }

        });
    }

    function deleteMember(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the Member",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_member/"+id; 
                    }

        });
    }

    function deleteCMS(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the CMS",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_cms/"+id; 
                    }

        });
    }

    function deleteAmenity(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the Amenity",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_amenity/"+id; 
                    }

        });
    }


    function deleteComplaint(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the Complaint",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>user/delete_complaint/"+id; 
                    }

        });
    }

    function deleteFAQ(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the FAQ",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_faq/"+id; 
                    }

        });
    }


    function approveForPayment(id) {
        swal({
            title: "Update for Payment",
            text: "You want to update the request for payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_for_payment/"+id; 
                    } 

        });
    }

    function approveRequest(id, rid) {
        swal({
            title: "Approve Request",
            text: "You want to approve the request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_approve_request/"+id+"/"+rid; 
                    } 

        });
    }

    function approveCert(id) {
        swal({
            title: "Approve Cerification Request",
            text: "You want to approve the certification request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_approve_cert/"+id; 
                    } 

        });
    }

    function approveFreeRequest(id) {
        swal({
            title: "Approve as Free Request",
            text: "You want to approve the request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_approve_free_request/"+id; 
                    } 

        });
    }

    function disapprove(id) {
        swal({
            title: "Disapprove Request",
            text: "You want to disapprove the request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_disapprove_request/"+id; 
                    } 

        });
    }

    function deleteCert(id) {
        swal({
            title: "Delete Certification Request",
            text: "You want to delete the certification request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_cert/"+id; 
                    } 

        });
    }

    function disapprovedComplaint(id) {
        swal({
            title: "Disapprove Request",
            text: "You want to disapprove the request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_disapproved_complaint/"+id; 
                    } 

        });
    }

    function setAmount(id) {
      swal({
            title: "Update for Payment",
            text: "You want to update the request for payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                       swal("Enter the amount to be paid", {
                           content: {
                                element: "input",
                                attributes: {
                                  type: 'number',  
                                  placeholder: 'Enter the amount to be paid',  
                                },
                              },
                        })
                        .then((value) => {
                            location.href="<?=base_url()?>admin/update_for_payment/"+id+"/"+value; 
                            //location.href="<?=base_url()?>admin/update_fee_parking/"+id+"/"+value+"/"+uid; 
                        });
                      
                    }
        });

}

    function approveRequestOk(id) {
            swal({
                title: "Approve Request",
                text: "You want to approve the request for payment",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                          //alert(id);
                          location.href="<?=base_url()?>admin/update_for_payment/"+id+"/"; 
                        } 

            });
        }

    function setCertAmount(id) {
      swal({
            title: "Update for Payment",
            text: "You want to update the certification request payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                       swal("Enter the amount to be paid", {
                           content: {
                                element: "input",
                                attributes: {
                                  type: 'number',  
                                  placeholder: 'Enter the amount to be paid',  
                                },
                              },
                        })
                        .then((value) => {
                            location.href="<?=base_url()?>admin/update_cert_for_payment/"+id+"/"+value; 
                            //location.href="<?=base_url()?>admin/update_fee_parking/"+id+"/"+value+"/"+uid; 
                        });
                      
                    }
        });

}
    
</script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

<script>
  $(function () {
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
    $('.textarea').wysihtml5()

  })
</script>
