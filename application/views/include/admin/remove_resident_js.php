<script src="<?=base_url()?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>
<script>
    function deleteItem(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      location.href="<?=base_url()?>admin/remove_resident/"+id; 
                         
                    } else {
                        swal("The record is safe!");
            }

        });
    }
</script>