<?php
$job_parameters = array('JobParam', 'add_job_parameter', 'edit_job_parameter', 'add_job_values');
$vendor_parameters = array('');
$package_parameters = array('packages', 'create_package', 'edit_package');
$jobs = array('jobs', 'add_job', 'edit_job');
$vendors = array('vendors', 'add_service', 'edit_service');
$companies = array('companies', 'add_company');
$subscribers = array('subscribers', 'add_subscriber');
$cms = array('contents', 'add_content', 'view_content', 'edit_content');

$parameters = array('JobParam', 'add_job_parameter', 'edit_job_parameter', 'add_job_values', 'packages', 'create_package', 'edit_package');

?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url() ?>uploads_thumb/<?= $this->session->image ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= $this->session->name ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?= $this->uri->segment(2) == 'home' ? 'active' : '' ?>">
        <a href="<?= base_url() ?>Admin/home">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="<?= $this->uri->segment(2) == 'admins' ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/admins">
            <i class="fa fa-users"></i>
            <span>User Admin</span>

          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="<?= (in_array($this->uri->segment(2), $companies)) ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/companies">
            <!-- <a href="<?= base_url() ?>Admin/maintenance"> -->
            <i class="fa fa-building"></i>
            <!-- <i class="fa fa-circle-o text-yellow"></i> -->
            <span>Company Profile</span>

          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="<?= (in_array($this->uri->segment(2), $subscribers)) ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/subscribers">
            <!-- <a href="<?= base_url() ?>Admin/maintenance"> -->
            <i class="fa fa-folder"></i>
            <!-- <i class="fa fa-circle-o text-yellow"></i> -->
            <span>Subscription Requests</span>

          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="treeview <?= (in_array($this->uri->segment(2), $parameters)) ? 'active' : '' ?>">
          <a href="#">
            <i class="fa fa-gears"></i> <span>Parameters</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li style="padding-left:10px;" class="<?= (in_array($this->uri->segment(2), $job_parameters)) ? 'active' : '' ?>">
              <a href="<?= base_url() ?>Admin/JobParam">
                <i class="fa fa-circle-o"></i>
                <span>Job Parameters</span>
              </a>
            </li>
            <li style="padding-left:10px;" class="<?= (in_array($this->uri->segment(2), $vendor_parameters)) ? 'active' : '' ?>">
              <!-- <a href="<?= base_url() ?>Admin/parameters"> -->
              <a href="<?= base_url() ?>Admin/maintenance">
                <!-- <i class="fa fa-circle-o"></i> -->
                <i class="fa fa-circle-o text-yellow"></i>
                <span>Vendor Parameters</span>
              </a>
            </li>
            <li style="padding-left:10px;" class="<?= (in_array($this->uri->segment(2), $package_parameters)) ? 'active' : '' ?>">
              <a href="<?= base_url() ?>Admin/packages">
                <i class="fa fa-circle-o"></i>
                <span>Package Parameters</span>
              </a>
            </li>
          </ul>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin' || $this->session->job_access) : ?>
        <li class="<?= (in_array($this->uri->segment(2), $jobs)) ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/jobs">
            <i class="fa fa-briefcase"></i>
            <span>Jobs</span>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin' || $this->session->vendor_access) : ?>
        <li class="<?= (in_array($this->uri->segment(2), $vendors)) ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/vendors">
            <!-- <a href="<?= base_url() ?>Admin/maintenance"> -->
            <i class="fa fa-building-o"></i>
            <!-- <i class="fa fa-circle-o text-yellow"></i> -->
            <span>Vendors</span>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tv"></i> <span>CMS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?= base_url() ?>admin/contents">
                <i class="fa fa-circle-o"></i>
                <span>Site CMS</span>
              </a>
            </li>
            <!-- <li>
              <a href="<?= base_url() ?>admin/faqs">
                <i class="fa fa-circle-o"></i>
                <span>FAQS</span>
              </a>
            </li> -->
          </ul>
        </li>

      <?php endif; ?>

      <?php if ($this->session->role == 'admin') : ?>
        <li class="<?= $this->uri->segment(2) == 'trails' ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/trails">
            <i class="fa fa-book"></i>
            <span>Audit Trail</span>
          </a>
        </li>

        <li class="<?= $this->uri->segment(2) == 'reports' ? 'active' : '' ?>">
          <a href="<?= base_url() ?>Admin/reports">
            <i class="fa fa-book"></i>
            <span>Reports</span>

          </a>
        </li>
      <?php endif; ?>




      <li class="header">LABELS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li style="padding-bottom:100%"><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>

      <!-- <li>
        <a href="<?= base_url() ?>admin/residents">
          <i class="fa fa-users"></i>
          <span>Residents</span>
          <small class="label pull-right bg-blue"><?= $count_all_residents ?></small>
          <small class="label pull-right bg-green"><?= $count_all_residents_active ?></small>
          <small class="label pull-right bg-red"><?= $count_all_residents_inactive ?></small>
        </a>
      </li> -->
      <!-- 
      <?php if ($this->session->role == 'admin') : ?>
        <li>
          <a href="<?= base_url() ?>admin/amenities">
            <i class="fa fa-star"></i>
            <span>Amenities</span>
            <small class="label pull-right bg-blue"><?= $count_all_amenities ?></small>
          </a>
        </li>
      <?php endif; ?>

      <li>
        <a href="<?= base_url() ?>admin/request">
          <i class="fa fa-users"></i>
          <span>Request</span>
          <small class="label pull-right bg-blue"><?= $count_all_requested ?></small>
          <small class="label pull-right bg-orange"><?= $count_all_request ?></small>
          <small class="label pull-right bg-red"><?= $count_all_request_disapprove ?></small>
          <small class="label pull-right bg-green"><?= $count_all_request_approve ?></small>
        </a>
      </li>

      <li>
        <a href="<?= base_url() ?>admin/certifications">
          <i class="fa fa-users"></i>
          <span>Certification</span>
        </a>
      </li>

      <?php if ($this->session->role == 'admin' || $this->session->role == 'officer') : ?>
        <li>
          <a href="<?= base_url() ?>admin/complaints">
            <i class="fa fa-bullhorn"></i>
            <span>Complaint</span>
            <small class="label pull-right bg-green">0</small>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($this->session->role != 'officer') : ?>
        <li>
          <a href="<?= base_url() ?>admin/fees">
            <i class="fa fa-money"></i>
            <span>Regular Fees</span>
          </a>
        </li>
        <li>
          <a href="<?= base_url() ?>admin/payment">
            <i class="fa fa-money"></i>
            <span>Payments</span>
          </a>
        </li>
      <?php endif; ?>



      <li>
        <a href="<?= base_url() ?>admin/message">
          <i class="fa fa-tags"></i>
          <span>Message</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-blue"><?= $count_all ?></small>
            <small class="label pull-right bg-green"><?= $count_all_active ?></small>

          </span>
        </a>
      </li> -->
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>