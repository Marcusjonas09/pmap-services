<header class="main-header">

  <!-- Logo -->
  <a href="#" class="logo" style="background-color: #2F308C;">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>PS</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>PMAP</b> Services</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" style="background-color:#2F308C;" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <?php if ($this->session->role == 'normal') : ?>
      <ul class="nav navbar-nav inline">
        <li class="pull-left">
          <a href="#"><strong><?= $this->session->services_user['company']['name'] ?></strong></a>
        </li>
      </ul>
    <?php endif; ?>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">


        <!-- <?php if ($this->session->role == 'admin') : ?>


          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success"><?= $count_all_active ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?= $count_all_active ?> messages</li>
              <li>

                <ul class="menu">
                  <?php foreach ($messages as $r) : ?>
                    <li>

                      <a href="#">
                        <div class="pull-left">
                          <img src="<?= base_url() ?>assets/img/profile.png" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          <?= $r->message_from_name ?>
                          <small><i class="fa fa-clock-o"></i>
                            <?php
                            $post_date = strtotime($r->created_at);
                            $now = time();
                            $units = 2;
                            echo timespan($post_date, $now, $units);
                            ?>
                          </small>
                        </h4>
                        <p><?= character_limiter($r->message_body, 30); ?></p>
                      </a>
                    </li>
                  <?php endforeach; ?>

                </ul>
              </li>
              <li class="footer"><a href="<?= base_url() ?>admin/message">See All Messages</a></li>
            </ul>
          </li>





          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <?php $notif = ($count_all_new_complaints + $count_all_new_request + $count_all_new_members); ?>
              <span class="label label-warning"><?= $notif ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?= $notif ?> notifications</li>
              <li>

                <ul class="menu">
                  <?php if ($count_all_new_complaints > 0) : ?>
                    <li>


                      <a href="<?= base_url() ?>admin/complaints">
                        New Complaints
                        <small class="pull-right">
                          <?= $count_all_new_complaints ?>
                        </small>
                      </a>
                      </a>
                    </li>
                  <?php endif; ?>

                  <?php if ($count_all_new_request > 0) : ?>
                    <li>


                      <a href="<?= base_url() ?>admin/request">
                        New Request
                        <small class="pull-right">
                          <?= $count_all_new_request ?>
                        </small>
                      </a>
                      </a>
                    </li>
                  <?php endif; ?>

                  <?php if ($count_all_new_members > 0) : ?>
                    <li>


                      <a href="<?= base_url() ?>admin/residents">
                        New Members
                        <small class="pull-right">
                          <?= $count_all_new_members ?>
                        </small>
                      </a>
                      </a>
                    </li>
                  <?php endif; ?>

                </ul>
              </li>

            </ul>
          </li>

          <?php $total = $count_all_request + $count_all_request_payment ?>
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger"><?= $total ?></span>
            </a>
            <ul class="dropdown-menu">

              <li class="header">You have <?= ($count_all_request + $count_all_request_payment) ?> tasks</li>
              <li>

                <ul class="menu">
                  <?php if ($count_all_request > 0) : ?>
                    <li>

                      <a href="#">
                        <h3>
                          Pending
                          <small class="pull-right">
                            <?= sprintf("%.0f", ($count_all_request / $total) * 100) . '%'; ?>
                          </small>
                        </h3>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">80% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                  <?php endif; ?>


                  <?php if ($count_all_request_payment > 0) : ?>
                    <li>

                      <a href="#">
                        <h3>
                          For Payment
                          <small class="pull-right">
                            <?= sprintf("%.0f", ($count_all_request_payment / $total) * 100) . '%'; ?>
                          </small>
                        </h3>
                        <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-blue" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">80% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                  <?php endif; ?>


                </ul>
              </li>
              <li class="footer">
                <a href="<?= base_url() ?>admin/request">View all tasks</a>
              </li>
            </ul>
          </li>


        <?php endif; ?> -->


        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= base_url() ?>uploads_thumb/<?= $this->session->image ?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?= $this->session->name; ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?= base_url() ?>uploads_thumb/<?= $this->session->image ?>" class="img-circle" alt="User Image">

              <p>
                <?= $this->session->name ?>
              </p>
            </li>

            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="<?= base_url() ?>admin/logout" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>