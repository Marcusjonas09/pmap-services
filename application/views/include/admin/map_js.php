
<!-- DataTables -->
<script src="<?=base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?=base_url()?>assets/bower_components/sweet-alert/sweetalert.min.js"></script>


<script src="<?=base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
  $(function () {
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
  })
</script>

<script>

    // $(".swa-confirm").submit(function (e) {
    //     e.preventDefault();
    //     swal({
    //         title: $(this).data("swa-title"),
    //         text: $(this).data("swa-text"),
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#cc3f44",
    //         confirmButtonText: $(this).data("swa-btn-txt"),
    //         closeOnConfirm: false,
    //         html: false
    //     }, function(){
    //         $(".swa-confirm").off("submit").submit();
    //     });
    // });
    

    $('.btn-modal-fee').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('admin/modal_fee');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    $('.btn-modal-cert').click(function(){
            var request_id    = $(this).data("requestid");
            //alert(request_id);
            $.ajax({
                url : "<?php echo site_url('user/modal_cert');?>",
                method : "POST",
                data : {id: request_id},
                success: function(data){
                    $('#modal_request').html(data);
                }
            });
        });

    function updateFee(id, status, uid) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to update the payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_fee/"+id+"/"+status+"/"+uid; 
                    }

        });
    }


    function approveRequest(id, rid) {
        swal({
            title: "Approve Request",
            text: "You want to approve the request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/update_approve_request/"+id+"/"+rid; 
                    } 

        });
    }


    function deleteFee(id, uid) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the payment",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>admin/delete_fee/"+id+"/"+uid; 
                    }

        });
    }

    function deleteCert(id) {
      //alert('wahaha');
        swal({
            title: "Are you sure?",
            text: "You want to delete the certification request",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                      location.href="<?=base_url()?>user/delete_cert/"+id; 
                    }

        });
    }

    function updateFeeParking(id, uid) {
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; 
        var yyyy = today.getFullYear();
        if(dd<10) 
        {
            dd='0'+dd;
        } 
        if(mm<10) 
        {
            mm='0'+mm;
        } 
        today = yyyy+'-'+mm+'-'+dd;
      //alert('wahaha');
      swal({
            title: "Are you sure?",
            text: "You want set the parking expiration",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                      //alert(id);
                       swal("Enter Parking Expiration Date:(yyyy-mm-dd)", {
                           content: {
                                element: "input",
                                attributes: {
                                  value: today,
                                },
                              },
                        })
                        .then((value) => {
                            location.href="<?=base_url()?>admin/update_fee_parking/"+id+"/"+value+"/"+uid; 
                        });
                      
                    }

        });



        // swal("Write something here:", {
        //   content: "input",
        // })
        // .then((value) => {
        //   swal(`You typed: ${value}`);
        // });
    }

</script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable()
    $('#example3').DataTable()
    
  })
</script>


