<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Site Maintenance!</title>
</head>

<body>
    <div class="container-fluid d-flex justify-content-center">
        <div class="card" style="width: 30rem; margin-top:10rem;">
            <div class="card-body">
                <h1 class="card-title">Site Maintenance ...</h1>
                <p class="card-text font-weight-bold">This part of the site is currently under maintenance.</p>
            </div>
        </div>

        <!-- <div class="card" style="width: 30rem; margin-top:10rem;">
            <div class="card-body">
                <h3 class="card-title"><strong>xCODE PH</strong></h3>
                <h5 class="card-text mt-0">Manila, Philippines</h5>
                <p class="card-text my-0">curiosomarcusjonas@gmail.com</p>
                <p class="card-text my-0">09280195803 / 09280195803</p>
                <br>
                <h5 class="card-title mb-0">Marcus Jonas Curioso</h5>
                <p class="card-text my-0 font-weight-bold">Web Developer</p>
                <p class="card-text my-0">curiosomarcusjonas@gmail.com</p>
                <p class="card-text my-0">09280195803 / 09280195803</p>
            </div>
        </div> -->
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>