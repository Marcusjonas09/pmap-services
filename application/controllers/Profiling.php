<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profiling extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model('AdminModel');
        $this->load->model('ParameterModel');
        $this->load->model('JobsModel');
        $this->load->model('VendorsModel');
        $this->load->model('PackageModel');
        $this->load->model('CompanyModel');
        $this->load->model('SubscriberModel');
    }
    public function index()
    {
        $this->load->view('PMAP/user/profiling/login');
    }

    public function signin()
    {
        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/user/profiling/login');
    }

    public function store_user()
    {
        $cookie_name = "user";
        $cookie_value = $_POST['user'];
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
        if (!isset($_COOKIE[$cookie_name])) {
            echo json_encode("Cookie named '" . $cookie_name . "' is not set!");
        } else {
            echo json_encode($_COOKIE[$cookie_name]);
        }
    }

    public function login()
    {
        $this->form_validation->set_rules('email', 'Email Address', 'required|strip_tags|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|strip_tags|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->signin();
        } else {
            $API = "http://pmap.site/api/v2/login";
            $data_array =  array(
                "email" => $_POST['email'],
                "password" => $_POST['password'],
            );
            $make_call = $this->callAPI('POST', $API, json_encode($data_array));
            $response = json_decode($make_call, true);
            $status   = $response['status'];
            $token   = $response['data']['token'];
            $user   = $response['data']['user'];
            // dd($user['id']);

            if ($status) {
                $this->session->set_userdata('token', $token);
                // $this->session->set_userdata('logged_in', true);
                if (($this->SubscriberModel->check_if_subbed_to_jobs($user['id']) || $this->SubscriberModel->check_if_subbed_to_vendors($user['id']))) {
                    $data = array(
                        'user_id' => $user['id'],
                        'role' => $user['role_id'] == 1 ? 'admin' : 'normal',
                        'name' => $user['name'],
                        'logged_in' => TRUE,
                        'type' => $user['role_id'] == 1 ? 'admin' : 'normal',
                        'image' => 'default_avatar.png',
                        'vendor_access' => TRUE,
                        'job_access' => TRUE,
                    );

                    $this->session->set_userdata($data);
                    $this->session->set_userdata('services_user', $user);
                    redirect($_POST['url']);
                    // } else {
                    //     $this->session->set_flashdata('services_access_fail', 'You are now allowed to access this service.');

                    //     redirect('login');
                    // }
                } else {
                    $this->session->set_flashdata('services_access_fail', 'Incorrect Email/Password');
                    redirect('login');
                }
            }
        }
    }

    public function callAPI($method, $url, $data)
    {
        $curl = curl_init();
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer' . $this->session->token,
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
            die("Connection Failure");
        }
        curl_close($curl);
        return $result;
    }

    public function register()
    {
        $this->load->view('include/header');
        $this->load->view('include/mainnav');
        $this->load->view('customer/register');
        $this->load->view('include/footer');
    }

    public function dd($data)
    {
        echo '<pre>';
        print_r($data);
        echo '<pre>';
        die();
    }
}
