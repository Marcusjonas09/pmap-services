<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Services extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model('UserModel');
        $this->load->model('JobsModel');
        $this->load->model('VendorsModel');
        $this->load->model('ParameterModel');
        $this->load->model('PackageModel');
        $this->load->model('CompanyModel');
        $this->load->model('SubscriberModel');
        $this->load->model('CMSModel');
        $this->perPage = 10;
    }

    public function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // WELCOME 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function welcome()
    {
        $data['job_packages'] = $this->PackageModel->get_all_job_packages();
        $data['vendor_packages'] = $this->PackageModel->get_all_vendor_packages();
        $data['contents'] = $this->CMSModel->contents();

        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/welcome/index', $data);
        $this->load->view('PMAP/includes/global_scripts');
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }



    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF WELCOME 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // PROFILING 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function job_request()
    {
        // $this->dd($_SESSION);
        if (isset($_POST['package_id'])) {
            $data['package'] = $this->PackageModel->get_package($_POST['package_id']);
            $this->load->view('PMAP/includes/header');
            $this->load->view('PMAP/includes/navbar');
            $this->load->view('PMAP/user/profiling/job_request', $data);
            $this->load->view('PMAP/includes/global_scripts');
            $this->load->view('PMAP/includes/job_scripts');
            $this->load->view('PMAP/includes/footer');
        } else {
            redirect('welcome');
        }
    }

    public function vendor_request()
    {
        if (isset($_POST['package_id'])) {
            $data['package'] = $this->PackageModel->get_package($_POST['package_id']);
            $this->load->view('PMAP/includes/header');
            $this->load->view('PMAP/includes/navbar');
            $this->load->view('PMAP/user/profiling/vendor_request', $data);
            $this->load->view('PMAP/includes/global_scripts');
            $this->load->view('PMAP/includes/job_scripts');
            $this->load->view('PMAP/includes/footer');
        } else {
            redirect('welcome');
        }
    }

    public function submit_job_request()
    {

        $this->dd($_POST);
        // $this->dd($_POST);
        // $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags|is_unique[company_tbl.CompanyName]|max_length[100]|trim');
        // $this->form_validation->set_rules('CompanyAddress', 'Company Address', 'required|strip_tags|max_length[500]|trim');
        // $this->form_validation->set_rules('CompanyEmail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email');
        // $this->form_validation->set_rules('CompanyTelNo', 'Company Telephone Number', 'required|strip_tags|max_length[100]|trim');
        // $this->form_validation->set_rules('CompanyCelNo', 'Company Cellphone Number', 'required|strip_tags|max_length[100]|trim');
        // $this->form_validation->set_rules('ContactFullname', 'Fullname ', 'required|strip_tags|max_length[255]|trim');
        // $this->form_validation->set_rules('ContactNo', 'Contact Number', 'required|strip_tags|max_length[100]|trim');
        // $this->form_validation->set_rules('ContactEmail', 'Email Address', 'required|strip_tags|max_length[100]|trim');
        // $this->form_validation->set_rules('ContactPosition', 'Company Position', 'required|strip_tags|max_length[100]|trim');

        $CompanyDetails = array(
            'CompanyName' => $_POST['CompanyName'],
            'CompanyAddress' => $_POST['CompanyAddress'],
            'CompanyEmail' => $_POST['CompanyEmail'],
            'CompanyTelNo' => isset($_POST['CompanyTelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
            'CompanyCelNo' => isset($_POST['CompanyCelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
            'ContactFullname' => $_POST['ContactFullname'],
            'ContactNo' => $_POST['ContactNo'],
            'ContactEmail' => $_POST['ContactEmail'],
            'ContactPosition' => $_POST['ContactPosition'],
        );

        $sub_details = array(
            'company_id' => $_SESSION['services_user']['id'],
            'package_id' => $this->input->post('package_id'),
            'package_type' => $this->input->post('package_type'),
            'date_submitted' => date('Y-m-d H:i:s', time()),
        );

        // $this->dd($sub_details);

        // if ($this->form_validation->run() == FALSE) {
        //     $this->job_request();
        // } else {
        // $this->dd($CompanyDetails);
        $this->SubscriberModel->add_subscriber($sub_details);
        $message = '<div class="alert alert-success" role="alert">Company details saved!</div>';
        redirect('welcome');
        // $this->welcome();
        // }
    }

    public function submit_vendor_request()
    {
        $this->dd($_POST);
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags|is_unique[company_tbl.CompanyName]|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyAddress', 'Company Address', 'required|strip_tags|max_length[500]|trim');
        $this->form_validation->set_rules('CompanyEmail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email');
        $this->form_validation->set_rules('CompanyTelNo', 'Company Telephone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyCelNo', 'Company Cellphone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactFullname', 'Fullname ', 'required|strip_tags|max_length[255]|trim');
        $this->form_validation->set_rules('ContactNo', 'Contact Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactEmail', 'Email Address', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactPosition', 'Company Position', 'required|strip_tags|max_length[100]|trim');

        // $CompanyDetails = array(
        //     'CompanyName' => $_POST['CompanyName'],
        //     'CompanyAddress' => $_POST['CompanyAddress'],
        //     'CompanyEmail' => $_POST['CompanyEmail'],
        //     'CompanyTelNo' => isset($_POST['CompanyTelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
        //     'CompanyCelNo' => isset($_POST['CompanyCelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
        //     'ContactFname' => $_POST['ContactFname'],
        //     'ContactLname' => $_POST['ContactLname'],
        //     'ContactMname' => $_POST['ContactMname'],
        //     'ContactTelNo' => isset($_POST['ContactTelno']) ? $_POST['ContactTelno'] : 'N/A',
        //     'ContactCelNo' => isset($_POST['ContactCelNo']) ? $_POST['ContactCelNo'] : 'N/A',
        //     'ContactEmail' => $_POST['ContactEmail'],
        //     'ContactPosition' => $_POST['ContactPosition'],
        // );

        if ($this->form_validation->run() == FALSE) {
            if ($_POST['package_type'] == 'job') {
                $this->job_request();
            } else {
                $this->vendor_request();
            }
        } else {
            // $this->CompanyModel->add_company($CompanyDetails);
            $message = '<div class="alert alert-success" role="alert">Company details saved!</div>';
            if ($_POST['package_type'] == 'job') {
                $this->job_request();
            } else {
                $this->vendor_request();
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PROFILING 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // PACKAGES 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function job_pricing()
    {
        $data['job_packages'] = $this->PackageModel->get_all_job_packages();
        $data['contents'] = $this->CMSModel->contents();

        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/packages/job_package', $data);
        $this->load->view('PMAP/includes/global_scripts');
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    public function vendor_pricing()
    {
        $data['vendor_packages'] = $this->PackageModel->get_all_vendor_packages();
        $data['contents'] = $this->CMSModel->contents();
        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/packages/vendor_package', $data);
        $this->load->view('PMAP/includes/global_scripts');
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PACKAGES 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function jobs()
    {
        $data['JobFunctions'] = $this->ParameterModel->getJobFunctions();
        $data['Industries'] = $this->ParameterModel->getIndustries();

        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/jobs/index', $data);
        $this->load->view('PMAP/includes/global_scripts');
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    function pagination()
    {
        $job_title = $this->input->post('jobtitle');
        $job_location = $this->input->post('joblocation');

        $isSpotlight = $this->input->post('isSpotlight');
        $isPreferred = $this->input->post('isPreferred');
        $isMemberCompany = $this->input->post('isMemberCompany');

        $jobFunction = $this->input->post('jobFunction');
        $Industry = $this->input->post('Industry');

        $this->load->library("pagination");

        $config = array();

        $config["total_rows"] = $this->JobsModel->count_results($job_title, $job_location, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry);

        $config["base_url"] = base_url() . 'User/jobs';
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination m-0 col-md-12">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li class="page-item page-link">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li class="page-item page-link">';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li class="page-item page-link"> ';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = '&lt;';
        $config["prev_tag_open"] = '<li class="page-item page-link">';
        $config["prev_tag_close"] = '</li>';
        $config["cur_tag_open"] = '<li class="active page-item"><a class="page-link" href="#">';
        $config["cur_tag_close"] = '</a></li>';
        $config["num_tag_open"] = '<li class="page-item page-link"> ';
        $config["num_tag_close"] = '</li>';
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        $per_page = $config["per_page"];

        $output = array(
            'pagination_link'  => $this->pagination->create_links(),
            'jobs_table'   => $this->JobsModel->fetch_details($per_page, $start, $job_title, $job_location, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry),
            'results' => $this->JobsModel->count_results($job_title, $job_location, $isSpotlight, $isPreferred, $isMemberCompany, $jobFunction, $Industry),
            'function' => $jobFunction
        );

        echo json_encode($output);
    }

    public function getJobSingle($JobId)
    {
        $output = array(
            'JobDetails'   => $this->JobsModel->getjobHTML($JobId),
            'JobId' => $JobId ? $JobId : 0
        );
        echo json_encode($output);
    }

    public function getJobRecent()
    {
        $output = array(
            'JobId'   => $this->JobsModel->getJobRecent()
        );
        echo json_encode($output);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // VENDORS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function vendors()
    {
        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/vendors/index');
        $this->load->view('PMAP/includes/global_scripts');
        $this->load->view('PMAP/includes/vendor_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    public function pagination_vendor()
    {
        $CompanyName = $this->input->post('CompanyName');
        $ServiceOffered = $this->input->post('ServiceOffered');

        $this->load->library("pagination");

        $config = array();

        $config["total_rows"] = $this->VendorsModel->count_results($CompanyName, $ServiceOffered);

        $config["base_url"] = "User/vendors/";
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination m-0 col-md-12">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li class="page-item page-link">';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li class="page-item page-link">';
        $config["last_tag_close"] = '</li>';
        $config['next_link'] = '&gt;';
        $config["next_tag_open"] = '<li class="page-item page-link"> ';
        $config["next_tag_close"] = '</li>';
        $config["prev_link"] = '&lt;';
        $config["prev_tag_open"] = '<li class="page-item page-link">';
        $config["prev_tag_close"] = '</li>';
        $config["cur_tag_open"] = '<li class="active page-item"><a class="page-link" href="#">';
        $config["cur_tag_close"] = '</a></li>';
        $config["num_tag_open"] = '<li class="page-item page-link"> ';
        $config["num_tag_close"] = '</li>';
        $config["num_links"] = 1;
        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        $per_page = $config["per_page"];

        $output = array(
            'pagination_link'  => $this->pagination->create_links(),
            'vendors_table'   => $this->VendorsModel->fetch_details($per_page, $start, $CompanyName, $ServiceOffered),
            'results' => $this->VendorsModel->count_results($CompanyName, $ServiceOffered)
            // ,
            // 'results' => $this->VendorsModel->count_results($vendor_name, $service_title)
        );
        echo json_encode($output);
    }

    public function getVendorSingle($ServiceId)
    {
        $output = array(
            'VendorDetails'   => $this->VendorsModel->getVendorHTML($ServiceId)
        );

        echo json_encode($output);
    }

    public function getVendorRecent()
    {

        if ($this->VendorsModel->getVendorRecent()) {
            $output = array(
                'VendorDetails'   => $this->VendorsModel->getVendorRecent()
            );
        } else {
            $output = false;
        }

        echo json_encode($output);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF VENDORS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function authenticate()
    {
        if (!$this->session->logged_in) {
            redirect('user/login');
        }
    }

    public function index()
    {
        $this->jobs();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('user/login');
    }
}
