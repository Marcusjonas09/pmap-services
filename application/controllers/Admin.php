<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Twilio\Rest\Client;

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model('AdminModel');
        $this->load->model('ParameterModel');
        $this->load->model('JobsModel');
        $this->load->model('VendorsModel');
        $this->load->model('PackageModel');
        $this->load->model('CompanyModel');
        $this->load->model('SubscriberModel');
        $this->load->model('CMSModel');
    }

    public function maintenance()
    {
        $this->load->view('maintenance');
    }

    public function authenticate($arr)
    {
        if (!$this->session->logged_in) {
            redirect('Admin/index');
        }

        $role = $this->session->role;
        $allow = false;
        foreach ($arr as $a) {
            if ($role == $a) {
                $allow = true;
                break;
            }
        }
        if (!$allow) {
            redirect('Admin/index');
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // REPORTS
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function JobsVsVendors()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $JobsRegisteredCompanies = $this->SubscriberModel->JobsRegisteredCompanies();
        $VendorRegisteredCompanies = $this->SubscriberModel->VendorRegisteredCompanies();

        $response = array(
            ['label', "No. of Subscribers"],
            [
                'Job Service', $JobsRegisteredCompanies
            ],
            [
                'Vendor Service', $VendorRegisteredCompanies
            ]
        );
        echo json_encode($response);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF REPORTS
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // DASHBOARD
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function JobsRegisteredCompanies()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        echo json_encode($this->SubscriberModel->JobsRegisteredCompanies());
    }

    public function VendorRegisteredCompanies()
    {
        $arr = array('admin');
        $this->authenticate($arr);
        echo json_encode($this->SubscriberModel->VendorRegisteredCompanies());
    }

    public function PendingJobsRequest()
    {
        $arr = array('admin');
        $this->authenticate($arr);
        echo json_encode($this->SubscriberModel->PendingJobsRequest());
    }

    public function PendingVendorsRequest()
    {
        $arr = array('admin');
        $this->authenticate($arr);
        echo json_encode($this->SubscriberModel->PendingVendorsRequest());
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF DASHBOARD
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // CMS
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function contents($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CMS/contents';
        $view['custom_css'] = 'admin/CMS/content_css';
        $view['custom_js'] = 'admin/CMS/content_js';
        $view['contents'] = $this->CMSModel->contents();

        $view['message'] = $message;

        $this->load->view('main', $view);
    }

    public function preview_landing()
    {

        $arr = array('admin');
        $this->authenticate($arr);

        $data['job_packages'] = $this->PackageModel->get_all_job_packages();
        $data['vendor_packages'] = $this->PackageModel->get_all_vendor_packages();
        $data['contents'] = $this->CMSModel->contents();

        $this->load->view('PMAP/includes/header');
        // $this->load->view('PMAP/includes/navbar');
        $this->load->view('admin/CMS/previews/LandingPage', $data);
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    public function preview_job_pricing()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $data['job_packages'] = $this->PackageModel->get_all_job_packages();
        $data['contents'] = $this->CMSModel->contents();

        $this->load->view('PMAP/includes/header');
        // $this->load->view('PMAP/includes/navbar');
        $this->load->view('admin/CMS/previews/JobPricing', $data);
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    public function preview_vendor_pricing()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $data['vendor_packages'] = $this->PackageModel->get_all_vendor_packages();
        $data['contents'] = $this->CMSModel->contents();

        $this->load->view('PMAP/includes/header');
        // $this->load->view('PMAP/includes/navbar');
        $this->load->view('admin/CMS/previews/VendorPricing', $data);
        $this->load->view('PMAP/includes/job_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    public function add_content($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CMS/add_content';
        $view['custom_css'] = 'admin/CMS/content_css';
        $view['custom_js'] = 'admin/CMS/content_js';
        $view['message'] = $message;

        $this->load->view('main', $view);
    }

    public function edit_content($content_id, $message = null)
    {
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CMS/edit_content';
        $view['custom_css'] = 'admin/CMS/content_css';
        $view['custom_js'] = 'admin/CMS/content_js';
        $view['message'] = $message;
        $view['contents'] = $this->CMSModel->view_content($content_id);

        $this->load->view('main', $view);
    }

    public function delete_content()
    {
        $this->CMSModel->delete_content($_POST['content_id']);
    }

    ///////////////////////////////////////////////////////////////////////////////

    public function create_content()
    {

        $upload_path = './uploads/';
        $config['upload_path']          = $upload_path;
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['overwrite']            = TRUE;
        $config['max_size']             = 1000;
        $config['max_width']            = 1366;
        $config['max_height']           = 768;
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('content_title', 'Title', 'required|strip_tags');
        $this->form_validation->set_rules('content_description', 'Description', 'required');
        $message = null;
        if ($this->form_validation->run() == FALSE) {
            if ($_POST['content_layout'] != "card_regular" && !$this->upload->do_upload('content_image')) {
                $message = 'The image File required!';
            }
            $this->add_content($message);
        } else {
            if ($_POST['content_layout'] != "card_regular" && !$this->upload->do_upload('content_image')) {
                $message = 'The image File required!';
                $this->add_content($message);
            } else {
                if (!$this->upload->do_upload('content_image')) {
                    $error = $this->upload->display_errors();
                    $this->form_validation->set_message('image_check', $error);
                } else {
                    $filename = $_FILES['content_image']['name'];
                    $this->load->library('image_lib');
                    $config2['image_library'] = 'gd2';
                    $config2['source_image'] = $upload_path . $filename;
                    $config2['new_image'] = './uploads_thumb';
                    $config2['create_thumb'] = FALSE;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width']         = 150;
                    $config2['height']       = 100;
                    $this->image_lib->initialize($config2);
                    $this->image_lib->resize();
                }
                $data['content_image'] = $_FILES['content_image']['name'];
                $data['content_title'] = $_POST['content_title'];
                $data['content_description'] = $_POST['content_description'];
                $data['page_assignment'] = $_POST['page_assignment'];
                $data['content_layout'] = $_POST['content_layout'];
                $data['created_at'] = date("Y-m-d H:i:s");
                $this->CMSModel->add_content($data);
                $message = '<div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Content added succesfully!</div>';
                $this->contents($message);
            }
        }
    }

    public function update_content()
    {
        $message = null;
        $this->form_validation->set_rules('content_title', 'Title', 'required|strip_tags');
        $this->form_validation->set_rules('content_description', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            if ($_POST['content_layout'] != "card_regular") {
                $message = 'Image File required!';
            }
            $this->edit_content($_POST['content_id'], $message);
        } else {

            $upload_path = './uploads/';
            $config['upload_path']          = $upload_path;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['overwrite']            = TRUE;
            $config['max_size']             = 1000;
            $config['max_width']            = 1366;
            $config['max_height']           = 768;
            $this->load->library('upload', $config);
            if (!empty($_FILES['content_image']['name'])) {
                if (!$this->upload->do_upload('content_image')) {
                    $error = $this->upload->display_errors();
                    $this->form_validation->set_message('image_check', $error);
                } else {
                    $filename = $_FILES['content_image']['name'];
                    $this->load->library('image_lib');
                    $config2['image_library'] = 'gd2';
                    $config2['source_image'] = $upload_path . $filename;
                    $config2['new_image'] = './uploads_thumb';
                    $config2['create_thumb'] = FALSE;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width']         = 150;
                    $config2['height']       = 100;
                    $this->image_lib->initialize($config2);
                    $this->image_lib->resize();
                }
                $data['content_image'] = $_FILES['content_image']['name'];
            }

            $data['content_title'] = $_POST['content_title'];
            $data['content_description'] = $_POST['content_description'];
            $data['page_assignment'] = $_POST['page_assignment'];
            $data['content_layout'] = $_POST['content_layout'];
            $data['updated_at'] = date("Y-m-d H:i:s");

            // $this->dd;
            $this->load->model('CMSModel');
            $this->CMSModel->edit_content($_POST['content_id'], $data);
            $message = '<div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Content updated succesfully!</div>';
            $this->contents($message);
        }
    }

    public function publish_content()
    {
        $this->CMSModel->publish_content($_POST['content_id']);
        $response = array(
            'status' => 'success',
            'icon' => 'success',
            'message' => "Content published!"
        );
        echo json_encode($response);
    }

    public function unpublish_content()
    {
        $this->CMSModel->unpublish_content($_POST['content_id']);
        $response = array(
            'status' => 'success',
            'icon' => 'success',
            'message' => "Content Unpublished!"
        );
        echo json_encode($response);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF CMS
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // COMPANY PROFILE
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function companies($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CompanyProfile/all_companies';
        $view['custom_css'] = 'admin/CompanyProfile/company_css';
        $view['custom_js'] = 'admin/CompanyProfile/company_js';
        $view['companies'] = $this->CompanyModel->companies();

        $view['message'] = $message;

        $this->load->view('main', $view);
    }

    public function view_company()
    {
        $CompanyId = $_POST["CompanyId"];

        $company = $this->CompanyModel->view_company($CompanyId);

        echo '
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel"><strong>View Company Profile </strong></h4>

                </div>
                <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                    <h4><strong>Company Details</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="CompanyName">Company Name</label>

                    <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyAddress">Company Address</label>
                    <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyTelNo">Telephone No. </label>
                    <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyCelNo">Mobile No.</label>
                    <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">Company Email</label>
                    <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>

                <div class="form-group col-md-6">
                    <label for="HRemail">HR Email Address</label>
                    <input readonly value="' . $company->HRemail . '" type="email" name="HRemail" id="HRemail" class="form-control" placeholder="example@example.com">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h4><strong>Contact Person</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="ContactFname">Fullname</label>
                    <input readonly value="' . $company->ContactFullname . '" type="text" name="ContactFname" id="ContactFname" placeholder="Fullname" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactPosition">Position in the company</label>
                    <input readonly value="' . $company->ContactPosition . '" type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactTelNo">Telephone No.</label>
                    <input readonly value="' . $company->ContactNo . '" type="text" name="ContactTelNo" id="ContactTelNo" placeholder="Telephone No" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactEmail">Email</label>
                    <input readonly value="' . $company->ContactEmail . '" type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control">
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        ';
    }

    public function get_company_for_job()
    {
        if (isset($_POST["sub_id"]) && $_POST["sub_id"] != "") {
            $sub_id = $_POST["sub_id"];

            $company = $this->SubscriberModel->view_job_subscriber_valid($sub_id);

            echo '
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="CompanyName">Company Name</label>

                    <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyAddress">Company Address</label>
                    <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyTelNo">Telephone No. </label>
                    <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyCelNo">Mobile No.</label>
                    <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">Company Email</label>
                    <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">HR Email Address</label>
                    <input readonly value="' . $company->HRemail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>
            </div>
            <input type="hidden" name="CompanyId" value="' . $company->CompanyId . '">
            <input type="hidden" name="CompanyName" value="' . $company->CompanyName . '">
            <input type="hidden" name="CompanyLocation" value="' . $company->CompanyAddress . '">
            
        ';
        } else {
            echo "";
        }
    }

    public function get_company_for_vendor()
    {
        if (isset($_POST["sub_id"]) && $_POST["sub_id"] != "") {
            $sub_id = $_POST["sub_id"];

            $company = $this->SubscriberModel->view_vendor_subscriber_valid($sub_id);
            echo '
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="CompanyName">Company Name</label>

                    <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyAddress">Company Address</label>
                    <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyTelNo">Telephone No. </label>
                    <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyCelNo">Mobile No.</label>
                    <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">Company Email</label>
                    <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>
            </div>
            <input type="hidden" name="CompanyId" value="' . $company->CompanyId . '">
            <input type="hidden" name="CompanyName" value="' . $company->CompanyName . '">
            <input type="hidden" name="CompanyLocation" value="' . $company->CompanyAddress . '">
            <input type="hidden" name="PackageType" value="' . $company->package_type . '">
        ';
        } else {
            echo "";
        }
    }

    public function get_company()
    {
        if (isset($_POST["CompanyId"]) && $_POST["CompanyId"] != "") {
            $CompanyId = $_POST["CompanyId"];

            $company = $this->CompanyModel->view_company($CompanyId);
            echo '
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="CompanyName">Company Name</label>

                    <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyAddress">Company Address</label>
                    <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyTelNo">Telephone No. </label>
                    <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                </div>

                <div class="form-group col-md-3">
                    <label for="CompanyCelNo">Mobile No.</label>
                    <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">Company Email</label>
                    <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>

                <div class="form-group col-md-6">
                    <label for="CompanyEmail">HR Email Address</label>
                    <input readonly value="' . $company->HRemail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h4><strong>Contact Person</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="ContactFname">Fullname</label>
                    <input readonly value="' . $company->ContactFullname . '" type="text" name="ContactFname" id="ContactFname" placeholder="Fullname" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactPosition">Position in the company</label>
                    <input readonly value="' . $company->ContactPosition . '" type="text" name="ContactPosition" id="ContactPosition" placeholder="e.g. Manager" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactTelNo">Telephone No.</label>
                    <input readonly value="' . $company->ContactNo . '" type="text" name="ContactTelNo" id="ContactTelNo" placeholder="Telephone No" class="form-control">
                </div>

                <div class="form-group col-md-6">
                    <label for="ContactEmail">Email</label>
                    <input readonly value="' . $company->ContactEmail . '" type="text" name="ContactEmail" id="ContactEmail" placeholder="example@example.com" class="form-control">
                </div>
            </div>
            <input type="hidden" name="CompanyId" value="' . $company->CompanyId . '">
            <input type="hidden" name="CompanyName" value="' . $company->CompanyName . '">
            <input type="hidden" name="CompanyLocation" value="' . $company->CompanyAddress . '">
        ';
        } else {
            echo "";
        }
    }

    public function add_company()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CompanyProfile/add_company';
        $view['custom_css'] = 'admin/CompanyProfile/company_css';
        $view['custom_css'] = 'admin/CompanyProfile/company_css';
        $view['Packages'] = $this->PackageModel->get_all_packages();

        $this->load->view('main', $view);
    }

    public function create_company()
    {
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags|is_unique[company_tbl.CompanyName]|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyAddress', 'Company Address', 'required|strip_tags|max_length[500]|trim');
        $this->form_validation->set_rules('CompanyEmail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.CompanyEmail]');
        $this->form_validation->set_rules('HRemail', 'HR Email Address', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.HRemail]');
        $this->form_validation->set_rules('CompanyTelNo', 'Company Telephone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyCelNo', 'Company Cellphone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactFullname', 'Fullname ', 'required|strip_tags|max_length[255]|trim');
        $this->form_validation->set_rules('ContactNo', 'Contact Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactEmail', 'Email Address', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.ContactEmail]');
        $this->form_validation->set_rules('ContactPosition', 'Company Position', 'required|strip_tags|max_length[100]|trim');

        $CompanyDetails = array(
            'CompanyName' => $_POST['CompanyName'],
            'CompanyAddress' => $_POST['CompanyAddress'],
            'CompanyEmail' => $_POST['CompanyEmail'],
            'HRemail' => $_POST['HRemail'],
            'CompanyTelNo' => isset($_POST['CompanyTelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
            'CompanyCelNo' => isset($_POST['CompanyCelNo']) ? $_POST['CompanyCelNo'] : 'N/A',
            'ContactFullname' => $_POST['ContactFullname'],
            'ContactNo' => $_POST['ContactNo'],
            'ContactEmail' => $_POST['ContactEmail'],
            'ContactPosition' => $_POST['ContactPosition'],
        );

        if ($this->form_validation->run() == FALSE) {
            $this->add_company();
        } else {
            $this->CompanyModel->add_company($CompanyDetails);
            $message = '<div class="alert alert-success" role="alert">Company details saved!</div>';
            $this->companies($message);
        }
    }

    public function edit_company($CompanyId)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/CompanyProfile/edit_company';
        $view['custom_css'] = 'admin/CompanyProfile/company_css';
        $view['custom_css'] = 'admin/CompanyProfile/company_css';
        $view['company'] = $this->CompanyModel->view_company($CompanyId);

        $this->load->view('main', $view);
    }

    public function update_company()
    {

        $CompanyId = $this->input->post('CompanyId');
        $company = $this->CompanyModel->view_company($CompanyId);
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyAddress', 'Company Address', 'required|strip_tags|max_length[500]|trim');
        $this->form_validation->set_rules('CompanyEmail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.CompanyEmail]');
        $this->form_validation->set_rules('HRemail', 'HR Email Address', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.HRemail]');
        $this->form_validation->set_rules('CompanyTelNo', 'Company Telephone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('CompanyCelNo', 'Company Cellphone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactFullname', 'Fullname ', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactNo', 'Telephone Number', 'required|strip_tags|max_length[100]|trim');
        $this->form_validation->set_rules('ContactEmail', 'Email Address', 'required|strip_tags|max_length[100]|trim|valid_email|is_unique[company_tbl.ContactEmail]');
        $this->form_validation->set_rules('ContactPosition', 'Company Position', 'required|strip_tags|max_length[100]|trim');

        if (trim($_POST['CompanyEmail'], " ") ==  trim($company->CompanyEmail, " ")) {
            $this->form_validation->set_rules('CompanyEmail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email');
        }

        if (trim($_POST['HRemail'], " ") ==  trim($company->HRemail, " ")) {
            $this->form_validation->set_rules('HRemail', 'Company Email', 'required|strip_tags|max_length[100]|trim|valid_email');
        }

        if (trim($_POST['ContactEmail'], " ") ==  trim($company->ContactEmail, " ")) {

            $this->form_validation->set_rules('ContactEmail', 'Email Address', 'required|strip_tags|max_length[100]|trim|valid_email');
        }

        $CompanyDetails = array(
            'CompanyName' => $_POST['CompanyName'],
            'CompanyAddress' => $_POST['CompanyAddress'],
            'CompanyEmail' => $_POST['CompanyEmail'],
            'HRemail' => $_POST['HRemail'],
            'CompanyTelNo' => isset($_POST['CompanyTelNo']) ? $_POST['CompanyTelNo'] : 'N/A',
            'CompanyCelNo' => isset($_POST['CompanyCelNo']) ? $_POST['CompanyCelNo'] : 'N/A',
            'ContactFullname' => $_POST['ContactFullname'],
            'ContactNo' => $_POST['ContactNo'],
            'ContactEmail' => $_POST['ContactEmail'],
            'ContactPosition' => $_POST['ContactPosition'],
        );

        if ($this->form_validation->run() == FALSE) {
            $this->edit_company($CompanyId);
        } else {
            $this->CompanyModel->edit_company($CompanyId, $CompanyDetails);
            $message = '<div class="alert alert-success" role="alert">Company details saved!</div>';
            $this->companies($message);
        }
    }

    public function delete_company()
    {
        $this->CompanyModel->delete_company($this->input->post('CompanyId'));
    }



    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF COMPANY PROFILE
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function jobs($message = null)
    {
        //dd($this->session->sms);
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Jobs/all_jobs';
        $view['custom_css'] = 'admin/Jobs/jobs_css';
        $view['custom_js'] = 'admin/Jobs/jobs_js';

        $view['published'] = $this->JobsModel->getPublishedJobs();
        $view['unpublished'] = $this->JobsModel->getUnpublishedJobs();
        $view['expired'] = $this->JobsModel->getExpiredJobs();
        $view['deleted'] = $this->JobsModel->getDeletedJobs();
        $view['message'] = $message;

        $this->load->view('main', $view);
    }

    public function add_job()
    {
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Jobs/add_job';
        $view['custom_css'] = 'admin/Jobs/jobs_css';
        $view['custom_js'] = 'admin/Jobs/jobs_js';

        $view['parameters'] = $this->ParameterModel->fetchParameters();
        $view['parameterValues'] = $this->ParameterModel->fetchParameterValues();
        $view['Industries'] = $this->ParameterModel->getIndustries();
        $view['JobFunctions'] = $this->ParameterModel->getJobFunctions();
        $view['JobTypes'] = $this->ParameterModel->getJobTypes();
        $view['JobDurations'] = $this->ParameterModel->getJobDurations();
        $view['MinEducations'] = $this->ParameterModel->getMinEducations();
        $view['MinExperiences'] = $this->ParameterModel->getMinExperiences();
        $view['RequiredTravels'] = $this->ParameterModel->getRequiredTravels();
        $view['companies'] = $this->SubscriberModel->job_subscribers_valid();

        $this->load->view('main', $view);
    }

    public function post_job()
    {
        if (!empty($_POST['sub_id'])) {
            $subscriber = $this->SubscriberModel->view_subscriber($_POST['sub_id']);
            $package = $this->PackageModel->get_package($subscriber->package_id);
            $this->form_validation->set_rules('PostDuration', 'Post Duration', 'required|strip_tags|is_natural_no_zero|less_than_equal_to[' . $package->post_duration . ']');
        }

        $this->form_validation->set_rules('sub_id', 'Subscriber', 'required|strip_tags');

        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags');
        $this->form_validation->set_rules('CompanyLocation', 'Company Address', 'required|strip_tags');

        $this->form_validation->set_rules('JobPositionTitle', 'Job Title', 'required|strip_tags');
        $this->form_validation->set_rules('JobLocation', 'Job Location', 'required|strip_tags');
        $this->form_validation->set_rules('JobDescription', 'Job Description', 'required|strip_tags');
        $this->form_validation->set_rules('JobRequirements', 'Job Requirements', 'required|strip_tags');
        $this->form_validation->set_rules('Industry', 'Industry', 'required|strip_tags');
        $this->form_validation->set_rules('JobFunction', 'Job Function', 'required|strip_tags');
        $this->form_validation->set_rules('JobType', 'Job Type', 'required|strip_tags');
        $this->form_validation->set_rules('JobDuration', 'Job Duration', 'required|strip_tags');
        $this->form_validation->set_rules('MinEducation', 'Minimum Education', 'required|strip_tags');
        $this->form_validation->set_rules('MinExperience', 'Minimum Experience', 'required|strip_tags');
        $this->form_validation->set_rules('RequiredTravel', 'Travel', 'required|strip_tags');
        $this->form_validation->set_rules('MinSalary', 'Minimum Salary', 'required|strip_tags');
        $this->form_validation->set_rules('MaxSalary', 'Max Salary', 'required|strip_tags|greater_than_equal_to[' . $_POST['MinSalary'] . ']');

        $Job_details = array(
            'sub_id' => $this->input->post('sub_id'),
            'CompanyId' => $this->input->post('CompanyId'),
            'CompanyName' => $this->input->post('CompanyName'),
            'CompanyLocation' => $this->input->post('CompanyLocation'),

            'JobPositionTitle' => $this->input->post('JobPositionTitle'),
            'JobLocation' => $this->input->post('JobLocation'),
            'JobDescription' => $this->input->post('JobDescription'),
            'JobRequirements' => $this->input->post('JobRequirements'),
            'Industry' => $this->input->post('Industry'),
            'JobFunction' => $this->input->post('JobFunction'),
            'JobType' => $this->input->post('JobType'),
            'JobDuration' => $this->input->post('JobDuration'),
            'MinEducation' => $this->input->post('MinEducation'),
            'MinExperience' => $this->input->post('MinExperience'),
            'MinExperience' => $this->input->post('MinExperience'),
            'RequiredTravel' => $this->input->post('RequiredTravel'),
            'MinSalary' => $this->input->post('MinSalary'),
            'MaxSalary' => $this->input->post('MaxSalary'),
            'PostDuration' => $this->input->post('PostDuration')
        );

        // $this->dd($Job_details);

        if ($this->form_validation->run() == FALSE) {
            $this->add_job();
        } else {
            $message = '<div class="alert alert-success" role="alert">Record added successfully!</div>';
            $message = $this->JobsModel->post_job($Job_details);
            $this->jobs($message);
        }
    }

    public function delete_unpublished()
    {
        $this->JobsModel->delete_unpublished($this->input->post('JobId'));
    }

    public function publish_job()
    {

        // $this->dd();
        // $subscriber = $this->JobsModel->getSubscriber(106);
        // $this->dd($subscriber->monthly_post >= $subscriber->post_limit_monthly);

        $JobId = $this->input->post('JobId');
        $Job = $this->JobsModel->getJob($JobId);
        $subscriber = $this->JobsModel->getSubscriber($Job->CompanyId);
        $response = array();
        if ($subscriber->daily_post >= $subscriber->post_limit_daily) {
            $response = array(
                'status' => 'failed',
                'icon' => 'warning',
                'message' => "You have reached your daily post limit!"
            );
        } else {
            if ($subscriber->monthly_post >= $subscriber->post_limit_monthly) {
                $response = array(
                    'status' => 'failed',
                    'icon' => 'warning',
                    'message' => "You have reached your daily post limit!"
                );
            } else {
                $this->JobsModel->publish_job($Job, $subscriber);
                $response = array(
                    'status' => 'success',
                    'icon' => 'success',
                    'message' => "Published!"
                );
            }
        }
        echo json_encode($response);
    }

    public function repost_job()
    {
        $this->JobsModel->repost_job($this->input->post('JobId'));
    }

    public function unpublish_job()
    {
        $this->JobsModel->unpublish_job($this->input->post('JobId'));
    }

    public function view_job()
    {
        $JobId = $_POST["JobId"];

        $jobSingle = $this->JobsModel->getjob($JobId);

        $company = $this->CompanyModel->view_company($jobSingle->CompanyId);
        if ($jobSingle->PackageType == 1) {
            $package_type = "Basic";
        } else if ($jobSingle->PackageType == 2) {
            $package_type = "Premium";
        } else {
            $package_type = "Ultimate";
        }

        if ($jobSingle->CreatedAt == 0) {
            $CreatedAt = "N/A";
        } else {
            $CreatedAt = date('M-d-Y H:i:s A', strtotime($jobSingle->CreatedAt));
        }

        if ($jobSingle->ExpireAt == 0) {
            $ExpireAt = "N/A";
        } else {
            $ExpireAt = date('M-d-Y H:i:s A', strtotime($jobSingle->ExpireAt));
        }

        echo '
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel"><strong>View Post Information </strong></h4>
                    
                </div>
                <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Package Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Package Type</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $package_type . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Post Duration</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $jobSingle->PostDuration . ' day/s" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Start Date</label>
                                        <input readonly type="text" name="CreatedAt" id="CreatedAt" placeholder="Package Type" value="' . $CreatedAt . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">End Date</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $ExpireAt . '" class="form-control">
                                    </div>
                                </div>

                                <hr style="border:1px solid black;">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="CompanyName">Company Name</label>
                
                                    <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                </div>
                
                                <div class="form-group col-md-6">
                                    <label for="CompanyAddress">Company Address</label>
                                    <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                </div>
                
                                <div class="form-group col-md-3">
                                    <label for="CompanyTelNo">Telephone No. </label>
                                    <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                </div>
                
                                <div class="form-group col-md-3">
                                    <label for="CompanyCelNo">Mobile No.</label>
                                    <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                </div>
                
                                <div class="form-group col-md-6">
                                    <label for="CompanyEmail">Company Email</label>
                                    <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                </div>
                
                                <div class="form-group col-md-6">
                                    <label for="CompanyEmail">HR Email Address</label>
                                    <input readonly value="' . $company->HRemail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                </div>
                                </div>

                                <hr style="border:1px solid black;">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Job Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-6">
                                        <label for="JobPositionTitle">Job Title</label>
                                        <input readonly value="' . $jobSingle->JobPositionTitle . '" type="text" name="JobPositionTitle" id="JobPositionTitle" placeholder="Job Title" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobLocation">Location</label>
                                        <input readonly value="' . $jobSingle->JobLocation . '" type="text" name="JobLocation" id="JobLocation" placeholder="Location" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="JobDescription">Job Description</label>
                                        <div class="well" style="padding:10px; overflow-y:auto; height:200px;">
                                            ' . $jobSingle->JobDescription . '
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="JobRequirements">Job Requirements</label>
                                        <div class="well" style="padding:10px; overflow-y:auto; height:200px;">
                                            ' . $jobSingle->JobRequirements . '
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="Idustry">Industry</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->Industry . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Job Function</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->JobFunction . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobType">Job Type</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->JobType . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobDuration">Job Duration</label>
                                        <input readonly type="text" name="JobDuration" id="JobDuration" placeholder="Duration in day/s" value="' . $jobSingle->JobDuration . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Minimum Education</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->MinEducation . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Minimum Experience</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->MinExperience . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Required Travel</label>
                                        <input readonly type="text" name="RequiredTravel" id="RequiredTravel" placeholder="Minimum Salary" value="' . $jobSingle->RequiredTravel . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Job Post Duration</label>
                                        <input readonly type="text" name="PostDuration" id="PostDuration" placeholder="Duration in days" value="' . $jobSingle->PostDuration . ' day/s" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Minimum Salary</label>
                                        <input readonly type="text" name="MinSalary" id="MinSalary" placeholder="Minimum Salary" value="' . $jobSingle->MinSalary . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="JobFunction">Maximum Salary</label>
                                        <input readonly type="text" name="MaxSalary" id="MaxSalary" placeholder="Minimum Salary" value="' . $jobSingle->MaxSalary . '" class="form-control">
                                    </div>

                                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        ';
    }

    public function edit_job($JobId)
    {

        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Jobs/edit_job';
        $view['custom_css'] = 'admin/Jobs/jobs_css';
        $view['custom_js'] = 'admin/Jobs/jobs_js';

        $view['parameters'] = $this->ParameterModel->fetchParameters();
        $view['parameterValues'] = $this->ParameterModel->fetchParameterValues();
        $view['Industries'] = $this->ParameterModel->getIndustries();
        $view['JobFunctions'] = $this->ParameterModel->getJobFunctions();
        $view['JobTypes'] = $this->ParameterModel->getJobTypes();
        $view['JobDurations'] = $this->ParameterModel->getJobDurations();
        $view['MinEducations'] = $this->ParameterModel->getMinEducations();
        $view['MinExperiences'] = $this->ParameterModel->getMinExperiences();
        $view['RequiredTravels'] = $this->ParameterModel->getRequiredTravels();

        $view['Packages'] = $this->PackageModel->get_all_packages();
        $view['Job'] = $this->JobsModel->getjob($JobId);
        $view['companies'] = $this->SubscriberModel->job_subscribers_valid();

        $this->load->view('main', $view);
    }

    public function update_job()
    {
        $this->form_validation->set_rules('JobPositionTitle', 'Job Position', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobLocation', 'Job Location', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobDuration', 'Job Duration', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobRequirements', 'Job Requirements', 'required|strip_tags');
        $this->form_validation->set_rules('RequiredTravel', 'Required Travel', 'required|strip_tags|trim');
        $this->form_validation->set_rules('PostDuration', 'Post Duration', 'required|strip_tags|trim');
        $this->form_validation->set_rules('MinSalary', 'MinSalary', 'required|strip_tags|trim');
        $this->form_validation->set_rules('MaxSalary', 'Max Salary', 'required|strip_tags|trim');
        $this->form_validation->set_rules('Industry', 'Job Location', 'required|strip_tags|trim');
        $this->form_validation->set_rules('MinEducation', 'Job Requirements', 'required|strip_tags|trim');
        $this->form_validation->set_rules('MinExperience', 'Max Salary', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobType', 'Job Type', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobFunction', 'Job Function', 'required|strip_tags|trim');
        $this->form_validation->set_rules('PackageType', 'PackageType', 'required|strip_tags|trim');
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('CompanyLocation', 'Company Location', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobPositionTitle', 'Job Title', 'required|strip_tags|trim');
        $this->form_validation->set_rules('JobDescription', 'Job Description', 'required|strip_tags');

        $JobId = $this->input->post('JobId');

        $Job_details = array(
            'JobPositionTitle' => $this->input->post('JobPositionTitle'),
            'JobLocation' => $this->input->post('JobLocation'),
            'JobRequirements' => $this->input->post('JobRequirements'),
            'JobDescription' => $this->input->post('JobDescription'),
            'Industry' => $this->input->post('Industry'),
            'JobFunction' => $this->input->post('JobFunction'),
            'JobType' => $this->input->post('JobType'),
            'JobDuration' => $this->input->post('JobDuration'),
            'MinEducation' => $this->input->post('MinEducation'),
            'MinExperience' => $this->input->post('MinExperience'),
            'RequiredTravel' => $this->input->post('RequiredTravel'),
            'MinSalary' => $this->input->post('MinSalary'),
            'MaxSalary' => $this->input->post('MaxSalary'),
            'PostDuration' => $this->input->post('PostDuration')
        );
        if ($this->form_validation->run() == FALSE) {
            $this->edit_job($JobId);
        } else {
            $message = '<div class="alert alert-success" role="alert">Post updated successfully!</div>';
            $this->JobsModel->update_job($JobId, $Job_details);
            $this->jobs($message);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOBS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // VENDORS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function vendors($message = null)
    {
        //dd($this->session->sms);
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Vendors/all_services';
        $view['custom_css'] = 'admin/Vendors/vendors_css';
        $view['custom_js'] = 'admin/Vendors/vendors_js';

        $view['published'] = $this->VendorsModel->getPublishedPosts();
        $view['unpublished'] = $this->VendorsModel->getUnpublishedPosts();
        $view['expired'] = $this->VendorsModel->getExpiredPosts();
        $view['deleted'] = $this->VendorsModel->getDeletedPosts();
        $view['message'] = $message;

        // $this->dd($view['expired']);

        $this->load->view('main', $view);
    }

    public function add_service()
    {
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Vendors/add_service';
        $view['custom_css'] = 'admin/Vendors/vendors_css';
        $view['custom_js'] = 'admin/Vendors/vendors_js';

        // $view['Packages'] = $this->PackageModel->get_all_packages();
        // $view['Companies'] = $this->VendorsModel->getCompanies();

        $view['companies'] = $this->SubscriberModel->vendor_subscribers_valid();

        $this->load->view('main', $view);
    }

    public function post_service()
    {

        if (!empty($_POST['sub_id'])) {
            $subscriber = $this->SubscriberModel->view_subscriber($_POST['sub_id']);
            $package = $this->PackageModel->get_package($subscriber->package_id);
            $this->form_validation->set_rules('PostDuration', 'Post Duration', 'required|strip_tags|is_natural_no_zero|less_than_equal_to[' . $package->post_duration . ']');
        }

        $this->form_validation->set_rules('sub_id', 'Subscriber', 'required|strip_tags');
        $this->form_validation->set_rules('CompanyName', 'Company Name', 'required|strip_tags');
        $this->form_validation->set_rules('CompanyLocation', 'Company Address', 'required|strip_tags');
        $this->form_validation->set_rules('ServiceTitle', 'Service Title', 'required|strip_tags');
        $this->form_validation->set_rules('ServiceDescription', 'Service Description', 'required');

        $post_details = array(
            'sub_id' => $this->input->post('sub_id'),
            'PostDuration' => $this->input->post('PostDuration'),
            'CompanyId' => $this->input->post('CompanyId'),
            'CompanyName' => $this->input->post('CompanyName'),
            'CompanyAddress' => $this->input->post('CompanyLocation'),
            'ServiceTitle' => $this->input->post('ServiceTitle'),
            'ServiceDescription' => $this->input->post('ServiceDescription'),
        );

        if ($this->form_validation->run() == FALSE) {
            $this->add_service();
        } else {
            $message = '<div class="alert alert-success" role="alert">Record added successfully!</div>';
            $message = $this->VendorsModel->post_service($post_details);
            $this->vendors($message);
        }
    }

    public function repost_service()
    {
        $this->VendorsModel->repost_service($this->input->post('ServiceId'));
    }

    public function delete_unpublished_service()
    {
        $this->VendorsModel->delete_unpublished_service($this->input->post('ServiceId'));
    }

    public function publish_service()
    {
        $ServiceId = $this->input->post('ServiceId');
        $Service = $this->VendorsModel->getVendor($ServiceId);
        $subscriber = $this->VendorsModel->getSubscriber($Service->CompanyId);
        $response = array();
        if ($subscriber->daily_post >= $subscriber->post_limit_daily) {
            $response = array(
                'status' => 'failed',
                'icon' => 'warning',
                'message' => "You have reached your daily post limit!"
            );
        } else {
            if ($subscriber->monthly_post >= $subscriber->post_limit_monthly) {
                $response = array(
                    'status' => 'failed',
                    'icon' => 'warning',
                    'message' => "You have reached your daily post limit!"
                );
            } else {
                $this->VendorsModel->publish_service($Service, $subscriber);
                $response = array(
                    'status' => 'success',
                    'icon' => 'success',
                    'message' => "Published!"
                );
            }
        }
        echo json_encode($response);
    }

    public function unpublish_service()
    {
        $this->VendorsModel->unpublish_vendor($this->input->post('ServiceId'));
    }

    public function view_service()
    {
        $VendorId = $_POST["ServiceId"];

        $vendorSingle = $this->VendorsModel->getvendor($VendorId);
        $company = $this->CompanyModel->view_company($vendorSingle->CompanyId);
        if ($vendorSingle->PackageType == 1) {
            $package_type = "Basic";
        } else if ($vendorSingle->PackageType == 2) {
            $package_type = "Premium";
        } else {
            $package_type = "Ultimate";
        }

        if ($vendorSingle->CreatedAt == 0) {
            $CreatedAt = "N/A";
        } else {
            $CreatedAt = date('M-d-Y H:i:s A', strtotime($vendorSingle->CreatedAt));
        }

        if ($vendorSingle->ExpireAt == 0) {
            $ExpireAt = "N/A";
        } else {
            $ExpireAt = date('M-d-Y H:i:s A', strtotime($vendorSingle->ExpireAt));
        }

        echo '
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel"><strong>View Post Information </strong></h4>

                </div>
                <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Package Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Package Type</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $package_type . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Post Duration</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $vendorSingle->PostDuration . ' day/s" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">Start Date</label>
                                        <input readonly type="text" name="CreatedAt" id="CreatedAt" placeholder="Package Type" value="' . $CreatedAt . '" class="form-control">
                                    </div>

                                    <div class="form-group col-md-3">
                                    <label for="PackageType">End Date</label>
                                        <input readonly type="text" name="PackageType" id="PackageType" placeholder="Package Type" value="' . $ExpireAt . '" class="form-control">
                                    </div>
                                </div>

                                <hr style="border:1px solid black;">

                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><strong>Company Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="CompanyName">Company Name</label>
                    
                                        <input readonly value="' . $company->CompanyName . '" type="text" name="CompanyName" id="CompanyName" class="form-control" placeholder="Company Name">
                                    </div>
                    
                                    <div class="form-group col-md-6">
                                        <label for="CompanyAddress">Company Address</label>
                                        <input readonly value="' . $company->CompanyAddress . '" type="text" name="CompanyAddress" id="CompanyAddress" class="form-control" placeholder="Company Address">
                                    </div>
                    
                                    <div class="form-group col-md-3">
                                        <label for="CompanyTelNo">Telephone No. </label>
                                        <input readonly value="' . $company->CompanyTelNo . '" type="text" name="CompanyTelNo" id="CompanyTelNo" class="form-control" placeholder="Telephone No">
                                    </div>
                    
                                    <div class="form-group col-md-3">
                                        <label for="CompanyCelNo">Mobile No.</label>
                                        <input readonly value="' . $company->CompanyCelNo . '" type="text" name="CompanyCelNo" id="CompanyCelNo" class="form-control" placeholder="Mobile No">
                                    </div>
                    
                                    <div class="form-group col-md-6">
                                        <label for="CompanyEmail">Company Email</label>
                                        <input readonly value="' . $company->CompanyEmail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                    </div>
                    
                                    <div class="form-group col-md-6">
                                        <label for="CompanyEmail">HR Email Address</label>
                                        <input readonly value="' . $company->HRemail . '" type="text" name="CompanyEmail" id="CompanyEmail" class="form-control" placeholder="example@example.com">
                                    </div>
                                </div>

                                <hr style="border:1px solid black;">

                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><strong>Service Details</strong></h4>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="form-group col-md-12">
                                        <label for="JobPositionTitle">Title</label>
                                        <input readonly value="' . $vendorSingle->ServiceTitle . '" type="text" name="JobPositionTitle" id="JobPositionTitle" placeholder="Job Title" class="form-control">
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="JobDescription">Description</label>
                                        <div class="well" style="padding:10px; overflow-y:auto; height:200px;">
                                            ' . $vendorSingle->ServiceDescription . '
                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        ';
    }

    public function edit_service($ServiceId)
    {
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Vendors/edit_service';
        $view['custom_css'] = 'admin/Vendors/vendors_css';
        $view['custom_js'] = 'admin/Vendors/vendors_js';

        $view['Packages'] = $this->PackageModel->get_all_packages();
        // $view['Companies'] = $this->ParameterModel->getAllVendors();
        $view['Service'] = $this->VendorsModel->getvendor($ServiceId);
        $view['companies'] = $this->SubscriberModel->vendor_subscribers_valid();

        $this->load->view('main', $view);
    }

    public function update_service()
    {

        $this->form_validation->set_rules('ServiceTitle', 'Service Title', 'required|strip_tags|trim');
        $this->form_validation->set_rules('ServiceDescription', 'Service Description', 'required|trim');

        $ServiceId = $this->input->post('ServiceId');

        $post_details = array(
            'ServiceTitle' => $this->input->post('ServiceTitle'),
            'ServiceDescription' => $this->input->post('ServiceDescription'),
        );

        if ($this->form_validation->run() == FALSE) {
            $this->edit_service($ServiceId);
        } else {
            $message = '<div class="alert alert-success" role="alert">Service updated successfully!</div>';
            $this->VendorsModel->update_service($ServiceId, $post_details);
            $this->vendors($message);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF VENDORS
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOB PARAMETERS 
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function JobParam($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/JobParam/param_js';
        $view['custom_css'] = 'admin/JobParam/param_css';

        $view['content'] = 'admin/JobParam/all_parameters';
        $view['message'] = $message;
        $view['parameters'] = $this->ParameterModel->get_all();
        $this->load->view('main', $view);
    }

    public function add_job_parameter($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/JobParam/param_js';
        $view['custom_css'] = 'admin/JobParam/param_css';
        $view['content'] = 'admin/JobParam/add_parameter';
        $this->load->view('main', $view);
    }

    public function add_job_param()
    {
        $this->form_validation->set_rules('param_name', 'Parameter Name', 'required|strip_tags|trim|is_unique[parameter_tbl.param_name]');
        $this->form_validation->set_rules('param_type', 'Parameter Type', 'required|strip_tags');
        // $param_name = $this->input->post('param_name');
        if ($this->form_validation->run() == FALSE) {
            $this->add_job_parameter();
        } else {
            $message = '<div class="alert alert-success" role="alert">Record added successfully!</div>';
            $this->ParameterModel->add($_POST);
            $this->JobParam($message);
        }
    }

    public function edit_job_param()
    {
        $this->form_validation->set_rules('param_name', 'Parameter Name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('param_type', 'Parameter Type', 'required|strip_tags');

        $param_id = $this->input->post('param_id');
        $param_name = $this->input->post('param_name');
        $param_type = $this->input->post('param_type');
        if ($this->form_validation->run() == FALSE) {
            $this->edit_job_parameter($param_id);
        } else {

            $message = $this->ParameterModel->edit($param_id, $param_name, $param_type);
            $this->JobParam($message);
        }
    }

    public function edit_job_parameter($id)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/JobParam/param_js';
        $view['custom_css'] = 'admin/JobParam/param_css';
        $view['content'] = 'admin/JobParam/edit_parameter';
        $view['parameter'] = $this->ParameterModel->view($id);
        $this->load->view('main', $view);
    }

    public function add_job_values($id, $message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/JobParam/param_js';
        $view['custom_css'] = 'admin/JobParam/param_css';
        $view['content'] = 'admin/JobParam/add_values';
        $view['message'] = $message;
        $view['parameter'] = $this->ParameterModel->view($id);
        $view['values'] = $this->ParameterModel->fetchValues($id);
        $this->load->view('main', $view);
    }

    public function dd($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();
    }

    public function add_job_value()
    {
        $arr = array('admin');
        $this->authenticate($arr);
        $parameter_value = array(
            'pv_param' => $this->input->post('param_id'),
            'pv_value' => $this->input->post('param_value')
        );

        $param_id = $this->input->post('param_id');
        $this->form_validation->set_rules('param_value', 'Parameter value', 'required|strip_tags');

        if ($this->form_validation->run() == FALSE) {
            $this->add_job_values($param_id);
        } else {
            $message = $this->ParameterModel->add_value($parameter_value);
            $this->add_job_values($param_id, $message);
        }
    }

    public function delete_job_parameter($id, $param_type)
    {
        if ($param_type == 'select') {
            $this->ParameterModel->delete_job_select($id);
            $message = '<div class="alert alert-success" role="alert">Record successfully deleted!</div>';
        } else {
            $this->ParameterModel->delete($id);
            $message = '<div class="alert alert-success" role="alert">Record successfully deleted!</div>';
        }

        $this->JobParam($message);
    }

    public function delete_job_param_val($id, $param_id)
    {
        if (!$this->ParameterModel->delete_job_param_val($id)) {
            $this->ParameterModel->delete_job_param_val($id);
            $message = '<div class="alert alert-success" role="alert">Record successfully deleted!</div>';
        } else {
            $message = '<div class="alert alert-warning" role="alert">Failed to delete Record!</div>';
        }
        $this->add_job_values($param_id, $message);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END PARAMETER 
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // PACKAGES
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function packages($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/PackageParam/package_js';
        $view['custom_css'] = 'admin/PackageParam/package_css';
        $view['content'] = 'admin/PackageParam/all_packages';
        $view['message'] = $message;

        // $view['packages'] = $this->PackageModel->get_all_packages();
        $view['job_packages'] = $this->PackageModel->get_all_job_packages();
        $view['vendor_packages'] = $this->PackageModel->get_all_vendor_packages();

        $this->load->view('main', $view);
    }

    public function create_package($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/PackageParam/package_js';
        $view['custom_css'] = 'admin/PackageParam/package_css';
        $view['content'] = 'admin/PackageParam/create_package';
        $view['message'] = $message;
        $this->load->view('main', $view);
    }

    public function edit_package($id, $message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/PackageParam/package_js';
        $view['custom_css'] = 'admin/PackageParam/package_css';
        $view['content'] = 'admin/PackageParam/edit_package';
        $view['message'] = $message;
        $view['package'] = $this->PackageModel->get_package($id);
        $this->load->view('main', $view);
    }

    public function create_package_function()
    {
        $this->form_validation->set_rules('package_name', 'Package Name', 'required|strip_tags|trim|is_unique[packages_tbl.package_name]');
        $this->form_validation->set_rules('package_duration', 'Package Duration', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_limit_daily', 'Daily Post Limit', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_limit_monthly', 'Monthly Post Limit', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_duration', 'Post Duration', 'required|strip_tags|is_natural_no_zero|less_than_equal_to[30]');
        $this->form_validation->set_rules('package_cost', 'Package Cost', 'required|strip_tags');

        $package_details = array(
            'package_name' => $this->input->post('package_name'),
            'package_type' => $this->input->post('package_type'),
            'package_duration' => $this->input->post('package_duration'),
            'post_limit_daily' => $this->input->post('post_limit_daily'),
            'post_limit_monthly' => $this->input->post('post_limit_monthly'),
            'post_duration' => $this->input->post('post_duration'),
            'package_cost' => $this->input->post('package_cost')
        );

        if ($this->form_validation->run() == FALSE) {
            $this->create_package();
        } else {
            $message = $this->PackageModel->create_package($package_details);
            $this->create_package($message);
        }
    }

    public function edit_package_function()
    {
        $this->form_validation->set_rules('package_name', 'Package Name', 'required|strip_tags|trim');
        $this->form_validation->set_rules('package_duration', 'Package Duration', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_limit_daily', 'Daily Post Limit', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_limit_monthly', 'Monthly Post Limit', 'required|strip_tags|is_natural_no_zero');
        $this->form_validation->set_rules('post_duration', 'Post Duration', 'required|strip_tags|is_natural_no_zero|less_than_equal_to[30]');
        $this->form_validation->set_rules('package_cost', 'Package Cost', 'required|strip_tags');

        $package_id = $this->input->post('package_id');
        $package_details = array(
            'package_name' => $this->input->post('package_name'),
            'package_type' => $this->input->post('package_type'),
            'package_duration' => $this->input->post('package_duration'),
            'post_limit_daily' => $this->input->post('post_limit_daily'),
            'post_limit_monthly' => $this->input->post('post_limit_monthly'),
            'post_duration' => $this->input->post('post_duration'),
            'package_cost' => $this->input->post('package_cost')
        );

        if ($this->form_validation->run() == FALSE) {
            $this->edit_package($package_id);
        } else {
            $message = $this->PackageModel->edit_package($package_id, $package_details);
            $this->edit_package($package_id, $message);
        }
    }

    public function delete_package()
    {
        $this->PackageModel->delete_package($this->input->post('package_id'));
    }

    public function view_package()
    {
        $package_id = $_POST["package_id"];

        $package = $this->PackageModel->get_package($package_id);
        echo '
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel"><strong>View Package Information </strong></h4>
                    
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4><strong>Package Details</strong></h4>
                            <label>Package Name</label>
                            <input readonly value="' . $package->package_name . '" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4><strong>&nbsp;</strong></h4>
                            <label>Package Duration</label>
                            <input readonly value="' . $package->package_duration . ' day/s" class="form-control" type="text" name="package_name" param_name placeholder="Package Name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4><strong>Job Posting Details</strong></h4>
                            <label>Post Limit (Daily)</label>
                            <input readonly value="' . $package->post_limit_daily . '" class="form-control" type="number" name="post_limit_daily" param_name placeholder="Number of posts allowed per day">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4><strong>&nbsp;</strong></h4>
                            <label>Post Limit (Monthly)</label>
                            <input readonly value="' . $package->post_limit_monthly . '" class="form-control" type="number" name="post_limit_monthly" param_name placeholder="Number of posts allowed per month">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Post Duration</label>
                            <input readonly value="' . $package->post_duration . ' day/s" class="form-control" type="text" name="post_duration" param_name placeholder="Duration of a single job post in days">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Package Cost</label>
                            <input readonly value="' . $package->package_cost . '" class="form-control" type="number" name="package_cost" param_name placeholder="Package Cost (Philippine Peso)">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
        ';
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PACKAGES
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // SUBSCRIPTIONS
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function subscribers($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['subscribers'] = $this->SubscriberModel->subscribers();
        $view['vendors'] = $this->SubscriberModel->vendor_subscribers();
        $view['jobs'] = $this->SubscriberModel->job_subscribers();

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/Subscribers/subs_js';
        $view['custom_css'] = 'admin/Subscribers/subs_css';
        $view['content'] = 'admin/Subscribers/all_subs';
        $view['message'] = $message;
        $this->load->view('main', $view);
    }

    public function add_subscriber($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['companies'] = $this->CompanyModel->companies();
        $view['packages'] = $this->PackageModel->get_all_packages();

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/Subscribers/subs_js';
        $view['custom_css'] = 'admin/Subscribers/subs_css';
        $view['content'] = 'admin/Subscribers/add_subscriber';
        $view['message'] = $message;
        $this->load->view('main', $view);
    }

    public function create_subscriber()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $package_type = $this->PackageModel->get_package($this->input->post('package_id'))->package_type;

        $this->form_validation->set_rules('company_id', 'Company', 'required|strip_tags');
        $this->form_validation->set_rules('package_id', 'Package', 'required|strip_tags');

        $sub_details = array(
            'company_id' => $this->input->post('company_id'),
            'package_id' => $this->input->post('package_id'),
            'package_type' => $package_type,
            'date_submitted' => date('Y-m-d H:i:s', time()),
        );

        if ($this->form_validation->run() == FALSE) {
            $this->add_subscriber();
        } else {
            if ($package_type == 'job') {
                if ($this->SubscriberModel->check_if_subbed_to_jobs($this->input->post('company_id'))) {
                    $message = $this->SubscriberModel->check_if_subbed_to_jobs($this->input->post('company_id'));
                    $this->subscribers($message);
                } else {
                    $message = '<div class="alert alert-success" role="alert">Subscriber added successfully!</div>';
                    $this->SubscriberModel->add_subscriber($sub_details);
                    $this->subscribers($message);
                }
            } else {
                if ($this->SubscriberModel->check_if_subbed_to_vendors($this->input->post('company_id'))) {
                    $message = $this->SubscriberModel->check_if_subbed_to_vendors($this->input->post('company_id'));
                    $this->subscribers($message);
                } else {
                    $message = '<div class="alert alert-success" role="alert">Subscriber added successfully!</div>';
                    $this->SubscriberModel->add_subscriber($sub_details);
                    $this->subscribers($message);
                }
            }
        }
    }

    public function view_subscriber($sub_id)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['subscriber'] = $this->SubscriberModel->view_subscriber($sub_id);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'admin/Subscribers/subs_js';
        $view['custom_css'] = 'admin/Subscribers/subs_css';
        $view['content'] = 'admin/Subscribers/view_subscriber';
        $this->load->view('main', $view);
    }

    public function approve_subscriber($sub_id)
    {
        $arr = array('admin');
        $this->authenticate($arr);


        if ($this->SubscriberModel->approve_subscriber($sub_id)) {
            $message = '<div class="alert alert-success" role="alert">Request approved successfully!</div>';
        } else {
            $message = '<div class="alert alert-warning" role="alert">Request already approved!</div>';
        }

        $this->subscribers($message);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF SUBSCRIPTIONS
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////
    // JOB PARAMETER V2
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function Job_Parameters($message = null)
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_js'] = 'include/admin/job_param_js';
        $view['custom_css'] = 'include/admin/job_param_css';
        $view['content'] = 'admin/Job_Param/form_builder';
        $view['message'] = $message;
        $this->load->view('main', $view);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
    // END OF PARAMETER V2
    //////////////////////////////////////////////////////////////////////////////////////////////

    public function settings()
    {
        //dd($_SESSION);
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->load->model('SettingModel');
        $view['settings'] = $this->SettingModel->get_all();
        //dd($view['settings']);

        $view['messages'] = $this->MessageModel->get_all();
        $view['count_all'] = $this->MessageModel->count_all();
        $view['count_all_active'] = $this->MessageModel->count_all_active();
        $view['count_all_request'] = $this->EventModel->count_all_request('pending');
        $view['count_all_request_approve'] = $this->EventModel->count_all_request('approved');
        $view['count_all_request_disapprove'] = $this->EventModel->count_all_request('disapproved');
        $view['count_all_amenities'] = $this->AmenityModel->count_all_amenities();
        $view['count_all_requested'] = $this->EventModel->count_all_requested();
        $view['count_all_request_payment'] = $this->EventModel->count_all_request_payment();
        $view['count_all_residents'] = $this->ResidentModel->count_all('');
        $view['count_all_residents_active'] = $this->ResidentModel->count_all('1');
        $view['count_all_residents_inactive'] = $this->ResidentModel->count_all('0');

        $view['count_all_new_complaints'] = $this->ComplaintModel->count_all_new_complaints();
        $view['count_all_new_request'] = $this->EventModel->count_all_new_request();
        $view['count_all_new_members'] = $this->ResidentModel->count_all_new_members();

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/settings';
        $view['custom_js'] = 'include/admin/fee_js';
        $view['custom_css'] = 'include/admin/fee_css';

        $this->load->view('main', $view);
    }

    public function do_update_settings()
    {

        $this->load->model('SettingModel');
        $str = '<ul>';
        if (isset($_POST['emailsms'])) {
            if (isset($_POST['message'])) {
                $data['settings_enable'] = 1;
                $this->SettingModel->update(1, $data);
                $this->session->set_userdata('msg', 1);
                $str .= '<li>message is enabled</li>';
            } else {
                $data['settings_enable'] = 0;
                $this->SettingModel->update(1, $data);
                $this->session->set_userdata('msg', 0);
                $str .= '<li><font color="red">message is readonly</font></li>';
            }

            if (isset($_POST['sms'])) {
                $data['settings_enable'] = 1;
                $this->SettingModel->update(2, $data);
                $this->session->set_userdata('sms', 1);
                $str .= '<li>sms is enabled</li>';
            } else {
                $data['settings_enable'] = 0;
                $this->SettingModel->update(2, $data);
                $this->session->set_userdata('sms', 0);
                $str .= '<li><font color="red">message is readonly</font></li>';
            }

            if (isset($_POST['captcha'])) {
                $data['settings_enable'] = 1;
                $this->SettingModel->update(3, $data);
                $this->session->set_userdata('sms', 1);
                $str .= '<li>registration captcha is enabled</li>';
            } else {
                $data['settings_enable'] = 0;
                $this->SettingModel->update(3, $data);
                $str .= '<li><font color="red">message is readonly</font></li>';
            }
            $str .= '</ul>';
            $this->session->set_flashdata('update_success_emailsms', $str);
        }

        if (isset($_POST['tablereset'])) {
            if ($_POST['tablereset'] == "payments") {
                $this->db->truncate('dues');
                $this->db->set('fees_collected', 0);
                $this->db->set('fees_unpaid', 0);
                $this->db->update('residents');
            } elseif ($_POST['tablereset'] == "request") {
                $this->db->truncate('event_calendar');
            } elseif ($_POST['tablereset'] == "complaints") {
                $this->db->truncate('complaints');
            } elseif ($_POST['tablereset'] == "certifications") {
                $this->db->truncate('certifications');
            } elseif ($_POST['tablereset'] == "messages") {
                $this->db->truncate('messages');
            } elseif ($_POST['tablereset'] == "trails") {
                $this->db->truncate('trails');
            } else {
            }
        }


        if (isset($_POST['exportpayments'])) {
            $this->load->dbutil();
            $delimiter = ",";
            $newline = "\r\n";
            $enclosure = '"';
            $query = $this->db->query("SELECT * FROM dues");
            $data =  $this->dbutil->csv_from_result($query, $delimiter, $newline, $enclosure);
            $this->load->helper('download');
            force_download('mybackup.csv', $data);
        }
        if (isset($_POST['backupdb'])) {
            $this->load->dbutil();
            $backup = $this->dbutil->backup();
            $this->load->helper('download');
            force_download('ehoabackup.gz', $backup);
        }
        if (isset($_POST['backuptables'])) {
            $this->load->dbutil();
            $tables = array('residents', 'dues', 'tbladmin', 'certifications', 'complaints');

            $prefs = array(
                'tables'        => $tables,   // Array of tables to backup.
                'ignore'        => array(),                     // List of tables to omit from the backup
                'format'        => 'zip',                       // gzip, zip, txt
                'filename'      => 'ehoabackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
                'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
                'newline'       => "\n"                         // Newline character used in backup file
            );

            $backup = $this->dbutil->backup($prefs);
            $this->load->helper('download');
            force_download('ehoabackup.gz', $backup);
        }
        if (isset($_POST['backuptrails'])) {
            $this->load->dbutil();
            $tables = array('trails');
            $prefs = array(
                'tables'        => $tables,   // Array of tables to backup.
                'ignore'        => array(),                     // List of tables to omit from the backup
                'format'        => 'zip',                       // gzip, zip, txt
                'filename'      => 'ehoabackup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'      => TRUE,                        // Whether to add DROP TABLE statements to backup file
                'add_insert'    => TRUE,                        // Whether to add INSERT data to backup file
                'newline'       => "\n"                         // Newline character used in backup file
            );

            $backup = $this->dbutil->backup($prefs);
            $this->load->helper('download');
            force_download('ehoabackup.gz', $backup);
        }

        redirect('admin/settings');
    }

    // function import_dump($folder_name = null , $file_name) {
    //     $folder_name = 'dumps';
    //     $path = 'assets/backup_db/'; // Codeigniter application /assets
    //     $file_restore = $this->load->file($path . $folder_name . '/' . $file_name, true);
    //     $file_array = explode(';', $file_restore);
    //     foreach ($file_array as $query)
    //      {
    //      $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
    //      $this->db->query($query);
    //      $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
    //      }
    //     }

    public function index()
    {
        $view['content'] = 'admin/login/index';
        $view['custom_js'] = 'admin/login/login_js';
        $this->load->view('main', $view);
    }

    public function login()
    {
        $response = array();
        $this->form_validation->set_rules('admin_username', 'Username', 'required|callback_account_check');
        $this->form_validation->set_rules('admin_password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            redirect('admin/home');
        }
    }

    public function account_check($str)
    {
        $cond = $_POST;

        $cond['admin_password'] = sha1($cond['admin_password']);
        if (!$this->AdminModel->login($cond)) {
            $this->form_validation->set_message('account_check', 'Invalid username or password');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function signin()
    {
        $this->load->view('PMAP/includes/header');
        $this->load->view('PMAP/includes/navbar');
        $this->load->view('PMAP/user/profiling/login');
        $this->load->view('PMAP/includes/login_scripts');
        $this->load->view('PMAP/includes/footer');
    }

    // public function login()
    // {
    //     $response = array();
    //     $this->form_validation->set_rules('admin_username', 'Username', 'required');
    //     $this->form_validation->set_rules('admin_password', 'Password', 'required');

    //     if ($this->form_validation->run() == FALSE) {
    //         $this->index();
    //     } else {
    //         $response = array(
    //             'status'   => true,
    //             'message' => 'Login successful!',
    //             'redirect' => 'Admin/home'
    //         );
    //     }

    //     echo json_encode($response);
    // }

    // public function account_check($str)
    // {
    //     $cond = $_POST;

    //     $cond['admin_password'] = sha1($cond['admin_password']);
    //     if (!$this->AdminModel->login($cond)) {
    //         $this->form_validation->set_message('account_check', 'Invalid username or password');
    //         return FALSE;
    //     } else {
    //         return TRUE;
    //     }
    // }

    public function logout()
    {
        if ($this->session->userdata['role'] == 'admin') {
            $this->session->sess_destroy();
            redirect('admin');
        } else  if ($this->session->userdata['role'] == 'normal') {
            $this->session->sess_destroy();
            redirect('/');
        } else {
            $this->session->sess_destroy();
            redirect($this->uri->segment(1));
        }
    }

    public function home()
    {
        // $this->dd($_SESSION);
        $arr = array('admin', 'normal');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['custom_css'] = 'admin/Dashboard/dashboard_css';
        $view['custom_js'] = 'admin/Dashboard/dashboard_js';
        $view['content'] = 'admin/Dashboard/index';
        $this->load->view('main', $view);
    }


    public function admins()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['admins'] = $this->AdminModel->get_all();
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/admins';
        $view['custom_js'] = 'include/admin/residents_js';
        $view['custom_css'] = 'include/admin/residents_css';
        $this->load->view('main', $view);
    }

    public function add_admin()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/add_admin';
        $view['custom_js'] = 'include/admin/add_resident_js';
        $view['custom_css'] = 'include/admin/add_resident_css';
        $this->load->view('main', $view);
    }

    public function do_add_admin()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
        $this->form_validation->set_rules('admin_username', 'Username', 'required');
        $this->form_validation->set_rules('admin_password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->add_resident();
        } else {
            //dd($_POST);
            $data = $_POST;
            $data['admin_password'] = sha1($data['admin_password']);
            $data['created_at'] = date("Y-m-d H:i:s");
            unset($data['upass_confirmation']);
            $this->AdminModel->insert($data);
            redirect('admin/admins');
        }
    }

    public function update_admin()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        if (isset($_POST['update_info'])) {
            $this->form_validation->set_rules('admin_name', 'Admin Name', 'required');
        } else if (isset($_POST['updatepassword'])) {
            $this->form_validation->set_rules('admin_username', 'Username', 'required');
            $this->form_validation->set_rules('admin_password', 'Password', 'required');
        } else if (isset($_POST['updatepicture'])) {
            //dd($_POST);
            $this->form_validation->set_rules('updatepicture', 'Picture', 'required|callback_image_check');
        } else {
        }
        if ($this->form_validation->run() == FALSE) {
            $this->edit_admin($_POST['a_id']);
        } else {
            //dd($_POST);
            $data['updated_at'] = date("Y-m-d H:i:s");
            if (isset($_POST['update_info'])) {
                $data = elements(array('admin_name', 'status'), $_POST);

                $this->AdminModel->update($_POST['a_id'], $data);
            } else if (isset($_POST['updatepassword'])) {
                $data = elements(array('admin_username', 'admin_password'), $_POST);
                $data['admin_password'] = sha1($data['admin_password']);
                $this->AdminModel->update($_POST['a_id'], $data);
            } else {
            }
            redirect('admin/admins');
        }
    }

    public function update_admin_status($id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->AdminModel->update_status($id);
        redirect('admin/admins');
    }

    public function update_resident_status($id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        //SEND EMAIL MESSAGE
        $request = $this->ResidentModel->get_resident($id);
        $dataemail['email'] = $request->email;
        $dataemail['message'] = "Hi " . $request->fname . ' ' . $request->lname .
            ", \r\n Your registration to ehoa has been successfully approve. You may login through " .
            base_url() . 'user/login';
        $this->send_message($dataemail);
        //.SEND EMAIL MESSAGE

        //SEND SMS MESSAGE
        // if($this->session->sms){
        //     $message = "Thanks for registering to eHOA you are now  ";
        //     $result = $this->itexmo($request->user_contact_no,$message,$this->config->item('itextmo_api'));
        //     if ($result == ""){
        //         echo "iTexMo: No response from server!!!
        //         Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.  
        //         Please CONTACT US for help. "; 
        //         die('<a href="'.base_url().'admin" >Click to load the index</a>'); 
        //     }else if ($result == 0){
        //         echo "Message Sent!";
        //     }
        //     else{   
        //         echo "Error Num ". $result . " was encountered!";
        //         die('<a href="'.base_url().'admin" >Click to load the index</a>'); 
        //     }

        //  }
        //.SEND SMS MESSAGE

        $this->ResidentModel->update_status($id);
        redirect('admin/residents');
    }

    public function password_check($pass)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $cond['id'] = $_POST['r_id'];
        $cond['upass'] = sha1($pass);
        if (!$this->ResidentModel->password_check($cond)) {
            $this->form_validation->set_message('password_check', 'Incorrect current password!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function delete_resident($r_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['resident'] = $this->ResidentModel->get_resident($r_id);
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/delete_resident';
        $view['custom_js'] = 'include/admin/remove_resident_js';
        $this->load->view('main', $view);
    }

    public function remove_resident($r_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['resident'] = $this->ResidentModel->destroy($r_id);
        redirect('admin/residents');
    }

    public function delete_admin($r_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['resident'] = $this->AdminModel->destroy($r_id);
        redirect('admin/admins');
    }

    // public function image_check($str = "")
    // {
    //     $arr = array('admin', 'cashier', 'officer');
    //     $this->authenticate($arr);

    //     if (
    //         $this->uri->segment(2) == "do_add_event" || $this->uri->segment(2) == "do_add_member"
    //         || $this->uri->segment(2) == "do_add_cms" || $this->uri->segment(2) == "do_add_amenity"
    //     ) {
    //         $upload_path = './uploads_event/';
    //     } else if (
    //         $this->uri->segment(2) == "do_update_event" || $this->uri->segment(2) == "do_update_member"
    //         || $this->uri->segment(2) == "do_update_cms" || $this->uri->segment(2) == "do_update_amenity"
    //     ) {
    //         $upload_path = './uploads_event/';
    //     } else {
    //         $upload_path = './uploads/';
    //         //dd('dasd');
    //     }
    //     $config['upload_path']          = $upload_path;
    //     $config['allowed_types']        = 'gif|jpg|png|jpeg';
    //     $config['overwrite']            = TRUE;
    //     $config['max_size']             = 1000;
    //     $config['max_width']            = 1366;
    //     $config['max_height']           = 768;
    //     $this->load->library('upload', $config);
    //     if (!$this->upload->do_upload('profilepict')) {
    //         $error = $this->upload->display_errors();
    //         $this->form_validation->set_message('image_check', $error);
    //         return FALSE;
    //     } else {
    //         $filedata = $this->upload->data();
    //         $filename = $filedata['file_name'];
    //         $this->load->library('image_lib');
    //         $config2['image_library'] = 'gd2';
    //         $config2['source_image'] = $upload_path . $filename;
    //         $config2['new_image'] = './uploads_thumb';
    //         $config2['create_thumb'] = FALSE;
    //         $config2['maintain_ratio'] = TRUE;
    //         $config2['width']         = 150;
    //         $config2['height']       = 100;
    //         $this->image_lib->initialize($config2);
    //         $this->image_lib->resize();
    //         if (isset($_POST['r_id'])) {
    //             $data['profilepict'] = $filename;
    //             $this->ResidentModel->update($_POST['r_id'], $data);
    //         }
    //         if (isset($_POST['a_id'])) {
    //             $data['image'] = $filename;
    //             //dd($data['image']);
    //             $this->AdminModel->update($_POST['a_id'], $data);
    //         }
    //         $this->session->set_flashdata('filename', $filename);
    //         return TRUE;
    //     }
    // }

    public function cms()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->load->model('SiteModel');
        $view['cms'] = $this->SiteModel->get_all();
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/cms';
        $view['custom_js'] = 'include/admin/request_amenity_js';
        $this->load->view('main', $view);
    }

    public function add_cms()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/add_cms';
        $view['custom_js'] = 'include/admin/add_resident_js';
        $view['custom_css'] = 'include/admin/add_resident_css';
        $this->load->view('main', $view);
    }

    public function do_add_cms()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('add_cms', 'Image', 'callback_image_check');
        if ($this->form_validation->run() == FALSE) {
            $this->add_cms();
        } else {
            $filename = $this->session->flashdata('filename');
            //dd($filename);
            $data['cms_image'] = $filename;
            $data['cms_title'] = $_POST['title'];
            $data['cms_description'] = $_POST['description'];
            $data['cms_type'] = $_POST['type'];
            $data['created_at'] = date("Y-m-d H:i:s");
            $this->load->model('SiteModel');
            $this->SiteModel->insert($data);
            redirect('admin/cms');
        }
    }

    public function edit_cms($cms_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->load->model('SiteModel');
        $view['cms'] = $this->SiteModel->get($cms_id);
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/edit_cms';
        $view['custom_js'] = 'include/admin/add_resident_js';
        $view['custom_css'] = 'include/admin/add_resident_css';
        $this->load->view('main', $view);
    }

    public function do_update_cms($cms_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->form_validation->set_rules('title', 'CMS Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if (isset($_POST['update_image'])) {
            $this->form_validation->set_rules('update_image', 'Image', 'callback_image_check');
        }
        if ($this->form_validation->run() == FALSE) {
            $this->edit_cms($cms_id);
        } else {
            $filename = $this->session->flashdata('filename');
            if (isset($_POST['update_image'])) {
                $data['cms_image'] = $filename;
            }
            $data['cms_title'] = $_POST['title'];
            $data['cms_description'] = $_POST['description'];
            $data['cms_type'] = $_POST['type'];
            $data['updated_at'] = date("Y-m-d H:i:s");
            $this->load->model('SiteModel');
            $this->SiteModel->update($cms_id, $data);
            redirect('admin/cms');
        }
    }

    public function delete_cms($cms_id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->load->model('SiteModel');
        $this->SiteModel->delete($cms_id);
        redirect('admin/cms');
    }

    public function message()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        // $view['messages'] = $this->MessageModel->get_all();
        // $view['count_all'] = $this->MessageModel->count_all();
        // $view['count_all_active'] = $this->MessageModel->count_all_active();

        // $view['count_all_request'] = $this->EventModel->count_all_request('pending');
        // $view['count_all_request_approve'] = $this->EventModel->count_all_request('approved');
        // $view['count_all_request_disapprove'] = $this->EventModel->count_all_request('disapproved');
        // $view['count_all_amenities'] = $this->AmenityModel->count_all_amenities();
        // $view['count_all_requested'] = $this->EventModel->count_all_requested();
        // $view['count_all_request_payment'] = $this->EventModel->count_all_request_payment();

        // $view['count_all_new_complaints'] = $this->ComplaintModel->count_all_new_complaints();
        // $view['count_all_new_request'] = $this->EventModel->count_all_new_request();
        // $view['count_all_new_members'] = $this->ResidentModel->count_all_new_members();

        // $view['count_all_residents'] = $this->ResidentModel->count_all('');
        // $view['count_all_residents_active'] = $this->ResidentModel->count_all('1');
        // $view['count_all_residents_inactive'] = $this->ResidentModel->count_all('0');

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/messages';
        $view['custom_js'] = 'include/user/message_js';
        $view['custom_css'] = 'include/user/message_css';
        $this->load->view('main', $view);
    }

    public function mark_as_read($id)
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->MessageModel->update($id, array('message_active' => '0'));
        redirect('admin/message');
    }

    public function trails()
    {
        $arr = array('admin', 'cashier', 'officer');
        $this->authenticate($arr);

        $this->load->model('TrailModel');
        $view['trails'] = $this->TrailModel->get_all();
        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/trail';
        $view['custom_js'] = 'include/admin/request_amenity_js';
        $view['custom_css'] = 'include/admin/residents_css';
        $this->load->view('main', $view);
    }

    public function reports()
    {
        $arr = array('admin');
        $this->authenticate($arr);

        $view['navbar'] = 'include/admin/navbar';
        $view['sidebar'] = 'include/admin/sidebar';
        $view['content'] = 'admin/Reports/reports';
        $view['custom_css'] = 'admin/Reports/report_css';
        $view['custom_js'] = 'admin/Reports/report_js';
        $this->load->view('main', $view);
    }


    public function send_message($data)
    {
        //dd($_SESSION);
        if ($this->session->userdata('msg')) {
            $this->load->library('email');
            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            //$config['smtp_user']    = '';
            //$config['smtp_pass']    = '';
            $config['smtp_user']    = 'romandeangel';
            $config['smtp_pass']    = 'MJD@nge1';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'text'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      

            $this->email->initialize($config);
            //dd($config);

            $this->email->from('adminehoa@mehoa.org', 'eHOA Admin');
            $this->email->to($data['email']);

            $this->email->subject('Email Send');
            //$activation_code = random_string('alnum', 16);
            $this->email->message($data['message'] . "\r\n [THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS EMAIL]");

            $this->email->send();
        }
    }





    //CURL LESS
    //##########################################################################
    // ITEXMO SEND SMS API - PHP - CURL-LESS METHOD
    // Visit www.itexmo.com/developers.php for more info about this API
    //##########################################################################
    public function itexmo($number, $message, $apicode)
    {
        $url = 'https://www.itexmo.com/php_api/api.php';
        $itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
        $param = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($itexmo),
            ),
        );
        $context  = stream_context_create($param);
        return file_get_contents($url, false, $context);
    }
    //##########################################################################


    //CURL METHOD
    //##########################################################################
    // ITEXMO SEND SMS API - PHP - CURL METHOD
    // Visit www.itexmo.com/developers.php for more info about this API
    //##########################################################################
    // function itexmo($number,$message,$apicode){
    //             $ch = curl_init();
    //             $itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
    //             curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
    //             curl_setopt($ch, CURLOPT_POST, 1);
    //              curl_setopt($ch, CURLOPT_POSTFIELDS, 
    //                       http_build_query($itexmo));
    //             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //             return curl_exec ($ch);
    //             curl_close ($ch);
    // }
    //##########################################################################






}
