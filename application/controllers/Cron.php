<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CronModel');
    }

    public function reset_daily_post()
    {
        $this->CronModel->reset_daily_post();
    }

    public function reset_montlhy_post()
    {
        $this->CronModel->reset_montlhy_post();
    }
}
